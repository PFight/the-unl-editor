////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Bind.hpp>
#include <The2D/Component.hpp>
#include <The2D/Parameters.hpp>
#include <The2D/Connectors.hpp>
#include <The2D/CValueVector.hpp>

#include "Logic/Graph/Graph.hpp"
#include "Logic/Graph/NodeGroup.hpp"

using namespace the;

////////////////////////////////////////////////////////////
namespace mp {

    class CreateGroup : public Component
    {
        public:
            THE_ENTITY(CreateGroup, Component)

            ToValueVectorR<GraphItem*> toSelection;
            ToFunc<sptr<NodeGroup>()> toCreateGroupFunc;
            ToValueR<Graph*> toGraph;

            CCommand DoCreate;

            CreateGroup();
            virtual ~CreateGroup();

        private:
            InitState onInit() override;
            void onDeinit() override;

            void create();
            
    };
}; // namespace mp
////////////////////////////////////////////////////////////