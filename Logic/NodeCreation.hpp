#pragma once

#include <Base/ToggleComponent.hpp>
#include <The2D/Bind.hpp>
#include <The2D/Connectors.hpp>
#include <The2D/CValueVector.hpp>
#include "Logic/Grid/GridManager.hpp"
#include "Logic/SceneManager.hpp"

using namespace the;

namespace mp
{
    class NodeEdgeEdit;
    class Graph;
    class GraphItem;

    class NodeCreation : public ToggleComponent
    {
        public:
            THE_ENTITY(NodeCreation, the::Component)

            ToValueVector<GraphItem*> toSelectedItems;
            ToValueR<QRectF> toSelectionRect;
            ManagerBind<SceneManager> toScene;

            the::Bind<NodeEdgeEdit> toEditor;
            ToValueR<Graph*> toGraph;

            CCommand DoAddNode;
            CEvent EvNodeCreated;

            NodeCreation();

        private:
            void addNode();
       
    };
}