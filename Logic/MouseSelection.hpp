#pragma once

#include <The2D/Component.hpp>
#include <The2D/Bind.hpp>
#include <The2D/Vec2.hpp>
#include <The2D/CValueVector.hpp>

#include <QPoint>
#include <QObject>

#include "Logic/MouseHoverGraph.hpp"

using namespace the;

namespace mp
{
    class MouseSelection : public Component
    {
        public:
            THE_ENTITY(MouseSelection, Component)

            ToValueVector<GraphItem*> toSelectedItems;
            Bind<MouseHoverGraph> toMouseHoverGraph;
            ToValueR<Graph*> toActiveGraph;

            CCommand DoSelectUnderMouse;
            CCommand DoAddToSelectionUnderMouse;

            MouseSelection();

        private:

            the::InitState onInit();
            void onDeinit();

            void selectUnderMouse(bool resetSelection);
    };
}