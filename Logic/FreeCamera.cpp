#include "Logic/FreeCamera.hpp"
#include "MouseHoverGraph.hpp"
#include <QEvent>
#include <QGraphicsSceneMouseEvent>

using namespace mp;
using namespace the;



FreeCamera::FreeCamera(): mDragBegun(false), mShiftingCamera(false)
{
    REGISTER(toCameraPos);
    REGISTER(toScene);
    REGISTER(prmSpeed).defval(0.01);
    REGISTER(EvDragBegin);
    REGISTER(EvDragEnd);
    setAutoSubscribeToLoop(false);
}

the::InitState FreeCamera::onInit()
{
    return true;
}

void FreeCamera::draw()
{
    Vec2 moveVector;
    if((mCurrentMove & MOVE_UP) != 0)
    {
        moveVector = moveVector + Vec2(0, -1);
    }
    if((mCurrentMove & MOVE_DOWN) != 0)
    {
        moveVector = moveVector + Vec2(0, 1);
    }
    if((mCurrentMove & MOVE_RIGHT) != 0)
    {
        moveVector = moveVector + Vec2(1, 0);
    }
    if((mCurrentMove & MOVE_LEFT) != 0)
    {
        moveVector = moveVector + Vec2(-1, 0);
    }
    float moveVectorLength = sqrt(moveVector.x*moveVector.x + moveVector.y *moveVector.y);
    if(abs(moveVector.x) > 1 || abs(moveVector.y) > 1)
    {
        Vec2 normalMoveVector(moveVector.x / moveVectorLength, moveVector.y / moveVectorLength);
        Vec2 shift = prmSpeed() * normalMoveVector;
        shiftCamera(shift);
    }
}

void FreeCamera::shiftCamera(Vec2 shift)
{
    // mSiftingCamera needed to prevent infinite loop (see eventFilter)
    mShiftingCamera = true;
    toCameraPos = (toCameraPos + shift);
    mShiftingCamera = false;
}

void FreeCamera::enableMoving(MoveDirection direction, bool enable)
{
    if(enable)
    {
        mCurrentMove = (MoveDirection) (mCurrentMove | direction);
    }
    else
    {
        mCurrentMove = (MoveDirection) (mCurrentMove & (~direction));
    }
}

void FreeCamera::beginMouseDrag(QPoint mousePos)
{
    mLastMousePos = mousePos;
    mDragBegun = true;
    EvDragBegin();
}

void FreeCamera::endMouseDrag()
{
    mLastMousePos = QPoint(0,0);
    mDragBegun = false;
    EvDragEnd();
}

void FreeCamera::processNewMousePos(QPoint newMousePos)
{
    if (mLastMousePos.x() == 0 && mLastMousePos.y() == 0)
    {
        mLastMousePos = newMousePos;
    }
    
    if(mDragBegun)
    {
        QPoint shift = mLastMousePos - newMousePos;
        shiftCamera(Vec2(shift.x(), shift.y()));
        mLastMousePos = newMousePos;
    }
}

void FreeCamera::onDeinit()
{
}

 bool FreeCamera::eventFilter(QObject * watched, QEvent * event)
 {
     // Shifting camera raises another one MouseMove event
     if (!mShiftingCamera)
     {
        switch (event->type())
        {
            CASE(QEvent::GraphicsSceneMousePress)
                {
                   QGraphicsSceneMouseEvent* mouseEv = static_cast<QGraphicsSceneMouseEvent*>(event);
                   beginMouseDrag(mouseEv->screenPos());
                }
            END_CASE;
            CASE(QEvent::GraphicsSceneMouseMove)
                QGraphicsSceneMouseEvent* mouseEv = static_cast<QGraphicsSceneMouseEvent*>(event);
                processNewMousePos(mouseEv->screenPos());
            END_CASE;
            CASE(QEvent::GraphicsSceneMouseRelease)
                endMouseDrag();
            END_CASE;
            default:;
        }
     }
     return false;
 }

void FreeCamera::onEnable(bool enable)
{
    if (enable)
    {
       //toScene->getView()->setMouseTracking(true);
       toScene->getScene()->installEventFilter(this);
       if (QApplication::mouseButtons() != Qt::NoButton)
       {
           mDragBegun = true;
       }
       mLastMousePos = QPoint(0,0);
    }
    else
    {
       toScene->getScene()->removeEventFilter(this);
       mDragBegun = false;
    }
}