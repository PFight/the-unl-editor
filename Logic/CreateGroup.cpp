////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/Config.hpp>
#include "Logic/CreateGroup.hpp"

using namespace mp;
using namespace the;

CreateGroup::CreateGroup()
{
    REGISTER(toSelection);
    REGISTER(toCreateGroupFunc);
    REGISTER(toGraph);
    REGISTER(DoCreate).setFunc([&]{ create(); });
}

CreateGroup::~CreateGroup()
{
}

void CreateGroup::onDeinit()
{
}

the::InitState CreateGroup::onInit()
{
    return READY;
}

void CreateGroup::create()
{
    eastl::vector<Node*> nodes;
    for (GraphItem* item: toSelection)
    {
        if (Node* node = item->toNode())
        {
            nodes.push_back(node);
        }
    }

    if (nodes.size() > 0)
    {
        auto group = toCreateGroupFunc();
        for (auto node : nodes)
        {
            group->toNodes.addTarget(node);
        }
        toGraph()->addNodeGroup(group);
    }
    else
    {
        WARN("There is not selected items");
    }
}