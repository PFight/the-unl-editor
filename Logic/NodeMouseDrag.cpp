#include "Logic/NodeMouseDrag.hpp"
#include "MouseHoverGraph.hpp"
#include <QEvent>
#include <QGraphicsSceneMouseEvent>
#include <Logic/Graph/Node.hpp>

using namespace mp;
using namespace the;



NodeMouseDrag::NodeMouseDrag()
{
    REGISTER(toLayout);
    REGISTER(toScene);
    REGISTER(toSelectedItems);
    REGISTER(toMouseHoverGraph);
    REGISTER(prmMaxForceKoef).defval(30.0f);
    REGISTER(mDragJoint).setInitOrder(BY_PARENT);
    REGISTER(mAttachBody)
        .setGroup("MouseDragAttach");
        mAttachBody.prmPosition = b2Vec2(0,0);
}

the::InitState NodeMouseDrag::onInit()
{
    return true;
}

void NodeMouseDrag::processNewMousePos(QPoint newMousePos)
{
    if (mLastMousePos.x() == 0 && mLastMousePos.y() == 0)
    {
        mLastMousePos = newMousePos;
        return;
    }
    QPoint qshift = newMousePos - mLastMousePos;
    Vec2 shift = toLayout->vectorFromScreen(qshift);

    for (auto item : toSelectedItems)
    {
        if (Node* node = item->toNode())
        {
            node->mLayout.CPosition = node->mLayout.CPosition + shift;
        }
    }
    /* b2MouseJoint* mouseJoint = mDragJoint.getMouseJoint();
    mouseJoint->SetTarget(toLayout->pointToBox2D(toLayout->pointFromScreen(newMousePos)));*/
    mLastMousePos = newMousePos;
}

void NodeMouseDrag::onDeinit()
{
}

 bool NodeMouseDrag::eventFilter(QObject * watched, QEvent * event)
 { 
    switch (event->type())
    {
        CASE(QEvent::GraphicsSceneMouseMove)
            QGraphicsSceneMouseEvent* mouseEv = static_cast<QGraphicsSceneMouseEvent*>(event);
            processNewMousePos(mouseEv->screenPos());
        END_CASE;
        default:;
    }
    return false;
 }

 void NodeMouseDrag::onEnable(bool enable)
 {
    if (enable)
    {
        /*if(mDragJoint.isInitialized())
        {
            mDragJoint.deinit();
        }
        mDragJoint.prmTarget = toLayout->pointToBox2D(toLayout->pointFromScreen(mousePos));
        mDragJoint.toBodyA.set(&mAttachBody);
        mDragJoint.toBodyB.set(&node->mLayout.mBody);
        mDragJoint.prmMaxForce = prmMaxForceKoef * node->mLayout.mBody.getBody()->GetMass();
        mDragJoint.init();
        node->mLayout.mBody.getBody()->SetActive(true);*/
        mLastMousePos = QPoint(0,0);
        toScene->getScene()->installEventFilter(this);
    }
    else
    {
       //mDragJoint.deinit();
       toScene->getScene()->removeEventFilter(this);
    }
 }