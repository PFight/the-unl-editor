////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include "Config.hpp"
#include "The2D/Manager.hpp"
#include "The2D/Object.hpp"
#include <The2D/Connectors.hpp>

#include <QtWidgets/QApplication>

////////////////////////////////////////////////////////////
namespace mp
{

    using namespace the;

    class Application: public QApplication
    {
        Q_OBJECT

        public:
            the::Signal<void(QEvent* ev)> SigEvent;

            Application(int &argc, char **argv, int = ApplicationFlags);

        protected:
            bool notify(QObject *, QEvent *) override;
    };

    class ApplicationManager: public the::Manager
    {
        public:
 
            int prmArgc;
            char* prmArgv[];

            CCommand DoExit;

            THE_ENTITY(mp::ApplicationManager, the::Manager)

            /// \brief Constructor, here the window handle is initialized using the::ScreenSizeX and the::ScreenSizeY
            ApplicationManager();

            void draw();

            Application* getApp() const { return mApp; }

            void exit();

        private:

            Application* mApp;

            the::InitState onInit();
            void onDeinit();

    };

}; // namespace mp
////////////////////////////////////////////////////////////
