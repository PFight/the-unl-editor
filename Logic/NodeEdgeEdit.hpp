#pragma once

#include <The2D/Component.hpp>
#include <The2D/Connectors.hpp>
#include <The2D/Bind.hpp>
#include <Logic/UserInput/InputEvent.hpp>
#include <QtWidgets/QGraphicsProxyWidget>
#include <The2D/CValueVector.hpp>

using namespace the;

class QTextEdit;

namespace mp
{
    class MainWindowManager;
    class NodeEdgeEdit;
    class GraphItem;

     class EditGraphicsItem : public QGraphicsProxyWidget
    {
        Q_OBJECT
        public:
            EditGraphicsItem(NodeEdgeEdit* parentComponent);

            static constexpr int EditGraphicsItemID = 3;

            NodeEdgeEdit* getParentComponent();

        protected:
            NodeEdgeEdit* mParentComponent;

            bool sceneEvent ( QEvent * event ) override;

            friend class NodeEdgeEdit;
    };

    class NodeEdgeEdit : public the::Component
    {
        public:
            THE_ENTITY(NodeEdgeEdit, the::Component)

            ToValueVectorR<GraphItem*> toSelectedItems;
            the::ManagerBind<mp::MainWindowManager> toMainWindow;

            PSize prmEditSize;

            CCommand DoAccept;
            CCommand DoCancel;
            CCommand DoEditSelected;

            NodeEdgeEdit();



        private:
            QTextEdit* mEdit;
            EditGraphicsItem* mEditProxy;
            CString* mEditingValue;

            the::InitState onInit();
            void onDeinit();

            void editSelection();
            void accept();
            void cancel();
    };
}