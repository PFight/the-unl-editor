////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Bind.hpp>
#include <The2D/Component.hpp>
#include "Logic/Graph/GraphHelper.hpp"
#include <The2D/Connectors.hpp>

using namespace the;


////////////////////////////////////////////////////////////
namespace mp {

    class Graph;

    class SentenceToNodes : public the::Component
    {
        public:
            THE_ENTITY(SentenceToNodes, the::Component)

            ToValueR<Graph*> toGraph;
            Bind<GraphHelper> toGraphHelper;

            CInt prmInterval;

            CCommand DoGenerateNodes;            

            SentenceToNodes();
            virtual ~SentenceToNodes();

        private:
            InitState onInit() override;
            void onDeinit() override;

            void generateNodes();

    };
}; // namespace mp
////////////////////////////////////////////////////////////
