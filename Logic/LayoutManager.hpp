////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Parameters.hpp>
#include <The2D/Manager.hpp>
#include <The2D/Bind.hpp>
#include <Box2D/Common/b2Math.h>
#include <QPoint>
#include <QPointF>


using namespace the;

class QGraphicsItem;

////////////////////////////////////////////////////////////
namespace mp {

    class SceneManager;

    class LayoutManager : public the::Manager
    {
        public:
            THE_ENTITY(LayoutManager, the::Manager)

            ManagerBind<SceneManager> toSceneManager;

            PFloat prmPhysicsScale;

            LayoutManager();
            virtual ~LayoutManager();

            Vec2 pointFromBox2D(b2Vec2 pos);
            Vec2 pointFromScreen(QPoint pos);
            Vec2 vectorFromBox2D(b2Vec2 vec);
            Vec2 vectorFromScreen(QPoint vec);
            float fromBox2D(float val);
            float fromScreen(int val);
            b2Vec2 pointToBox2D(Vec2 pos);
            QPoint pointToScreen(Vec2 pos);
            b2Vec2 vectorToBox2D(Vec2 vec);
            QPoint vectorToScreen(Vec2 vec);
            float32 toBox2D(float val);
            int toScreen(float val);

            Vec2 pointFromItem(QPointF pos, QGraphicsItem* item);
            Vec2 vectorFromItem(QPointF vec, QGraphicsItem* item);
            QPointF pointToItem(Vec2 pos, QGraphicsItem* item);
            QPointF vectorToItem(Vec2 vec, QGraphicsItem* item);

        private:
            InitState onInit() override;
            void onDeinit() override;

    };
}; // namespace mp
////////////////////////////////////////////////////////////
