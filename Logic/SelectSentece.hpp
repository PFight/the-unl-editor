////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Bind.hpp>
#include <The2D/Component.hpp>
#include "Logic/SentenceToGraph.hpp"
#include "Logic/SentenceToNodes.hpp"

using namespace the;


////////////////////////////////////////////////////////////
namespace mp {

    class SelectSentece : public the::Component
    {
        public:
            THE_ENTITY(SelectSentece, the::Component)

            ToValueR<Graph*> toGraph;
            Bind<SentenceToGraph> toGraphSelector;
            Bind<SentenceToNodes> toNodesGenerator;

            CCommand DoSelect;

            SelectSentece();
            virtual ~SelectSentece();



        private:
            InitState onInit() override;
            void onDeinit() override;

            void select();

    };
}; // namespace mp
////////////////////////////////////////////////////////////
