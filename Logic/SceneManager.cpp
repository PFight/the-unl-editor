////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include "Logic/SceneManager.hpp"
#include "ui_mainwindow.h"
#include <QLayout>
#include <QtWidgets/QPushButton>
#include <qgraphicsproxywidget.h>
#include <QDebug>
#include <QApplication>
#include <mainwindow.h>

using namespace mp;



SceneManager::SceneManager(): mScene(nullptr), mView(nullptr)
{
    REGISTER(toMainWindow);
}

void SceneManager::onDeinit()
{
//    toMainWindow->getWindow()->SigResize.remove(this);
    mView->removeEventFilter(this);
}


the::InitState SceneManager::onInit()
{
    //toMainWindow->getWindow()->SigResize.add(this, std::bind(&SceneManager::windowResize, this, std::placeholders::_1));

    mView = toMainWindow->getWindow()->ui->graphicsView;
    mScene = new QGraphicsScene();
    mView->setScene(mScene);
    mView->installEventFilter(this);

    windowResize(nullptr);

    QApplication::processEvents(QEventLoop::ProcessEventsFlag::AllEvents);

    return true;
}

void SceneManager::windowResize(QResizeEvent * event)
{
    mView->setSceneRect(-10000, -10000, 20000, 20000);
                         //toMainWindow->getWindow()->size().width(),
                         //toMainWindow->getWindow()->size().height());
}

bool SceneManager::eventFilter(QObject* watcher, QEvent* ev)
{

    if (ev->type() == QEvent::KeyPress)
    {
        QApplication::sendEvent(mScene, ev);
        return true;
    }
    return false;
}