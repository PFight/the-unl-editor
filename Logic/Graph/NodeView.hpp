#pragma once

#include <The2D/Component.hpp>
#include <The2D/Connectors.hpp>
#include <The2D/Utils/Signals.hpp>
#include <QtWidgets/QLabel>
#include <QtWidgets/QGraphicsProxyWidget>
#include <QEvent>

#include "Logic/SceneManager.hpp"

using namespace the;

namespace mp
{
    class Node;
    class NodeView;

    class NodeGraphicsItem : public QGraphicsProxyWidget
    {
        Q_OBJECT
        public:
            NodeGraphicsItem(NodeView* parentComponent);

            static constexpr int NodeGraphicsItemID = 1;

            NodeView* getParentNodeView();

        protected:
            NodeView* mParentComponent;
            
            bool sceneEvent ( QEvent * event ) override;
    };

    class NodeView : public the::Component
    {
        public:
            THE_ENTITY(NodeView, the::Component)

            enum SelectionState
            {
                NOT_SELECTED,
                HALF_SELECTED,
                SELECTED
            };

            ManagerBind<SceneManager> toSceneManager;
            CVec2 toPosition;
            CString toText;

            PString prmNotSelectedStyle;
            PString prmHalfSelectedStyle;
            PString prmSelectedStyle;

            CValue<SelectionState> CSelectionState;
            CEvent EvSelectionStateChanged;
            CVec2R CSize;
            //CBool CAutoSize;

            CCommand DoUpdateState;
            CCommand DoUpdatePosition;

            Signal<void(QEvent*)> SigSceneEvent;

            NodeView();

            Node* getNode();
            void setNode(Node* node);

            NodeGraphicsItem* getSceneItem();

        private:
            QLabel* mLabel;
            NodeGraphicsItem* mView;
            Vec2 mSize;
            Node* mMyNode;

            void getSize(Vec2& result);
            //void setSize(Vec2 size);

            void updateState();
            void updatePosition();

            the::InitState onInit();
            void onDeinit();
    };
}
