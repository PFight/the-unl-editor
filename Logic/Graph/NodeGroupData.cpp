////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/Config.hpp>
#include "Logic/Graph/NodeGroupData.hpp"

using namespace mp;
using namespace the;

NodeGroupData::NodeGroupData()
{
    REGISTER(toNodes);
}

NodeGroupData::~NodeGroupData()
{
}

NodeGroup* NodeGroupData::getGroup()
{
    return mGroup;
}
void NodeGroupData::setGroup(NodeGroup* group)
{
    mGroup = group;
}

void NodeGroupData::onDeinit()
{
}

the::InitState NodeGroupData::onInit()
{
    return READY;
}