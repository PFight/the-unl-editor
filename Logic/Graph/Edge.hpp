#pragma once

#include "Logic/Graph/EdgeView.hpp"
#include "Logic/Graph/EdgeData.hpp"
#include "Logic/Graph/EdgeLayout.hpp"
#include "Logic/Graph/Node.hpp"

using namespace the;


namespace mp
{
    class Edge : public GraphItem
    {
        public:
            THE_ENTITY(Edge, GraphItem)

            Bind<Node> toFirstNode;
            Bind<Node> toSecondNode;

            EdgeData mData;
            EdgeView mView;
            EdgeLayout mLayout;

            Edge();

            virtual bool isNode() override;
            virtual bool isEdge() override;
            virtual bool isGroup() override;

            virtual Node* toNode() override;
            virtual Edge* toEdge() override;
            virtual NodeGroup* toGroup() override;

        private:
            Graph* mGraph;

            the::InitState onInit();
            void onDeinit();
            void onSaveState() override;
    };
}
