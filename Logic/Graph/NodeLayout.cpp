#include "Logic/Graph/NodeLayout.hpp"
#include <The2D/Attached.hpp>

using namespace the;
using namespace mp;



NodeLayout::NodeLayout(): mSize(0.4, 0.17), mMyNode(nullptr), mLastPosition(0,0)
{
    REGISTER(prmPosition).defval((rand() % 10 - 5)/50.0f, (rand() % 10 - 5)/50.0f);
    mBody.prmMassData.mass.defval(1);
    REGISTER(toLayout);
    REGISTER(CPosition)
        .setGetter([&](Vec2& result){ result = toLayout->pointFromBox2D(mBody.getBody()->GetPosition()); })
        .setSetter(std::bind(&NodeLayout::setPosition, this, std::placeholders::_1))
        .setValueChangedEvent(EvPositionChanged);
    REGISTER(CSize)
        .setAccessors(&mSize);
    REGISTER(mBody).setInitOrder(AFTER_PARENT);
    REGISTER(EvPositionChanged);
    REGISTER(prmPositionChangeSensitivity).defval(0.01);

    mBody.prmType.defval(b2_dynamicBody);
    mBody.prmFixedRotation.defval(true);
    mBody.prmLinearDamping.defval(5);
}

void NodeLayout::setPosition(Vec2 position)
{
    mBody.getBody()->SetTransform(toLayout->pointToBox2D(position), mBody.getBody()->GetAngle());
}

void NodeLayout::update()
{
    if ((mBody.getBody()->GetPosition() - mLastPosition).LengthSquared() > prmPositionChangeSensitivity*prmPositionChangeSensitivity)
    {
        EvPositionChanged.raise();
        mLastPosition = mBody.getBody()->GetPosition();
        //INFO("phys pos of %i is (%f, %f) pos is (%f, %f)",this, mPosition.x, mPosition.y, CPosition().x, CPosition().y);
    }
}

the::InitState NodeLayout::onInit()
{
    mBody.prmPosition = toLayout->pointToBox2D(Vec2(prmPosition.x, prmPosition.y));
    return true;
}

void NodeLayout::onDeinit()
{
}

void NodeLayout::onSaveState()
{
    prmPosition.set(CPosition().x, CPosition().y);
}

Node* NodeLayout::getNode()
{
    return mMyNode;
}
void NodeLayout::setNode(Node* node)
{
    mMyNode = node;
}