////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Bind.hpp>
#include <The2D/Component.hpp>
#include <The2D/Parameters.hpp>
#include <The2D/Connectors.hpp>
#include "Logic/Graph/NodeView.hpp"
#include <The2D/CValueVector.hpp>

using namespace the;

class QGraphicsPathItem;

////////////////////////////////////////////////////////////
namespace mp {

    class NodeGroup;
    class NodeGroupView;

    class NodeGroupGraphicsItem : public QGraphicsPathItem
    {
        public:
            NodeGroupGraphicsItem(NodeGroupView* parentComponent);

            static constexpr int NodeGroupGraphicsItemID = 3;

            NodeGroupView* getParentNodeGroupView();

        protected:
            NodeGroupView* mParentComponent;

            bool sceneEvent ( QEvent * event ) override;
    };

    class NodeGroupView : public Component
    {
        public:
            THE_ENTITY(NodeGroupView, Component)

            PFloat prmBorderWidth;
            PColor prmBorderColor;
            PColor prmBorderSelectionColor;

            CValueVectorR<NodeView*> toNodes;
            ManagerBind<SceneManager> toSceneManager;
            
            enum SelectionState
            {
                NOT_SELECTED,
                SELECTED
            };
            CValue<SelectionState> CSelectionState;

            Signal<void(QEvent*)> SigSceneEvent;

            NodeGroupView();
            virtual ~NodeGroupView();

            NodeGroup* getGroup();
            void setGroup(NodeGroup* group);

            NodeGroupGraphicsItem* getSceneItem();


        private:
            NodeGroup* mGroup;
            NodeGroupGraphicsItem* mView;
            eastl::vector<NodeView*> mNodes;

            InitState onInit() override;
            void onDeinit() override;

            void nodesChanged();
            void updateView();
            void updateSelectionState();

    };
}; // namespace mp
////////////////////////////////////////////////////////////