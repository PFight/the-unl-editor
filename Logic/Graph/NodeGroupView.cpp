////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/Config.hpp>
#include "Logic/Graph/NodeGroupView.hpp"

using namespace mp;
using namespace the;

NodeGroupGraphicsItem::NodeGroupGraphicsItem(NodeGroupView* parent): mParentComponent(parent)
{
    setData(0, NodeGroupGraphicsItemID);
}

NodeGroupView* NodeGroupGraphicsItem::getParentNodeGroupView()
{
    return mParentComponent;
}

bool NodeGroupGraphicsItem::sceneEvent ( QEvent * event )
{
    if (event->type() == QEvent::GraphicsSceneHoverEnter)
    {
        mParentComponent->CSelectionState = NodeGroupView::SELECTED;
        INFO("Group hower enter");
    }
    else if (event->type() == QEvent::GraphicsSceneHoverLeave)
    {
        mParentComponent->CSelectionState = NodeGroupView::NOT_SELECTED;
        INFO("Group hower leave");
    }
    mParentComponent->SigSceneEvent(event);
    return QGraphicsPathItem::sceneEvent(event);
}


NodeGroupView::NodeGroupView()
{
    REGISTER(toNodes).onValueChange([&]{ nodesChanged(); });
    REGISTER(toSceneManager);
    REGISTER(prmBorderWidth).defval(15);
    REGISTER(prmBorderColor);
        prmBorderColor.a.defval(255);
        prmBorderColor.r.defval(255);
        prmBorderColor.g.defval(255);
        prmBorderColor.b.defval(255);
    REGISTER(prmBorderSelectionColor);
        prmBorderSelectionColor.a.defval(255);
        prmBorderSelectionColor.r.defval(250);
        prmBorderSelectionColor.g.defval(0);
        prmBorderSelectionColor.b.defval(0);
    REGISTER(CSelectionState).defval(NOT_SELECTED).onValueChange([&] { updateSelectionState(); });
}

NodeGroupView::~NodeGroupView()
{
}

NodeGroup* NodeGroupView::getGroup()
{
    return mGroup;
}
void NodeGroupView::setGroup(NodeGroup* group)
{
    mGroup = group;
}

NodeGroupGraphicsItem* NodeGroupView::getSceneItem()
{
    return mView;
}

void NodeGroupView::onDeinit()
{
    toSceneManager->getScene()->removeItem(mView);
    mView = nullptr;
}

the::InitState NodeGroupView::onInit()
{
    mView = new NodeGroupGraphicsItem(this);
    toSceneManager->getScene()->addItem(mView);
    nodesChanged();
    return READY;
}

void NodeGroupView::nodesChanged()
{
    for (NodeView* node : mNodes)
    {
        node->toPosition.SigValueChanged.remove(this);
    }

    for (NodeView* node : toNodes)
    {
        node->toPosition.SigValueChanged.add([&]{ updateView(); });
        mNodes.push_back(node);
    }
    updateView();
}

void NodeGroupView::updateView()
{
    if (toNodes.size() == 0) return;

    eastl::vector<Vec2> points;
    points.reserve(toNodes.size()*4);
    for (NodeView* node : toNodes)
    {
        // Suppose, that node is rectangle
        // and find it corners
        Vec2 pos = node->toPosition;
        Vec2 size = node->CSize;
        points.push_back(pos); // upper left
        // Y directed up?
        points.push_back(Vec2(pos.x, pos.y + size.y)); // lower left
        points.push_back(Vec2(pos.x + size.x, pos.y + size.y)); // lower right
        points.push_back(Vec2(pos.x + size.x, pos.y)); // upper right
    }

    // We need to find convex hull
    // Use Jarvis march
    eastl::vector<Vec2> hull;
    hull.reserve(toNodes.size());

    // Go march! : )
    // http://en.wikipedia.org/wiki/Gift_wrapping_algorithm
    // jarvis(S)
    //   pointOnHull = leftmost point in S
    //   i = 0
    //   repeat
    //     P[i] = pointOnHull
    //     endpoint = S[0]         // initial endpoint for a candidate edge on the hull
    //     for j from 1 to |S|
    //        if (endpoint == pointOnHull) or (S[j] is on left of line from P[i] to endpoint)
    //           endpoint = S[j]   // found greater left turn, update endpoint
    //     i = i+1
    //     pointOnHull = endpoint
    //   until endpoint == P[0]      // wrapped around to first hull point

    // Find most left
    Vec2 start = points[0];
    for (auto& p : points)
    {
        if (p.x < start.x) start = p;
    }

    int i = 0;
    Vec2 endpoint;
    Vec2 pointOnHull = start;
    do
    {
       hull.push_back(pointOnHull);
       endpoint = points[0]; // initial endpoint for a candidate edge on the hull
       for (int j =0; j< (int)points.size(); j++)
       {
            // S[j] is on left of line from P[i] to endpoint
            Vec2 a = endpoint - hull[i];
            Vec2 b = points[j] - hull[i];
            float dot = a.x*-b.y + a.y*b.x;
            bool toTheLeft = (dot < 0);
            if ((endpoint - pointOnHull).lengthSquared() < 0.1 || toTheLeft)
            {
                 endpoint = points[j]; // found greater left turn, update endpoint
            }
       }
       i = i+1;
       pointOnHull = endpoint;
    }
    while ((endpoint - hull[0]).lengthSquared() > 0.1); // wrapped around to first hull point

    // Scale hull relative to it's center, making border
    Vec2 center = hull[0];
    for (int i = 1; i < (int)hull.size(); i++)
    {
        center = center + hull[i];
    }
    center = (1.0f/hull.size()) * center;

    for (int i =0; i< (int)hull.size(); i++)
    {
        // desired = actual * k = actual + prmBorderWidth
        // k = (actual + prmBorderWidth)/actual = 1 + prmBorderWidth / actual
        Vec2 fromCenter = hull[i] - center;
        float length = fromCenter.length();
        float k = 1 + prmBorderWidth / length;
        hull[i] = center + (k*fromCenter);
    }

    // Create polygon
    QPainterPath path;
    path.moveTo(QPoint(hull[0].x, hull[0].y));
    for (int i =1; i< (int)hull.size(); i++)
    {
        path.lineTo(QPoint(hull[i].x, hull[i].y));
    }
    path.closeSubpath();
    mView->setPath(path);
}

void NodeGroupView::updateSelectionState()
{
    QPen pen = mView->pen();
    if (CSelectionState == SELECTED)
    {
        pen.setColor(QColor(prmBorderSelectionColor.r,
            prmBorderSelectionColor.g, prmBorderSelectionColor.b,
            prmBorderSelectionColor.a));
    }
    else if (CSelectionState == NOT_SELECTED)
    {
        pen.setColor(QColor(prmBorderColor.r,
            prmBorderColor.g, prmBorderColor.b, prmBorderColor.a));
    }
    mView->setPen(pen);
}