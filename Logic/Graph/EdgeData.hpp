#pragma once

#include "The2D/Component.hpp"
#include "Logic/Graph/NodeData.hpp"

using namespace the;

namespace mp
{
    class Edge;

    class EdgeData : public the::Component
    {
        public:
            THE_ENTITY(EdgeData, the::Component)

            PString prmSource;
            PString prmUnl;

            Bind<NodeData> toFirstNode;
            Bind<NodeData> toSecondNode;

            ToBoolR toCurrentMode;
            CString CText;
            CString CUnl;
            CString CSource;

            CEvent EvTextChanged;

            EdgeData();

            Edge* getEdge();
            void setEdge(Edge* edge);

        private:
            Edge* mMyEdge;

            the::InitState onInit();
            void onDeinit();
            void onSaveState() override;

            void modeChanged();
    };
}
