#include "Logic/Graph/EdgeData.hpp"

using namespace mp;
using namespace the;




EdgeData::EdgeData(): mMyEdge(nullptr)
{
    REGISTER(prmSource).defval("*");
    REGISTER(prmUnl).defval("");
    REGISTER(toCurrentMode).defval(true).onValueChange([&]{ modeChanged(); });
    REGISTER(toFirstNode);
    REGISTER(toSecondNode);
    REGISTER(CUnl);
    REGISTER(CSource);
    REGISTER(CText)
        .setValueChangedEvent(EvTextChanged);
    REGISTER(EvTextChanged);
}

the::InitState EdgeData::onInit()
{
    CSource = prmSource();
    CUnl = prmUnl();

    toFirstNode->addEdge(getEdge());
    toSecondNode->addEdge(getEdge());

    modeChanged();

    return true;
}

void EdgeData::onDeinit()
{
    toFirstNode->removeEdge(getEdge());
    toSecondNode->removeEdge(getEdge());
}

void EdgeData::onSaveState()
{
    prmSource = CSource();
    prmUnl = CUnl();
}

Edge* EdgeData::getEdge()
{
    return mMyEdge;
}
void EdgeData::setEdge(Edge* edge)
{
    mMyEdge = edge;
}

void EdgeData::modeChanged()
{
    if (toCurrentMode)
    {
        CText.connect(&CSource);
    }
    else
    {
        if (CUnl() == "")
        {
            CUnl = CSource();
        }
        CText.connect(&CUnl);
    }
}