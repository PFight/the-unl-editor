#include "Logic/Graph/GraphItem.hpp"

using namespace mp;
using namespace the;

GraphItem::GraphItem()
{
}

GraphItem::~GraphItem()
{
}


void GraphItem::setGraph(Graph* graph)
{
    mGraph = graph;
}
Graph* GraphItem::getGraph()
{
    return mGraph;
}