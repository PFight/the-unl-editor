#include "Graph.hpp"

#include <QtWidgets/QMessageBox>

using namespace the;
using namespace mp;



Graph::Graph(): mMaxNodeID(-1), mMaxEdgeID(-1), mSentence("")
{
    REGISTER(toCurrentMode).defval(true);
}

the::InitState Graph::onInit()
{
    return true;
}

void Graph::onDeinit()
{

}

/*
std::shared_ptr<Node> Graph::loadKnot(int knotID)
{
    for(auto &knot : mKnots)
    {
        if(knot->mKnotID == knotID) return knot;
    }

    QSqlQuery getKnot = toDataBaseManager->createQuery();
    getKnot.prepare("SELECT KnotText, KnotTypeName, XCoord, YCoord FROM Knot WHERE KnotID = :knotID");
    getKnot.bindValue(":knotID", knotID);
    getKnot.exec();
    if(getKnot.isActive())
    {
        getKnot.next();
        std::shared_ptr<Node> knot(new Node());
        knot->prmText = getKnot.value(0).toString().toStdString();
        knot->mFromDB = true;
        knot->mKnotID = knotID;
        knot->prmPosition = b2Vec2(getKnot.value(2).toFloat(), getKnot.value(3).toFloat());
        REGISTER(knot);
        if(knotID > mMaxKnotID) mMaxKnotID = knotID;
        mKnots.push_back(knot);

        QSqlQuery getOutEdges  = toDataBaseManager->createQuery();
        getOutEdges.prepare("SELECT EdgeTypeName, End, EdgeText, EdgeID, Length FROM Edge WHERE Begin = :knotID");
        getOutEdges.bindValue(":knotID", knotID);
        getOutEdges.exec();
        while(getOutEdges.next())
        {
            int otherKnotID = getOutEdges.value(1).toInt();
            std::shared_ptr<Node> otherKnot = loadKnot(otherKnotID);
            std::shared_ptr<Edge> edge(new Edge());
            std::string edgeText = getOutEdges.value(2).toString().toStdString();
            edge->prmText = edgeText;
            edge->mFromDB = true;
            float len = getOutEdges.value(4).toFloat();
            edge->prmLength = len;
            edge->prmFrom = knot.get();
            edge->mEdgeID = getOutEdges.value(3).toInt();
            if(edge->mEdgeID > mMaxEdgeID) mMaxEdgeID = edge->mEdgeID;
            knot->mEdges.push_back(edge.get());
            edge->prmTo = otherKnot.get();
            otherKnot->mEdges.push_back(edge.get());
            REGISTER(edge);
            mEdges.push_back(edge);
        }

        return knot;
    }
    else
    {

        std::cout << getKnot.lastError().text().toStdString() << std::endl;
        QMessageBox::critical(nullptr, "Error with data base", "Data base files are corrupted.",
                                        QMessageBox::Default, QMessageBox::Escape );
        core()->exitGame();
        return std::shared_ptr<Node>();
    }
}
*/
bool Graph::saveMap()
{
    bool allOk = true;
    // Update existing knots and insert new
    /*for(auto &knot : mKnots)
    {
        if(knot->mFromDB)
        {
            b2Vec2 prmPos = knot->prmPosition;
            b2Vec2 curPos = knot->getPosition();
            if((knot->prmText  != knot->getText()) ||
               (knot->prmType.name != knot->getType().name) ||
               !(prmPos == curPos))
            {
                QSqlQuery query = toDataBaseManager->createQuery();
                query.prepare("UPDATE Knot SET KnotText=:text, KnotTypeName=:type, XCoord=:x, YCoord=:y WHERE KnotID=:id");
                query.bindValue(":text", knot->getText().c_str());
                query.bindValue(":type", knot->getType().name.c_str());
                query.bindValue(":id", knot->mKnotID);
                query.bindValue(":x", knot->getPosition().x);
                query.bindValue(":y", knot->getPosition().y);
                bool ok = query.exec();
                if(!ok)
                {
                    allOk = false;
                    std::cout << query.lastError().text().toStdString() << std::endl;
                }
            }
        }
        else
        {
            QSqlQuery query = toDataBaseManager->createQuery();
            query.prepare("INSERT INTO Knot (KnotText, KnotTypeName, KnotID, XCoord, YCoord) VALUES (:text, :type, :id, :x, :y)");
            query.bindValue(":text", knot->getText().c_str());
            query.bindValue(":type", knot->getType().name.c_str());
            query.bindValue(":id", knot->mKnotID);
            query.bindValue(":x", knot->getPosition().x);
            query.bindValue(":y", knot->getPosition().y);
            bool ok = query.exec();
            if(!ok)
            {
                allOk = false;
                std::cout << query.lastError().text().toStdString() << std::endl;
            }
        }
    }
    // Remove edges before knots, because otherwise we violate integrity of DB
    for(auto &removedEdge : mRemovedEdges)
    {
        if(removedEdge->mFromDB)
        {
            QSqlQuery query = toDataBaseManager->createQuery();
            query.prepare("DELETE FROM Edge WHERE EdgeID=:id");
            query.bindValue(":id", removedEdge->mEdgeID);
            bool ok = query.exec();

            if(!ok)
            {
                allOk = false;
                std::cout << query.lastError().text().toStdString() << std::endl;
            }
        }
    }
    for(auto &removedKnot : mRemovedKnots)
    {
        if(removedKnot->mFromDB)
        {
            QSqlQuery query = toDataBaseManager->createQuery();
            query.prepare("DELETE FROM Knot WHERE KnotID=:id");
            query.bindValue(":id", removedKnot->mKnotID);
            bool ok = query.exec();
            if(!ok)
            {
                allOk = false;
                std::cout << query.lastError().text().toStdString() << std::endl;
            }
        }
    }
    // Update existing edges and insert new
    for(auto &edge : mEdges)
    {
        Edge* edgeC = edge.get();
        if(edge->mFromDB)
        {
            if(edge->prmText  != edge->getText() || edge->prmType.name != edge->getType().name)
            {
                QSqlQuery query = toDataBaseManager->createQuery();
                query.prepare("UPDATE Edge SET EdgeText=:text, EdgeTypeName=:type, Length=:l WHERE EdgeID=:id");
                query.bindValue(":text", edge->getText().c_str());
                query.bindValue(":type", edge->getType().name.c_str());
                query.bindValue(":id", edge->mEdgeID);
                query.bindValue(":l", edge->getLength());
                bool ok = query.exec();
                if(!ok)
                {
                    allOk = false;
                    std::cout << query.lastError().text().toStdString() << std::endl;
                }
            }
        }
        else
        {
            QSqlQuery query = toDataBaseManager->createQuery();
            query.prepare("INSERT INTO Edge (EdgeText, EdgeTypeName, EdgeID, Begin, End, Length) VALUES (:text, :type, :id, :begin, :end, :l)");
            query.bindValue(":text", edge->getText().c_str());
            query.bindValue(":type", edge->getType().name.c_str());
            query.bindValue(":id", edge->mEdgeID);
            query.bindValue(":begin", edge->prmFrom->mKnotID);
            query.bindValue(":end", edge->prmTo->mKnotID);
            float len = edge->getLength();
            query.bindValue(":l", edge->getLength());
            bool ok = query.exec();

            if(!ok)
            {
                allOk = false;
                std::cout << query.lastError().text().toStdString() << std::endl;
            }
        }
    }
    if(!allOk)
    {
        QMessageBox::critical(nullptr, "Error with data base", "Data base files are corrupted. Not all data was saved.",
                                        QMessageBox::Default, QMessageBox::Escape );
    }*/
    return allOk;
}


bool Graph::mapHasChanges()
{
    return true;
    /*for(auto &knot : mKnots)
    {
        if(knot->mFromDB)
        {
            if(knot->prmText  != knot->getText() ||
               knot->prmType.name != knot->getType().name ||
                !(knot->prmPosition == knot->getPosition()))
            {
                return true;
            }
        }
        else
        {
            return true;
        }
    }
    for(auto &removedEdge : mRemovedEdges)
    {
        if(removedEdge->mFromDB)
        {
            return true;
        }
    }
    for(auto &removedKnot : mRemovedKnots)
    {
        if(removedKnot->mFromDB)
        {
            return true;
        }
    }
    for(auto &edge : mEdges)
    {
        if(edge->mFromDB)
        {
            if(edge->prmText  != edge->getText() ||
               edge->prmType.name != edge->getType().name ||
               edge->prmLength != edge->getLength())
            {
                return true;
            }
        }
        else
        {
            return true;
        }
    }
    return false;*/
}


void Graph::addNode(std::shared_ptr<Node> node, bool init)
{
    if(node->getName().empty())
    {
        node->setName(generateNodeID());
    }
    registerChild(node.get());
    node->setGraph(this);
    node->mData.toCurrentMode.connect(&toCurrentMode);
    mNodes.push_back(node);
    //core()->initService()->enableDebugLog(true);
    if(init) node->init();
    //core()->initService()->enableDebugLog(false);

    SigNodeAdded(node.get());
}

void Graph::addEdge(std::shared_ptr<Edge> edge, bool init)
{
    if(edge->getName().empty())
    {
        edge->setName(generateEdgeID());
    }
    registerChild(edge.get());
    edge->setGraph(this);
    edge->mData.toCurrentMode.connect(&toCurrentMode);
    mEdges.push_back(edge);
    if(init) edge->init();

    SigEdgeAdded(edge.get());
}

void Graph::addNodeGroup(std::shared_ptr<NodeGroup> group, bool init)
{
    if(group->getName().empty())
    {
        group->setName(generateNodeID());
    }
    group->setGraph(this);
    manageStateChild(group);
    //core()->initService()->enableDebugLog(true);
    if(init) group->init();
    //core()->initService()->enableDebugLog(false);
}

template <typename T>
class SptrEqualToPtr
{
public:
    T* ptr;

    SptrEqualToPtr(T* p) { ptr = p; }

    bool operator() (const std::shared_ptr<T>& value) {return value.get() == ptr; }
};

void Graph::removeNode(Node* node, bool deinit)
{
    while(node->mData.getEdges().size() > 0)
    {
        removeEdge(node->mData.getEdges().back());
    }

    if(deinit) node->deinit();

    unregisterChild(node);
    node->setGraph(nullptr);

    // Remove from mKnots, but keep in mRemovedKnots
    auto toRemoveIt = std::find_if(mNodes.begin(), mNodes.end(), SptrEqualToPtr<Node>(node));
    if(toRemoveIt != mNodes.end())
    {
        mRemovedKnots.push_back(*toRemoveIt);
        mNodes.erase(toRemoveIt);
    }

    SigNodeRemoved(node);
}

void Graph::removeEdge(Edge* edge, bool deinit)
{
    if(deinit) edge->deinit();

    unregisterChild(edge);
    edge->setGraph(nullptr);

    // Remove from mEdges, but keep in mRemovedEdges
    auto toRemoveIt = std::find_if(mEdges.begin(), mEdges.end(), SptrEqualToPtr<Edge>(edge));
    if(toRemoveIt != mEdges.end())
    {
        mRemovedEdges.push_back(*toRemoveIt);
        mEdges.erase(toRemoveIt);
    }

    SigEdgeRemoved(edge);
}

void Graph::remove(GraphItem* item, bool deinit)
{
    if (Node* node = item->toNode())
    {
        removeNode(node, deinit);
    }
    else if (Edge* edge = item->toEdge())
    {
        removeEdge(edge, deinit);
    }
}

string Graph::generateNodeID()
{
    return the::lexical_cast<string>(++mMaxNodeID);
}
string Graph::generateEdgeID()
{
    return the::lexical_cast<string>(++mMaxEdgeID);
}

