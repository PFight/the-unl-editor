////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Bind.hpp>
#include <The2D/Component.hpp>
#include <The2D/Parameters.hpp>
#include <The2D/Connectors.hpp>

using namespace the;

////////////////////////////////////////////////////////////
namespace mp {

    class NodeGroup;
    
    class NodeGroupLayout : public Component
    {
        public:
            THE_ENTITY(NodeGroupLayout, Component)

            NodeGroupLayout();
            virtual ~NodeGroupLayout();

            NodeGroup* getGroup();
            void setGroup(NodeGroup* group);

        private:
            NodeGroup* mGroup;
            
            InitState onInit() override;
            void onDeinit() override;
            
    };
}; // namespace mp
////////////////////////////////////////////////////////////