////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Bind.hpp>
#include <The2D/Component.hpp>
#include <The2D/Parameters.hpp>
#include <The2D/Connectors.hpp>
#include "Logic/Graph/NodeData.hpp"
#include <The2D/CValueVector.hpp>

using namespace the;

////////////////////////////////////////////////////////////
namespace mp {

    class NodeGroup;

    class NodeGroupData : public Component
    {
        public:
            THE_ENTITY(NodeGroupData, Component)

            ToValueVectorR<NodeData*> toNodes;

            NodeGroupData();
            virtual ~NodeGroupData();

            NodeGroup* getGroup();
            void setGroup(NodeGroup* group);

        private:
            NodeGroup* mGroup;

            InitState onInit() override;
            void onDeinit() override;

    };
}; // namespace mp
////////////////////////////////////////////////////////////