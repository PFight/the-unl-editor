#include "Logic/Graph/EdgeView.hpp"
#include <qgraphicsproxywidget.h>

using namespace the;
using namespace mp;



EdgeGraphicsItem::EdgeGraphicsItem(EdgeView* parent): mParentComponent(parent)
{
    setData(0, EdgeGraphicsItemID);
}

EdgeView* EdgeGraphicsItem::getParentEdgeView()
{
    return mParentComponent;
}

bool EdgeGraphicsItem::sceneEvent ( QEvent * event )
{
    mParentComponent->SigSceneEvent(event);
    return QGraphicsProxyWidget::sceneEvent(event);
}

EdgeView::EdgeView(): mLabel(nullptr), mLabelView(nullptr), mLine(nullptr), mMyEdge(nullptr)
{
    REGISTER(toFirstNode);
    REGISTER(toSecondNode);
    REGISTER(toSceneManager);
    REGISTER(toText).defval("edge text not bound");

    REGISTER(prmLabelNotSelectedStyle).defval("color : black; font: normal;");
    REGISTER(prmLabelSelectedStyle).defval("color : red; font: bold;");

    REGISTER(CSelectionState)
        .setValueChangedEvent(EvSelectionStateChanged)
        .set(NOT_SELECTED);
        
    REGISTER(EvSelectionStateChanged);

    REGISTER(DoUpdatePosition).setFunc(std::bind(&EdgeView::updatePosition, this));
    REGISTER(DoUpdateState)
        .setFunc(std::bind(&EdgeView::updateState, this))
        .connect(&EvSelectionStateChanged);
}

the::InitState EdgeView::onInit()
{
    mLabel = new QLabel();
    mLabelView = new EdgeGraphicsItem(this);
    mLabelView->setWidget(mLabel);
    toSceneManager->getScene()->addItem(mLabelView);
    mLine = new QGraphicsLineItem();
    toSceneManager->getScene()->addItem(mLine);

    setInitState(READY);
    updateState();

    return true;
}

void EdgeView::onDeinit()
{
    toSceneManager->getScene()->removeItem(mLabelView);
    mLabelView = nullptr;
    mLabel = nullptr;
    toSceneManager->getScene()->removeItem(mLine);
    mLine = nullptr;    
}

void EdgeView::updatePosition()
{
    if (isInitialized())
    {
        auto rect = mLine->boundingRect().united(mLabelView->boundingRect());

        mLine->setLine(toFirstNode->toPosition().x, toFirstNode->toPosition().y,
                      toSecondNode->toPosition().x, toSecondNode->toPosition().y);
        Vec2 labelPos = 0.5f * (toFirstNode->toPosition() + toSecondNode->toPosition());
        mLabelView->setPos(labelPos.x, labelPos.y);

        toSceneManager->getView()->update(rect.toRect());
    }
}

void EdgeView::updateState()
{
    if (isInitialized())
    {
        mLabel->setText(toText().c_str());

        string style = prmLabelNotSelectedStyle;
        switch(CSelectionState())
        {
            CASE(SELECTED)
                style = prmLabelSelectedStyle;
            END_CASE;
            CASE(NOT_SELECTED)
                style = prmLabelNotSelectedStyle;
            END_CASE;
        }
        mLabel->setStyleSheet(style.c_str());
        mLabelView->update(mLabelView->boundingRect());

        updatePosition();
    }
}

Edge* EdgeView::getEdge()
{
    return mMyEdge;
}
void EdgeView::setEdge(Edge* edge)
{
    mMyEdge = edge;
}

EdgeGraphicsItem* EdgeView::getSceneItem()
{
    return mLabelView;
}