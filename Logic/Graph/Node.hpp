#pragma once

#include "Logic/Graph/NodeData.hpp"
#include "Logic/Graph/NodeLayout.hpp"
#include "Logic/Graph/NodeView.hpp"
#include "Logic/Graph/GraphItem.hpp"

using namespace the;

namespace mp
{
    class Edge;
    class Graph;

    class Node : public GraphItem
    {
        public:
            THE_ENTITY(Node, GraphItem)

            NodeData mData;
            NodeView mView;
            NodeLayout mLayout;

            Node();

            virtual bool isNode() override;
            virtual bool isEdge() override;
            virtual bool isGroup() override;

            virtual Node* toNode() override;
            virtual Edge* toEdge() override;
            virtual NodeGroup* toGroup() override;

        private:

            void onPreInit();
            void onDeinit();
    };
}
