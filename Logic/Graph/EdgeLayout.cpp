#include "Logic/Graph/EdgeLayout.hpp"

using namespace the;
using namespace mp;




EdgeLayout::EdgeLayout(): mMyEdge(nullptr)
{
    REGISTER(toFirstNodeBody);
    REGISTER(toSecondNodeBody);

    REGISTER(prmDefaultEdgeLength).defval(1.2f);
    REGISTER(prmAutoLength).defval(false);
    mJoint.prmFrequencyHz.defval(0.5);
    mJoint.prmDampingRatio.defval(1);
    mJoint.prmCollideConnected.defval(false);

    REGISTER(CNominalLength)
        .setValueChangedEvent(EvNominalLengthChanged)
        .setSetter(std::bind(&EdgeLayout::setNominalLength, this, std::placeholders::_1))
        .setGetter(&mNominalLength)
        .set(prmDefaultEdgeLength.getDefaultValue());
    REGISTER(EvNominalLengthChanged);

    REGISTER(mJoint).setInitOrder(the::AFTER_PARENT);
}

the::InitState EdgeLayout::onInit()
{
    mJoint.prmLength = CNominalLength();
    if (prmAutoLength())
    {
        mJoint.prmForceAutoLength = true;
    }
    mJoint.toBodyA.set(toFirstNodeBody());
    mJoint.toBodyB.set(toSecondNodeBody());

    return true;
}

void EdgeLayout::onDeinit()
{
}

void EdgeLayout::setNominalLength(float val)
{
    if (isInitialized())
    {
        mJoint.deinit();
        mJoint.prmLength = val;
        mJoint.init();
    }
    mNominalLength = val;
}

Edge* EdgeLayout::getEdge()
{
    return mMyEdge;
}
void EdgeLayout::setEdge(Edge* edge)
{
    mMyEdge = edge;
}
