#pragma once

#include <The2D/Component.hpp>
#include <The2D/Connectors.hpp>

#include "Logic/Graph/NodeView.hpp"
#include "Logic/SceneManager.hpp"

#include <QtWidgets/QLabel>
#include <QtWidgets/QGraphicsLineItem>

using namespace the;

namespace mp
{
    class Edge;
    class EdgeView;

    class EdgeGraphicsItem : public QGraphicsProxyWidget
    {
        Q_OBJECT
        public:
            EdgeGraphicsItem(EdgeView* parentComponent);

            static constexpr int EdgeGraphicsItemID = 2;

            EdgeView* getParentEdgeView();

        protected:
            EdgeView* mParentComponent;

            bool sceneEvent ( QEvent * event ) override;
    };

    class EdgeView : public the::Component
    {
        public:
            THE_ENTITY(EdgeView, the::Component)

            enum SelectionState
            {
                NOT_SELECTED = 0,
                SELECTED=1
            };

            Bind<NodeView> toFirstNode;
            Bind<NodeView> toSecondNode;
            ManagerBind<SceneManager> toSceneManager;
            CString toText;

            PString prmLabelNotSelectedStyle;
            PString prmLabelSelectedStyle;

            CValue<SelectionState> CSelectionState;
            CEvent EvSelectionStateChanged;

            /// \brief Fast update only position (do not touch text and other).
            CCommand DoUpdatePosition;
            /// \brief Update text, selection state and position.
            CCommand DoUpdateState;

            Signal<void(QEvent*)> SigSceneEvent;

            EdgeView();

            Edge* getEdge();
            void setEdge(Edge* edge);

            EdgeGraphicsItem* getSceneItem();

        private:
            QLabel* mLabel;
            EdgeGraphicsItem* mLabelView;
            QGraphicsLineItem* mLine;
            Edge* mMyEdge;

            the::InitState onInit();
            void onDeinit();

            void updatePosition();
            void updateState();
    };

}