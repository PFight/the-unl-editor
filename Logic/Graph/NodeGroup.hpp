////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Bind.hpp>
#include "Logic/Graph/GraphItem.hpp"
#include <The2D/Parameters.hpp>
#include <The2D/Converters.hpp>
#include "Logic/Graph/NodeGroupLayout.hpp"
#include "Logic/Graph/NodeGroupView.hpp"
#include "Logic/Graph/NodeGroupData.hpp"
#include <The2D/CValueVector.hpp>
#include <The2D/CValueVectorConversion.hpp>
#include <The2D/BindList.hpp>

using namespace the;

////////////////////////////////////////////////////////////
namespace mp {

    class NodeGroup : public GraphItem
    {
        public:
            THE_ENTITY(NodeGroup, GraphItem)

            BindList<Node> toNodes;

            CValueVector<Node*> CNodes;
            CValueVectorConversion<Node*, NodeData*> GetNodesDatas;
            CValueVectorConversion<Node*, NodeView*> GetNodesViews;

            NodeGroupLayout mLayout;
            NodeGroupView mView;
            NodeGroupData mData;

            NodeGroup();
            virtual ~NodeGroup();

            virtual bool isNode();
            virtual bool isEdge();
            virtual bool isGroup();

            virtual Node* toNode();
            virtual Edge* toEdge();
            virtual NodeGroup* toGroup();
        private:

            void onSaveState() override;

            InitState onInit() override;
            void onDeinit() override;
            
    };
}; // namespace mp
////////////////////////////////////////////////////////////