
#include "Logic/Graph/NodeView.hpp"
#include <QtWidgets/QGraphicsProxyWidget>
#include <QDebug>

using namespace the;
using namespace mp;



NodeGraphicsItem::NodeGraphicsItem(NodeView* parent): mParentComponent(parent)
{
    setData(0, NodeGraphicsItemID);
}

NodeView* NodeGraphicsItem::getParentNodeView()
{
    return mParentComponent;
}

bool NodeGraphicsItem::sceneEvent ( QEvent * event )
{
    mParentComponent->SigSceneEvent(event);
    return QGraphicsProxyWidget::sceneEvent(event);
}

NodeView::NodeView(): mLabel(nullptr), mView(nullptr), mSize(0,0), mMyNode(nullptr)
{
    REGISTER(toSceneManager);
    REGISTER(toPosition).defval(Vec2(0,0));
    REGISTER(toText).defval("text not bound");
    REGISTER(CSelectionState)
        .setValueChangedEvent(&EvSelectionStateChanged)
        .set(NOT_SELECTED);
    REGISTER(EvSelectionStateChanged);

    REGISTER(prmNotSelectedStyle).defval("color : black; font: normal;");
    REGISTER(prmHalfSelectedStyle).defval("color : blue; font: bold;");
    REGISTER(prmSelectedStyle).defval("color : red; font: bold;");

    //REGISTER(CAutoSize).set(false);
    REGISTER(CSize)
        .setGetter(std::bind(&NodeView::getSize, this, std::placeholders::_1));
        //.setSetter(std::bind(&NodeDraw::setSize, this, std::placeholders::_1));
    REGISTER(DoUpdateState)
        .setFunc(std::bind(&NodeView::updateState, this))
        .connect(&EvSelectionStateChanged);
    REGISTER(DoUpdatePosition).setFunc(std::bind(&NodeView::updatePosition, this));

}

the::InitState NodeView::onInit()
{
    mLabel = new QLabel();
    mView = new NodeGraphicsItem(this);
    mView->setWidget(mLabel);
    toSceneManager->getScene()->addItem(mView);
    setInitState(READY);
    updateState();
    updatePosition();
    return READY;
}
void NodeView::onDeinit()
{
    delete mLabel;
    mLabel = nullptr;
    toSceneManager->getScene()->removeItem(mView);
    mView = nullptr;
}

void NodeView::getSize(Vec2& result)
{
    if (isInitialized())
    {
        result = Vec2(mLabel->size().width(), mLabel->size().height());
    }
    else
    {
        result = mSize;
    }
}
/*void NodeDraw::setSize(Vec2 size)
{
    ASSERT(!CAutoSize(), "Disable AutoSize first.");

    mSize = size;
    mLabel.setFixedSize(size.x, size.y);
}*/

void NodeView::updateState()
{
    if (isInitialized())
    {
        mLabel->setText(QString(toText().c_str()));

        string style = prmNotSelectedStyle;
        SelectionState state = CSelectionState;
        switch(state)
        {
            CASE(SELECTED)
                style = prmSelectedStyle;
            END_CASE
            CASE(HALF_SELECTED)
                style = prmHalfSelectedStyle;
            END_CASE
            CASE(NOT_SELECTED)
                style = prmNotSelectedStyle;
            END_CASE
        }
        mLabel->setStyleSheet(style.c_str());

        updatePosition();
    }
}
void NodeView::updatePosition()
{
    if (isInitialized())
    {
        auto rect = mView->boundingRect();
        mView->setPos(toPosition().x, toPosition().y);
        toSceneManager->getView()->update(rect.toRect());
        // INFO("node view pos (%f, %f)", toPosition().x, toPosition().y);
    }
}

Node* NodeView::getNode()
{
    return mMyNode;
}
void NodeView::setNode(Node* node)
{
    mMyNode = node;
}

NodeGraphicsItem* NodeView::getSceneItem()
{
    return mView;
}