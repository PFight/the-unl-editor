////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Component.hpp>
#include <The2D/Parameters.hpp>
#include <The2D/Connectors.hpp>

using namespace the;

////////////////////////////////////////////////////////////
namespace mp {

    class Node;
    class Edge;
    class Graph;
    class NodeGroup;

    class GraphHelper : public the::Component
    {
        public:
            THE_ENTITY(GraphHelper, the::Component)

            ToBoolR toCurrentMode;

            PMedia prmNodeTemplate;
            PMedia prmEdgeTemplate;

            CFunc<sptr<Graph>()> CreateGraph;
            CFunc<sptr<NodeGroup>()> CreateNodeGroup;

            GraphHelper();
            virtual ~GraphHelper();

            sptr<Node> createNode();
            sptr<NodeGroup> createNodeGroup();
            sptr<Edge> createEdge(Node* from, Node* to);
            sptr<Graph> createGraph();

        private:
            InitState onInit() override;
            void onDeinit() override;

    };
}; // namespace mp
////////////////////////////////////////////////////////////
