#pragma once

#include "Box2DSupport/Body.hpp"
#include "Box2DStc/Magnet.hpp"
#include <The2D/Vec2.hpp>
#include "Logic/LayoutManager.hpp"

using namespace the;

namespace mp
{
    class Node;

    class NodeLayout : public the::Component
    {
        public:
            THE_ENTITY(NodeLayout, the::Component)
                    
            ManagerBind<LayoutManager> toLayout;

            PPoint prmPosition;
            PFloat prmPositionChangeSensitivity;

            the::Body mBody;

            CVec2 CPosition;
            CVec2 CSize;

            CEvent EvPositionChanged;

            NodeLayout();

            void update() override;

            Node* getNode();
            void setNode(Node* node);

        private:
            Vec2 mSize;
            Node* mMyNode;
            b2Vec2 mLastPosition;

            Vec2 getPosition();
            void setPosition(Vec2 position);

            the::InitState onInit();
            void onDeinit();
            void onSaveState() override;
    };
}
