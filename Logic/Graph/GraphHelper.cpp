////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/Config.hpp>
#include <The2D/XmlLoader.hpp>
#include "Logic/Graph/GraphHelper.hpp"
#include "Logic/Graph/Node.hpp"
#include "Logic/Graph/Edge.hpp"
#include "Graph.hpp"
#include "Base/Utils.hpp"

using namespace mp;
using namespace the;



GraphHelper::GraphHelper()
{
    REGISTER(toCurrentMode);
    REGISTER(CreateGraph).setFunc(MY_FUNC(createGraph));
    REGISTER(prmEdgeTemplate).defval("Data/EdgeTemplate.xml");
    REGISTER(prmNodeTemplate).defval("Data/NodeTemplate.xml");
    REGISTER(CreateNodeGroup).setFunc(MY_FUNC(createNodeGroup));
}

GraphHelper::~GraphHelper()
{
}

void GraphHelper::onDeinit()
{

}

the::InitState GraphHelper::onInit()
{
    core()->rtti()->registerType<mp::Node>();
    core()->rtti()->registerType<mp::NodeData>();
    core()->rtti()->registerType<mp::NodeView>();
    core()->rtti()->registerType<mp::NodeLayout>();
    core()->rtti()->registerType<mp::Edge>();
    core()->rtti()->registerType<mp::EdgeData>();
    core()->rtti()->registerType<mp::EdgeView>();
    core()->rtti()->registerType<mp::EdgeLayout>();

    return true;
}

sptr<Node> GraphHelper::createNode()
{
    ASSERT(isInitialized(), "");

    std::shared_ptr<Node> nodeEnt(new Node());

    // TODO: load settings from xml
    
    //core()->xml()->parse(prmNodeTemplate(), nodeEnt.get());
    return nodeEnt;
}

sptr<NodeGroup> GraphHelper::createNodeGroup()
{
    std::shared_ptr<NodeGroup> group(new NodeGroup());
    return group;
}

sptr<Edge> GraphHelper::createEdge(Node* from, Node* to)
{
    ASSERT(isInitialized(), "");
    
    std::shared_ptr<Edge> edge(new Edge());
    edge->toFirstNode.set(from);
    edge->toSecondNode.set(to);

    // TODO: load settings from xml
    
    return edge;
}

sptr<Graph> GraphHelper::createGraph()
{
    Graph* g = new Graph();
    g->toCurrentMode.connect(&toCurrentMode);
    return make_sptr(g);
}
