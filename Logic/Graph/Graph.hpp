#pragma once

#include "Logic/Graph/Node.hpp"
#include "Logic/Graph/Edge.hpp"
#include "Logic/Graph/NodeGroup.hpp"
#include <The2D/GameTree.hpp>
#include <list>

namespace mp
{
    class Graph : public the::GameTree
    {
        public:
            THE_ENTITY(Graph, the::GameTree)

            /// \brief Value, that determines what is mode active - adapted graph (true) or UNL (false).
            ToBoolR toCurrentMode;

            Signal<void(Node*)> SigNodeAdded;
            Signal<void(Node*)> SigNodeRemoved;
            Signal<void(Edge*)> SigEdgeAdded;
            Signal<void(Edge*)> SigEdgeRemoved;


            Graph();

            std::list<std::shared_ptr<Node>>& getNodes(){ return mNodes; }
            std::list<std::shared_ptr<Edge>>& getEdges() { return mEdges; }

            void addNode(std::shared_ptr<Node> knot, bool init=true);
            void addEdge(std::shared_ptr<Edge> edge, bool init=true);
            void addNodeGroup(std::shared_ptr<NodeGroup> knot, bool init=true);
            void removeNode(Node* knot, bool deinit=true);
            void removeEdge(Edge* edge, bool deinit=true);
            void remove(GraphItem* item, bool deinit=true);

            bool saveMap();
            bool mapHasChanges();

            string generateNodeID();
            string generateEdgeID();

            const QString& getSentence() const
            {
                return mSentence;
            }
            void setSentence(const QString& sentence)
            {
                mSentence = sentence;
            }

        private:
            int mMaxNodeID;
            int mMaxEdgeID;
            std::list<std::shared_ptr<Node>> mNodes;
            std::list<std::shared_ptr<Edge>> mEdges;

            std::list<std::shared_ptr<Node>> mRemovedKnots;
            std::list<std::shared_ptr<Edge>> mRemovedEdges;

            QString mSentence;

            the::InitState onInit();
            void onDeinit();
    };
}
