#include "Logic/Graph/Edge.hpp"

using namespace mp;
using namespace the;



Edge::Edge()
{
    REGISTER(mData)
        .setInitOrder(AFTER_PARENT);
        mData.setEdge(this);
    REGISTER(mView)
        .setInitOrder(AFTER_PARENT);
        mView.setEdge(this);
    REGISTER(mLayout)
        .setInitOrder(AFTER_PARENT);
        mLayout.setEdge(this);
    REGISTER(toFirstNode);
    REGISTER(toSecondNode);
}

the::InitState Edge::onInit()
{
    mView.toText.connect(&mData.CText);

    Node* from = toFirstNode();
    Node* to = toSecondNode();
    mLayout.toFirstNodeBody.set(&from->mLayout.mBody);
    mLayout.toSecondNodeBody.set(&to->mLayout.mBody);
    mView.toFirstNode.set(&from->mView);
    mView.toSecondNode.set(&to->mView);
    mView.DoUpdatePosition.connect(&from->mLayout.EvPositionChanged);
    mView.DoUpdatePosition.connect(&to->mLayout.EvPositionChanged);
    mView.DoUpdateState.connect(&mData.EvTextChanged);
    mData.toFirstNode.set(&from->mData);
    mData.toSecondNode.set(&to->mData);

    return true;
}

void Edge::onDeinit()
{
    Node* from = toFirstNode();
    Node* to = toSecondNode();
    mView.DoUpdatePosition.disconnect(&from->mLayout.EvPositionChanged);
    mView.DoUpdatePosition.disconnect(&to->mLayout.EvPositionChanged);
    mView.DoUpdateState.disconnect(&mData.EvTextChanged);
}

bool Edge::isNode()
{
    return false;
}
bool Edge::isEdge()
{
    return true;
}
bool Edge::isGroup()
{
    return false;
}

Node* Edge::toNode()
{
    return nullptr;
}
Edge* Edge::toEdge()
{
    return this;
}
NodeGroup* Edge::toGroup()
{
    return nullptr;
}

void Edge::onSaveState()
{
    toFirstNode.path = toFirstNode->getName();
    toSecondNode.path = toSecondNode->getName();
}