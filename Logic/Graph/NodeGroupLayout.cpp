////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/Config.hpp>
#include "Logic/Graph/NodeGroupLayout.hpp"

using namespace mp;
using namespace the;

NodeGroupLayout::NodeGroupLayout()
{
}

NodeGroupLayout::~NodeGroupLayout()
{
}

NodeGroup* NodeGroupLayout::getGroup()
{
    return mGroup;
}
void NodeGroupLayout::setGroup(NodeGroup* group)
{
    mGroup = group;
}

void NodeGroupLayout::onDeinit()
{
}

the::InitState NodeGroupLayout::onInit()
{
    return READY;
}