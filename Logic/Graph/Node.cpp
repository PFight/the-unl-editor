#include "Logic/Graph/Node.hpp"
#include <The2D/Attached.hpp>

using namespace the;
using namespace mp;



Node::Node()
{
    REGISTER(mData)
        .setInitOrder(AFTER_PARENT);
        mData.setNode(this);
    REGISTER(mView)
        .setInitOrder(AFTER_PARENT);
        mView.setNode(this);
    REGISTER(mLayout)
        .setInitOrder(AFTER_PARENT);
        mLayout.setNode(this);
}

void Node::onPreInit()
{
    mView.toText.connect(&mData.CText);
    mView.toPosition.connect(&mLayout.CPosition);
    mView.DoUpdatePosition.connect(&mLayout.EvPositionChanged);
    mView.DoUpdateState.connect(&mData.EvTextChanged);
    mView.DoUpdateState.connect(&mData.EvEdgesChanged);
}

bool Node::isNode()
{
    return true;
}
bool Node::isEdge()
{
    return false;
}
bool Node::isGroup()
{
    return false;
}

Node* Node::toNode()
{
    return this;
}
Edge* Node::toEdge()
{
    return nullptr;
}
NodeGroup* Node::toGroup()
{
    return nullptr;
}

void Node::onDeinit()
{
    mView.toText.disconnect();
    mView.toPosition.disconnect();
    mView.DoUpdatePosition.disconnect(&mLayout.EvPositionChanged);
    mView.DoUpdateState.disconnect(&mData.EvTextChanged);
    mView.DoUpdateState.disconnect(&mData.EvEdgesChanged);
}
