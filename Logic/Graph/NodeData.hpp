#pragma once

#include <The2D/Component.hpp>
#include <The2D/Connectors.hpp>

using namespace the;

namespace mp
{
    class Edge;
    class Node;

    class NodeData : public the::Component
    {
        public:
            THE_ENTITY(NodeData, the::Component)

            PString prmSource;
            PString prmUnl;

            /// \brief Value, that determines what is mode active - adapted graph (true) or UNL (false).
            ToBoolR toCurrentMode;
            CString CText;
            CString CUnl;
            CString CSource;

            CEvent EvTextChanged;
            CEvent EvEdgesChanged;

            NodeData();

            void addEdge(Edge* edge);
            void removeEdge(Edge* edge);
            eastl::list<Edge*>& getEdges();

            Node* getNode();
            void setNode(Node* node);

        private:
            eastl::list<Edge*> mEdges;
            Node* mMyNode;

            the::InitState onInit();
            void onDeinit();
            void onSaveState() override;

            void modeChanged();
    };
}
