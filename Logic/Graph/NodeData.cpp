#include "Logic/Graph/NodeData.hpp"
#include <The2D/Attached.hpp>

using namespace the;
using namespace mp;



NodeData::NodeData(): mMyNode(nullptr)
{
    REGISTER(prmSource).defval("Node");
    REGISTER(prmUnl).defval("");
    REGISTER(toCurrentMode).defval(true).onValueChange([&]{ modeChanged(); });
    REGISTER(CText)
        .setValueChangedEvent(EvTextChanged);
    REGISTER(CUnl);
    REGISTER(CSource);
    REGISTER(EvTextChanged);
    REGISTER(EvEdgesChanged);
}

void NodeData::addEdge(Edge* edge)
{
    mEdges.push_back(edge);
    EvEdgesChanged.raise();
}
void NodeData::removeEdge(Edge* edge)
{
    mEdges.remove(edge);
    EvEdgesChanged.raise();
}
eastl::list<Edge*>& NodeData::getEdges()
{
    return mEdges;
}
the::InitState NodeData::onInit()
{
    CSource = prmSource();
    CUnl = prmUnl();
    modeChanged();

    return true;
}
void NodeData::onDeinit()
{
}

void NodeData::onSaveState()
{
    prmSource = CSource();
    prmUnl = CUnl();
}

Node* NodeData::getNode()
{
    return mMyNode;
}
void NodeData::setNode(Node* node)
{
    mMyNode = node;
}

void NodeData::modeChanged()
{
    if (toCurrentMode)
    {
        CText.connect(&CSource);
    }
    else
    {
        if (CUnl() == "")
        {
            CUnl = CSource();
        }
        CText.connect(&CUnl);
    }
}