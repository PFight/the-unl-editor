////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/Config.hpp>
#include "Logic/Graph/NodeGroup.hpp"
#include "Logic/Graph/Node.hpp"

using namespace mp;
using namespace the;

NodeGroup::NodeGroup()
{
    REGISTER(mLayout);
    REGISTER(mView).setInitOrder(BEFORE_PARENT);
    REGISTER(mData);
    REGISTER(toNodes);
    REGISTER(CNodes);
    REGISTER(GetNodesDatas).setForwardItemConv([&](Node* node){ return &node->mData; });
    REGISTER(GetNodesViews).setForwardItemConv([&](Node* node){ return &node->mView; });

    mData.toNodes.connect(&CNodes, &GetNodesDatas);
    mView.toNodes.connect(&CNodes, &GetNodesViews);
}

bool NodeGroup::isNode()
{
    return false;
}
bool NodeGroup::isEdge()
{
    return false;
}
bool NodeGroup::isGroup()
{
    return true;
}

Node* NodeGroup::toNode()
{
    return nullptr;
}
Edge* NodeGroup::toEdge()
{
    return nullptr;
}
NodeGroup* NodeGroup::toGroup()
{
    return this;
}

NodeGroup::~NodeGroup()
{
}

void NodeGroup::onSaveState()
{
    std::vector<Bind<Node>*> binds = toNodes();
    for (Bind<Node>* bind : binds)
    {
        bind->deinit();
        toNodes.remove(bind);
    }
    toNodes.clear();
    for (Node* node : CNodes)
    {
        toNodes.addTarget(node);
    }

    for (Bind<Node>* bind : toNodes())
    {
        bind->path = bind->Get()->getName();
    }
}

void NodeGroup::onDeinit()
{
    CNodes.clear();
}

the::InitState NodeGroup::onInit()
{
    CNodes = toNodes.getTargets();

    return READY;
}