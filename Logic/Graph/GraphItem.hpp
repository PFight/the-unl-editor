#pragma once

#include <The2D/Component.hpp>

using namespace the;

namespace mp
{
    class Edge;
    class Node;
    class Graph;
    class NodeGroup;

    class GraphItem : public the::Component
    {
        public:
            THE_ABSTRACT_ENTITY(GraphItem, Component);

            GraphItem();
            virtual ~GraphItem();

            void setGraph(Graph* graph);
            Graph* getGraph();

            virtual bool isNode() = 0;
            virtual bool isEdge() = 0;
            virtual bool isGroup() = 0;

            virtual Node* toNode() = 0;
            virtual Edge* toEdge() = 0;
            virtual NodeGroup* toGroup() = 0;

        private:
            Graph* mGraph;
    };
}
