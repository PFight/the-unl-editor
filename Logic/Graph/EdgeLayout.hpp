#pragma once

#include "The2D/Component.hpp"
#include "Box2DSupport/Joint.hpp"

using namespace the;

namespace mp
{
    class Edge;

    class EdgeLayout : public the::Component
    {
        public:
            THE_ENTITY(EdgeLayout, the::Component)

            Bind<Body> toFirstNodeBody;
            Bind<Body> toSecondNodeBody;

            PFloat prmDefaultEdgeLength;
            PBool prmAutoLength;

            CFloat CNominalLength;
            CEvent EvNominalLengthChanged;

            the::DistanceJoint mJoint;

            EdgeLayout();

            void setNominalLength(float val);

            Edge* getEdge();
            void setEdge(Edge* edge);

        private:
            float mNominalLength;
            Edge* mMyEdge;

            the::InitState onInit();
            void onDeinit();
    };
}
