////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/Config.hpp>
#include "Logic/FocusController.hpp"
#include "Base/Utils.hpp"
#include "ui_mainwindow.h"
#include "The2D/StateMachine/State.hpp"

using namespace mp;
using namespace the;



FocusController::FocusController()
{
    REGISTER(toMainWindow);
    REGISTER(IsWorkspaceFocused).EvActivated.SigRaised.add(MY_FUNC(focusWorkspace));
    REGISTER(IsTextEditFocused).EvActivated.SigRaised.add(MY_FUNC(focusEdit));
}

void FocusController::focusWorkspace()
{
    toMainWindow->getWindow()->ui->graphicsView->setFocus();
}
void FocusController::focusEdit()
{
    toMainWindow->getWindow()->ui->textEditMain->setFocus();
}

