////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/Config.hpp>
#include "Logic/SelectSentece.hpp"

using namespace mp;
using namespace the;



SelectSentece::SelectSentece() 
{
    REGISTER(toGraph);
    REGISTER(toGraphSelector);
    REGISTER(toNodesGenerator);
    REGISTER(DoSelect).setFunc(std::bind(&SelectSentece::select, this));
}

SelectSentece::~SelectSentece()
{
}

void SelectSentece::onDeinit()
{

}

the::InitState SelectSentece::onInit()
{

    return true;
}

void SelectSentece::select()
{
    toGraphSelector->DoActivateSelected();
    if (toGraph()->getNodes().empty())
    {
        toNodesGenerator->DoGenerateNodes();
    }
}


