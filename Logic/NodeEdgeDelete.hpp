////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Bind.hpp>
#include <The2D/Component.hpp>
#include <The2D/Connectors.hpp>
#include <The2D/CValueVector.hpp>

using namespace the;

////////////////////////////////////////////////////////////
namespace mp {

    class GraphItem;

    class NodeEdgeDelete : public the::Component
    {
        public:
            THE_ENTITY(NodeEdgeDelete, the::Component)

            ToValueVector<GraphItem*> toSelectedItems;

            CCommand DoDeleteSelected;

            NodeEdgeDelete();
            virtual ~NodeEdgeDelete();

            void deleteSelected();

        private:
            InitState onInit() override;
            void onDeinit() override;


    };
}; // namespace mp
////////////////////////////////////////////////////////////
