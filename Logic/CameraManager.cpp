////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include "Logic/CameraManager.hpp"

using namespace mp;
using namespace the;



CameraManager::CameraManager(): mPosition(0,0)
{
    REGISTER(toSceneManager);
    REGISTER(CPosition).setValueChangedEvent(&EvPositionChanged)
        .setSetter(std::bind(&CameraManager::setPosition, this, std::placeholders::_1))
        .setGetter(std::bind(&CameraManager::getPosition, this, std::placeholders::_1));
    REGISTER(EvPositionChanged);
}

CameraManager::~CameraManager()
{
}

void CameraManager::onDeinit()
{
}

the::InitState CameraManager::onInit()
{
    return true;
}

void CameraManager::setPosition(Vec2 pos)
{
    toSceneManager->getView()->centerOn(pos.x, pos.y);
    mPosition = Vec2(pos.x, pos.y);
}

void CameraManager::getPosition(Vec2& result)
{
    QPointF pos = toSceneManager->getView()->mapToScene(toSceneManager->getView()->viewport()->rect()).boundingRect().center();
    result = Vec2(pos.x(), pos.y());
}


