////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/Config.hpp>
#include "Logic/NodeKeyboardDrag.hpp"
#include "Base/Utils.hpp"
#include "Logic/Graph/Node.hpp"

using namespace mp;
using namespace the;

NodeKeyboardDrag::NodeKeyboardDrag()
{
    // Children registration
    REGISTER(toSelectedItems);
    REGISTER(toCellSize);
    REGISTER(DoMoveUp).setFunc(std::bind(&NodeKeyboardDrag::move, this, UP));
    REGISTER(DoMoveDown).setFunc(std::bind(&NodeKeyboardDrag::move, this, DOWN));
    REGISTER(DoMoveLeft).setFunc(std::bind(&NodeKeyboardDrag::move, this, LEFT));
    REGISTER(DoMoveRight).setFunc(std::bind(&NodeKeyboardDrag::move, this, RIGHT));
    REGISTER(toEdgeCorrector);
}

NodeKeyboardDrag::~NodeKeyboardDrag()
{
}

void NodeKeyboardDrag::onDeinit()
{
}

the::InitState NodeKeyboardDrag::onInit()
{
    return READY;
}

void NodeKeyboardDrag::move(Direction dir)
{
    toEdgeCorrector->DoDragStart();

    Vec2 shift(0,0);

    switch(dir)
    {
        CASE(UP) shift = Vec2(0, -toCellSize().y); END_CASE
        CASE(DOWN) shift = Vec2(0, toCellSize().y); END_CASE
        CASE(LEFT) shift = Vec2(-toCellSize().x, 0); END_CASE
        CASE(RIGHT) shift = Vec2(toCellSize().x, 0); END_CASE
    };

    for (auto item : toSelectedItems)
    {
        if (Node* node = item->toNode())
        {
            node->mLayout.CPosition = node->mLayout.CPosition + shift;
        }
    }

    toEdgeCorrector->DoDragFinish();
}