////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include "Logic/MainWindowManager.hpp"
#include <qboxlayout.h>
#include <qdebug.h>
#include <QApplication>

// -----------------------------------------------------------------------------------



mp::MainWindowManager::MainWindowManager(): mWindowClosed(true)
{
    REGISTER(toApp);
    REGISTER(EvWindowClose);
    REGISTER(EvWindowResize);
    REGISTER(EvWindowMove);
    mMainWindow = nullptr;
}

the::InitState mp::MainWindowManager::onInit()
{
    mMainWindow = new MainWindow(nullptr);
    mMainWindow->show();
    mMainWindow->SigCloseEvent.add(this, std::bind(&MainWindowManager::mainWindowCloseEvent, this,
        std::placeholders::_1));
    QApplication::processEvents(QEventLoop::AllEvents); // force window to open, making all
                                                        // operations with it valid (for those, who have binds to me)
    mWindowClosed = false;

    return true;
}

void mp::MainWindowManager::onDeinit()
{
    mMainWindow->SigCloseEvent.remove(this);
    mMainWindow->SigResize.remove(this);
    mMainWindow->SigMove.remove(this);

    if (!mWindowClosed)
    {
        mMainWindow->close();
        QApplication::processEvents(QEventLoop::AllEvents); // Let all to react
        mWindowClosed = true;
    }
}
void mp::MainWindowManager::mainWindowCloseEvent(QCloseEvent* ev)
{
    mWindowClosed = true;
    toApp->DoExit();
}