#pragma once

#include <The2D/Component.hpp>
#include <The2D/Connectors.hpp>
#include <The2D/Bind.hpp>
#include <The2D/CValueVector.hpp>
#include "Logic/Graph/NodeView.hpp"
#include "Logic/Graph/EdgeView.hpp"
#include "Logic/NodeEdgeEdit.hpp"
#include "Logic/Graph/GraphHelper.hpp"

using namespace the;

namespace mp
{
    class Node;
    class MainWindowManager;
    class Graph;


    class EdgeCreation : public the::Component
    {
        public:

            THE_ENTITY(EdgeCreation, the::Component)

            ToValueVector<GraphItem*> toSelectedItems;
            //the::DisplayText prmEdgeCreationModeNotify;
            ManagerBind<MainWindowManager> toMainWindow;
            ToValueR<Graph*> toGraph;
            Bind<GraphHelper> toGraphHelper;

            ToValueR<QRectF> toSelectionRect;

            CCommand DoStart;
            CCommand DoFinish;
            CCommand DoCancel;

            NodeView mFantomNode;

            EdgeCreation();

            void startAddEdge();
            void finishAddEdge();
            void cancelAddEdge();
            void showFinishFailMessage();


        private:
            bool mEnableSelectionMonitoring;
            eastl::list<sptr<EdgeView>> mFantomEdges;

            the::InitState onInit();
            void onDeinit();

            void onGraphSelectionChanged();
            void onGridSelectionChanged();

            Node* mNewEdgeBegin;
    };
}