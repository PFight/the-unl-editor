////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include "Logic/NodeEdgeDelete.hpp"
#include "Logic/Graph/Graph.hpp"

using namespace mp;



NodeEdgeDelete::NodeEdgeDelete()
{
    REGISTER(toSelectedItems);
    REGISTER(DoDeleteSelected).setFunc(std::bind(&NodeEdgeDelete::deleteSelected, this));
}

NodeEdgeDelete::~NodeEdgeDelete()
{
}

void NodeEdgeDelete::deleteSelected()
{
    eastl::vector<GraphItem*> selection = toSelectedItems();
    for (auto item : selection)
    {
        item->getGraph()->remove(item);
    }
    toSelectedItems.clear();
}

void NodeEdgeDelete::onDeinit()
{

}

the::InitState NodeEdgeDelete::onInit()
{

    return true;
}


