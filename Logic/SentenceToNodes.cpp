////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/Config.hpp>
#include "Logic/SentenceToNodes.hpp"
#include <cctype>
#include <QApplication>
#include <QChar>
#include "Logic/Graph/Graph.hpp"

using namespace mp;
using namespace the;



SentenceToNodes::SentenceToNodes() 
{
    REGISTER(toGraph);
    REGISTER(toGraphHelper);
    REGISTER(prmInterval).defval(10);
    REGISTER(DoGenerateNodes).setFunc(std::bind(&SentenceToNodes::generateNodes, this));
}

SentenceToNodes::~SentenceToNodes()
{
}

void SentenceToNodes::onDeinit()
{

}

the::InitState SentenceToNodes::onInit()
{

    return true;
}

void SentenceToNodes::generateNodes()
{
    // Generate nodes in row for each word in sentence.
    // First node will be located at (0,0), other from left of it in one line.


    // Find words
    std::vector<QString> words;
    QString currentWord;
    currentWord.reserve(10);
    QString sentence = toGraph()->getSentence();
    for (int i = 0; i < (int)sentence.length(); i++)
    {
        if (QChar(sentence[i]).isLetter())
        {
            currentWord += sentence[i];
        }
        else
        {
            if (!currentWord.size() == 0)
            {
                words.push_back(currentWord);
                currentWord = "";
            }
        }
    }
    if (!currentWord.size() == 0)
    {
        words.push_back(currentWord);
    }

    // Create nodes
    Graph* graph = toGraph();
    Vec2 lastPos(0,0);
    for (auto& word : words)
    {
        sptr<Node> node = toGraphHelper->createNode();
        node->mData.prmSource = word.toStdString();
        node->mLayout.prmPosition.set(lastPos.x, lastPos.y);
        graph->addNode(node);
        QApplication::processEvents(QEventLoop::AllEvents); // allow node->mView to create graphics items
        lastPos = Vec2(lastPos.x + node->mView.CSize().x + prmInterval, lastPos.y);
    }
}

