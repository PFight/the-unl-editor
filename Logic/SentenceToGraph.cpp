////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/Config.hpp>
#include "Logic/SentenceToGraph.hpp"
#include <QtWidgets/QTextEdit>
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QMessageBox>

using namespace mp;
using namespace the;



SentenceToGraph::SentenceToGraph()
{
    REGISTER(toMainWindow);
    REGISTER(toGraph);
    REGISTER(toCreateBlankGraph);
    REGISTER(DoActivateSelected).setFunc(std::bind(&SentenceToGraph::activateSentence, this));
    REGISTER(CGraphBySentence)
        .setFunc([&](QString sen){return mSentenceToGraph[sen].get();});
    REGISTER(CSentences).setGetter(&mSentences);
    REGISTER(EvSentencesChanged);
    REGISTER(CText)
        .setGetter(&mAllText)
        .setSetter([&](QString str){ toMainWindow->getWindow()->ui->textEditMain->setText(str); });
    REGISTER(toCreateGraph);
}

SentenceToGraph::~SentenceToGraph()
{
}

void SentenceToGraph::onDeinit()
{
    for (auto sgpair : mSentenceToGraph)
    {
        if (toGraph() == sgpair.second.get())
        {
            toCreateBlankGraph();
        }
        sgpair.second->deinit();
    }
    QObject::disconnect(mTextChangedConnection);
    QObject::disconnect(mSaveConnection);
    QObject::disconnect(mSaveAsConnection);
    QObject::disconnect(mOpenConnection);
}

the::InitState SentenceToGraph::onInit()
{
    mSentenceToGraph.clear();

    mTextChangedConnection = QObject::connect(toMainWindow->getWindow()->ui->textEditMain,
        &QTextEdit::textChanged, [&](){ parseSentences(); });
    mSaveConnection = QObject::connect(toMainWindow->getWindow()->ui->actionSave,
        &QAction::triggered, [&](bool){ save(); });
    mSaveAsConnection = QObject::connect(toMainWindow->getWindow()->ui->actionSave_as,
        &QAction::triggered, [&](bool){ saveAs(); });
    mOpenConnection = QObject::connect(toMainWindow->getWindow()->ui->actionOpen,
        &QAction::triggered, [&](bool){ open(); });


    return true;
}

void SentenceToGraph::activateSentence()
{
    QTextEdit& edit = *(toMainWindow->getWindow()->ui->textEditMain);
    // Get cursor pos
    QTextCursor cursor = edit.textCursor();
    int cursorPos = cursor.position();

    // Retreive sentence at cursor
    int startPos;
    int endPos;
    QString sentence = getSentenceFromPos(cursorPos, &startPos, &endPos);

    if (sentence.size() == 0) return;

    // Select it in editor
    cursor.setPosition(startPos);
    cursor.setPosition(endPos, QTextCursor::KeepAnchor);
    edit.setTextCursor(cursor);

    // Find or create graph for this sentence
    toGraph()->saveState();
    toGraph()->deinit();

    sptr<Graph>& graph = mSentenceToGraph[sentence];
    if (graph.get() == nullptr)
    {
        auto graphPtr = toCreateGraph();
        graph.swap(graphPtr);
        graph->setSentence(sentence);
        REGISTER(graph);
    }
    graph->init();
    toGraph = graph.get();
    toMainWindow->getWindow()->ui->labelCurrent->setText(sentence);
}

void SentenceToGraph::parseSentences()
{
    mSentences.clear();
    QTextEdit& edit = *(toMainWindow->getWindow()->ui->textEditMain);
    mAllText = edit.toPlainText();
    int startPos = 0;
    int endPos = mAllText.size();
    for (int pos = 0; pos < mAllText.size(); pos = endPos + 1)
    {
        QString sentence = getSentenceFromPos(pos, &startPos, &endPos);
        mSentences.push_back(sentence);
    }
}

QString SentenceToGraph::getSentenceFromPos(int cursorPos, int* p_startPos, int* p_endPos)
{
    QString allText = mAllText;

    if (allText.size()==0)
    {
        return "";
    }
    QString sentence;
    // Find position of sentence start
    int startPos = 0;
    for (int i=cursorPos-1; i >= 0; i--)
    {
        if (allText[i] == '.')
        {
            startPos = i+1;
            break;
        }
    }
    if (startPos >= (int)allText.length())
    {
        return "";
    }
    // Find position of the sentence end
    int endPos = allText.length();
    for (int i=cursorPos; i < (int)allText.length(); i++)
    {
        if (allText[i] == '.')
        {
            endPos = i;
            break;
        }
    }
    if (endPos < 0 || endPos <= startPos)
    {
        return "";
    }
    // Get text between start and end positions
    if (p_startPos) *p_startPos = startPos;
    if (p_endPos) *p_endPos = endPos;
    return allText.mid(startPos, endPos - startPos);
}


void SentenceToGraph::save()
{
    if (!CCurrentFile().empty())
    {
        TiXmlDocument doc;
        TiXmlElement* root = new TiXmlElement("Sentences");
        doc.LinkEndChild(root);
        for (int i =0; i < (int)mSentences.size(); i++)
        {
            QString sen = mSentences[i];
            Graph* graph = mSentenceToGraph[sen].get();

            TiXmlElement* sentenceNode = new TiXmlElement("Sentence");
            root->LinkEndChild(sentenceNode);
            sentenceNode->SetAttribute("text", sen.toStdString());

            if (graph != nullptr)
            {
                if (graph->isInitialized())
                {
                    graph->saveState();
                }
                else
                {
                    // State already saved
                }
                TiXmlElement* el = graph->getParser()->serialize();
                sentenceNode->LinkEndChild(el);
            }
        }
        doc.SaveFile(CCurrentFile());
        INFO("Saved to %s", CCurrentFile());
    }
    else
    {
        saveAs();
    }
}

void SentenceToGraph::saveAs()
{
    CCurrentFile = QFileDialog::getSaveFileName(
        toMainWindow->getWindow(), QObject::tr("Save file"), "",
        QObject::tr("AllFiles (*.*);;The UNL files (*.theunl)")).toStdString();
    if (!CCurrentFile().empty())
    {
        save();
    }
    else
    {
        // Dialog canceled
    }
}

void SentenceToGraph::open()
{
    string filePath = QFileDialog::getOpenFileName(
        toMainWindow->getWindow(), QObject::tr("Open file"), "",
        QObject::tr("AllFiles (*.*);;The UNL files (*.theunl)")).toStdString();
    if (!filePath.empty())
    {
        sptr<TiXmlDocument> doc(new TiXmlDocument());
        bool ok = doc->LoadFile(filePath);
        if (ok)
        {
            Graph* firstGraphInFile = nullptr;
            QString text;

            mSentenceToGraph.clear();

            // Load all sentences from file
            TiXmlElement* root = doc->RootElement();
            for (TiXmlElement* sentenceNode = root->FirstChildElement();
                 sentenceNode != nullptr; sentenceNode = sentenceNode->NextSiblingElement())
            {
                QString sen = sentenceNode->Attribute("text");
                Graph* graph = new Graph();
                REGISTER(graph);
                TiXmlElement* graphNode = sentenceNode->FirstChildElement();
                if (graphNode != nullptr)
                {
                    core()->xml()->parse(graphNode, doc, filePath, graph);
                }
                mSentenceToGraph.insert(std::make_pair(sen, make_sptr(graph)));
                text += sen + ".";
                if (firstGraphInFile == nullptr)
                {
                    firstGraphInFile = graph;
                }
            }
            toMainWindow->getWindow()->ui->textEditMain->setText(text);

            // Activate first graph in the file
            //toGraph->getActiveGraph()->deinit();
            //toGraph->setAvtiveGraph(firstGraphInFile);
            //firstGraphInFile->init();
            QTextCursor cursor = toMainWindow->getWindow()->ui->textEditMain->textCursor();
            cursor.setPosition(0);
            toMainWindow->getWindow()->ui->textEditMain->setTextCursor(cursor);
            activateSentence();
            CCurrentFile = filePath;
            INFO("Loaded from %s", filePath);
        }
        else
        {
            QMessageBox::critical(toMainWindow->getWindow(), QObject::tr("Error"),
                QObject::tr("Can't open file, it is corrupted."));
        }
    }
    else
    {
        // Dialog canceled
    }
}