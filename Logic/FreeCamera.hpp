#pragma once

#include <Base/ToggleComponent.hpp>
#include <The2D/Bind.hpp>
#include <The2D/Vec2.hpp>
#include <The2D/Parameters.hpp>
#include <The2D/Connectors.hpp>

#include "Logic/SceneManager.hpp"
#include "Logic/MouseHoverGraph.hpp"

#include <QObject>
#include <QPoint>

using namespace the;

namespace mp
{
    class FreeCamera : public QObject, public ToggleComponent
    {
        Q_OBJECT
        
        public:
            THE_ENTITY(FreeCamera, ToggleComponent)

            enum MoveDirection
            {
                MOVE_UP = 1,
                MOVE_DOWN =2,
                MOVE_LEFT =4,
                MOVE_RIGHT = 8
            };

            ToVec2 toCameraPos;
            ManagerBind<SceneManager> toScene;

            PFloat prmSpeed;

            CEvent EvDragBegin;
            CEvent EvDragEnd;


            FreeCamera();

            void draw() override;

            void enableMoving(MoveDirection direction, bool enable);
            void shiftCamera(Vec2 shift);
            void beginMouseDrag(QPoint mousePos);
            void endMouseDrag();
            void processNewMousePos(QPoint newMousePos);

        private:
            MoveDirection mCurrentMove;
            QPoint mLastMousePos;
            bool mDragBegun;
            bool mShiftingCamera;

            bool eventFilter(QObject * watched, QEvent * event) override;

            void onEnable(bool enable);

            the::InitState onInit();
            void onDeinit();
    };
}