#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include "The2D/Manager.hpp"
#include "The2D/Connectors.hpp"
#include <The2D/CValueVector.hpp>

using namespace the;

////////////////////////////////////////////////////////////
namespace mp
{
    class Node;
    class Edge;
    class GraphItem;
    class Graph;

    class SelectionManager: public the::Manager
    {
        public:
            THE_ENTITY(SelectionManager, the::Manager)

            ToValueR<Graph*> toGraph;

            CValueVector<GraphItem*> CSelectedItems;

            CEvent EvSelectionChanged;

            SelectionManager();

            void select(GraphItem* knot);
            void addToSelection(GraphItem* knot);
            void removeFromSelection(GraphItem* knot);
            void clearSelection();
            const eastl::vector<GraphItem*>& getSelectedItems();

            Node* getLastSelectedNode();
            Edge* getLastSelectedEdge();

        private:
            eastl::vector<GraphItem*> mSelection;
            Node* mLastSelectedNode;
            Edge* mLastSelectedEdge;
            Graph* mLastGraph;

            the::InitState onInit();
            void onDeinit();

            void removeDublicates();
            void graphChanged();

    };

}; // namespace mp
////////////////////////////////////////////////////////////
