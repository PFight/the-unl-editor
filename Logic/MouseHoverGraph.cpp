////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include "Logic/MouseHoverGraph.hpp"
#include <QEvent>
#include "Logic/Graph/Graph.hpp"
#include "GraphManager.hpp"

using namespace mp;

MouseHoverGraph::MouseHoverGraph(): mLastGraph(nullptr)
{
    REGISTER(toGraph).onValueChange([&]{ graphChanged(); });

    REGISTER(CHoverNode).setValueChangedEvent(&EvHoverNodeChanged) = nullptr;
    REGISTER(CHoverEdge).setValueChangedEvent(&EvHoverEdgeChanged) = nullptr;
    REGISTER(EvHoverNodeChanged);
    REGISTER(EvHoverEdgeChanged);
}

MouseHoverGraph::~MouseHoverGraph()
{
}

InitState MouseHoverGraph::onInit()
{
    graphChanged();
    
    return READY;
}

void MouseHoverGraph::graphChanged()
{
    if (mLastGraph != nullptr)
    {
        for (auto node : mLastGraph->getNodes())
        {
            nodeRemoved(node.get());
        }
        for (auto edge : mLastGraph->getEdges())
        {
            edgeRemoved(edge.get());
        }
        mLastGraph->SigNodeAdded.remove(this);
        mLastGraph->SigNodeRemoved.remove(this);
        mLastGraph->SigEdgeAdded.remove(this);
        mLastGraph->SigEdgeRemoved.remove(this);
    }

    toGraph()->SigNodeAdded.add(this, std::bind(&MouseHoverGraph::nodeAdded, this, std::placeholders::_1));
    toGraph()->SigNodeRemoved.add(this, std::bind(&MouseHoverGraph::nodeRemoved, this, std::placeholders::_1));
    toGraph()->SigEdgeAdded.add(this, std::bind(&MouseHoverGraph::edgeAdded, this, std::placeholders::_1));
    toGraph()->SigEdgeRemoved.add(this, std::bind(&MouseHoverGraph::edgeRemoved, this, std::placeholders::_1));

    for (auto node : toGraph()->getNodes())
    {
        nodeAdded(node.get());
    }
    for (auto edge : toGraph()->getEdges())
    {
        edgeAdded(edge.get());
    }

    mLastGraph = toGraph();
}
void MouseHoverGraph::nodeAdded(Node* node)
{
    node->mView.SigSceneEvent.add(this, std::bind(&MouseHoverGraph::nodeEvent, this, std::placeholders::_1, node));
}
void MouseHoverGraph::edgeAdded(Edge* edge)
{
    edge->mView.SigSceneEvent.add(this, std::bind(&MouseHoverGraph::edgeEvent, this, std::placeholders::_1, edge));
}
void MouseHoverGraph::nodeRemoved(Node* node)
{
    node->mView.SigSceneEvent.remove(this);
}
void MouseHoverGraph::edgeRemoved(Edge* edge)
{
    edge->mView.SigSceneEvent.remove(this);
}

void MouseHoverGraph::nodeEvent(QEvent* ev, Node* node)
{
    if (ev->type() == QEvent::GraphicsSceneHoverEnter)
    {
        CHoverNode = node;
    }
    else if (ev->type() == QEvent::GraphicsSceneHoverLeave)
    {
        CHoverNode = nullptr;
    }
}
void MouseHoverGraph::edgeEvent(QEvent* ev, Edge* edge)
{
    if (ev->type() == QEvent::GraphicsSceneHoverEnter)
    {
        CHoverEdge = edge;
    }
    else if (ev->type() == QEvent::GraphicsSceneHoverLeave)
    {
        CHoverEdge = nullptr;
    }
}