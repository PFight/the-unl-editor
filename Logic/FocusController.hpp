////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Bind.hpp>
#include <The2D/Component.hpp>
#include <The2D/Connectors.hpp>
#include "Logic/MainWindowManager.hpp"
#include "EditorState.hpp"

using namespace the;


////////////////////////////////////////////////////////////
namespace mp {

    class FocusController : public the::Component
    {
        public:
            THE_ENTITY(FocusController, the::Component)

            ManagerBind<MainWindowManager> toMainWindow;

            EditorState IsWorkspaceFocused;
            EditorState IsTextEditFocused;

            FocusController();

        private:
            void focusWorkspace();
            void focusEdit();

    };
}; // namespace mp
////////////////////////////////////////////////////////////
