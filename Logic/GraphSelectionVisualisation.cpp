////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/Config.hpp>
#include "Logic/GraphSelectionVisualisation.hpp"
#include "Logic/Graph/Node.hpp"
#include "Logic/Graph/NodeGroup.hpp"
#include "Logic/Graph/Edge.hpp"

using namespace mp;
using namespace the;

GraphSelectionVisualisation::GraphSelectionVisualisation()
{
    REGISTER(toSelectedItems).onValueChange([&]{ selectionUpdated(); });
}

GraphSelectionVisualisation::~GraphSelectionVisualisation()
{
}

void GraphSelectionVisualisation::onDeinit()
{
}

the::InitState GraphSelectionVisualisation::onInit()
{
    return READY;
}

void GraphSelectionVisualisation::selectionUpdated()
{
    for (auto item : mLastSelection)
    {
        if (!toSelectedItems.contains(item))
        {
            if (Node* node = item->toNode())
            {
                node->mView.CSelectionState = NodeView::NOT_SELECTED;
            }
            else if (Edge* edge = item->toEdge())
            {
                edge->mView.CSelectionState = EdgeView::NOT_SELECTED;
            }
            else if (NodeGroup* group = item->toGroup())
            {
                group->mView.CSelectionState = NodeGroupView::NOT_SELECTED;
            }
        }
    }
    for (auto item : toSelectedItems)
    {
        if (Node* knot = item->toNode())
        {
            knot->mView.CSelectionState = NodeView::SELECTED;
        }
        else if (Edge* edge = item->toEdge())
        {
            edge->mView.CSelectionState = EdgeView::SELECTED;
        }
        else if (NodeGroup* group = item->toGroup())
        {
            group->mView.CSelectionState = NodeGroupView::SELECTED;
        }
    }
    mLastSelection = toSelectedItems();
}