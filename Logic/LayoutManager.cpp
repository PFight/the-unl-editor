////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/Config.hpp>
#include "Logic/LayoutManager.hpp"
#include <QtWidgets/QGraphicsItem>
#include "Logic/SceneManager.hpp"

using namespace mp;
using namespace the;



LayoutManager::LayoutManager()
{
    REGISTER(toSceneManager);
    REGISTER(prmPhysicsScale).defval(100);
}

LayoutManager::~LayoutManager()
{
}

void LayoutManager::onDeinit()
{

}

the::InitState LayoutManager::onInit()
{

    return true;
}

Vec2 LayoutManager::pointFromBox2D(b2Vec2 pos)
{
    return Vec2(pos.x * prmPhysicsScale, -pos.y*prmPhysicsScale);
}
Vec2 LayoutManager::pointFromScreen(QPoint pos)
{
    QPointF mapped = toSceneManager->getView()->mapToScene(pos);
    return Vec2(mapped.x(), mapped.y());
}

Vec2 LayoutManager::vectorFromBox2D(b2Vec2 vec)
{
    return Vec2(vec.x * prmPhysicsScale, -vec.y*prmPhysicsScale);
}
Vec2 LayoutManager::vectorFromScreen(QPoint vec)
{
    QPointF mapped = toSceneManager->getView()->mapToScene(vec);
    QPointF mappedZero = toSceneManager->getView()->mapToScene(QPoint(0,0));
    QPointF result = mapped - mappedZero;
    return Vec2(result.x(), result.y());
}

float LayoutManager::fromBox2D(float val)
{
    return val * prmPhysicsScale;
}
float LayoutManager::fromScreen(int val)
{
    return val;
}
b2Vec2 LayoutManager::pointToBox2D(Vec2 pos)
{
    return b2Vec2(pos.x / prmPhysicsScale, -pos.y / prmPhysicsScale);
}
QPoint LayoutManager::pointToScreen(Vec2 pos)
{
    return toSceneManager->getView()->mapFromScene(pos.x, pos.y);
}
b2Vec2 LayoutManager::vectorToBox2D(Vec2 vec)
{
    return b2Vec2(vec.x / prmPhysicsScale, -vec.y / prmPhysicsScale);
}
QPoint LayoutManager::vectorToScreen(Vec2 vec)
{
    QPoint mapped = toSceneManager->getView()->mapFromScene(vec.x, vec.y);
    QPoint mappedZero = toSceneManager->getView()->mapFromScene(QPoint(0,0));
    return mapped - mappedZero;
}

float32 LayoutManager::toBox2D(float val)
{
    return val / prmPhysicsScale;
}
int LayoutManager::toScreen(float val)
{
    return val;
}

Vec2 LayoutManager::pointFromItem(QPointF pos, QGraphicsItem* item)
{
    QPointF scenePos = item->mapToScene(pos);
    return Vec2(scenePos.x(), scenePos.y());
}
Vec2 LayoutManager::vectorFromItem(QPointF vec, QGraphicsItem* item)
{
    QPointF mapped = item->mapToScene(vec.x(), vec.y());
    QPointF mappedZero = item->mapToScene(QPoint(0,0));
    QPointF res = mapped - mappedZero;
    return Vec2(res.x(), res.y());
}
QPointF LayoutManager::pointToItem(Vec2 pos, QGraphicsItem* item)
{
    return item->mapFromScene(QPointF(pos.x, pos.y));
}
QPointF LayoutManager::vectorToItem(Vec2 vec, QGraphicsItem* item)
{
    QPointF mapped = item->mapFromScene(vec.x, vec.y);
    QPointF mappedZero = item->mapFromScene(QPoint(0,0));
    return mapped - mappedZero;
}