////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Component.hpp>
#include <The2D/Parameters.hpp>
#include <The2D/Connectors.hpp>
#include <The2D/CValueVector.hpp>

using namespace the;

////////////////////////////////////////////////////////////
namespace mp {

    class GraphItem;

    class GraphSelectionVisualisation : public Component
    {
        public:
            THE_ENTITY(GraphSelectionVisualisation, Component)

            ToValueVectorR<GraphItem*> toSelectedItems;

            GraphSelectionVisualisation();
            virtual ~GraphSelectionVisualisation();

        private:
            eastl::vector<GraphItem*> mLastSelection;

            InitState onInit() override;
            void onDeinit() override;

            void selectionUpdated();
            
    };
}; // namespace mp
////////////////////////////////////////////////////////////