#pragma once

#include <Base/ToggleComponent.hpp>
#include <The2D/Bind.hpp>
#include <The2D/Vec2.hpp>
#include <The2D/Parameters.hpp>
#include <The2D/Connectors.hpp>
#include <The2D/CValueVector.hpp>

#include <Box2DSupport/Joint.hpp>

#include "Logic/LayoutManager.hpp"
#include "Logic/SceneManager.hpp"
#include "Logic/MouseHoverGraph.hpp"

#include <QObject>
#include <QPoint>

using namespace the;

namespace mp
{
    class GraphItem;

    class NodeMouseDrag : public QObject, public ToggleComponent
    {
        Q_OBJECT
        
        public:
            THE_ENTITY(NodeMouseDrag, ToggleComponent)

            ManagerBind<SceneManager> toScene;
            ManagerBind<LayoutManager> toLayout;
            ToValueVectorR<GraphItem*> toSelectedItems;
            Bind<MouseHoverGraph> toMouseHoverGraph;

            PFloat prmMaxForceKoef; ///< \brief MaxForce calculated as maxForceKoef * draggingBodyMass

            MouseJoint mDragJoint;;
            Body mAttachBody;

            NodeMouseDrag();
            
            void beginMouseDrag(Node* node, QPoint mousePos);
            void endMouseDrag();
            void processNewMousePos(QPoint newMousePos);

        private:
            QPoint mLastMousePos;
            bool eventFilter(QObject * watched, QEvent * event) override;

            void onEnable(bool enable);

            the::InitState onInit();
            void onDeinit();
    };
}