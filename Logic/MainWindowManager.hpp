////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include "The2D/Manager.hpp"
#include <The2D/Connectors.hpp>
#include "Logic/ApplicationManager.hpp"
#include "mainwindow.h"

class QBoxLayout;

////////////////////////////////////////////////////////////
namespace mp
{

    using namespace the;

    class MainWindowManager: public the::Manager
    {
        public:

            THE_ENTITY(mp::MainWindowManager, the::Manager)

            ManagerBind<ApplicationManager> toApp;

            CEvent EvWindowClose;
            CEvent EvWindowResize;
            CEvent EvWindowMove;

            MainWindowManager();


            MainWindow* getWindow() { return mMainWindow; }

        private:
            bool mWindowClosed;
            MainWindow* mMainWindow;

            the::InitState onInit();
            void onDeinit();

            void mainWindowCloseEvent(QCloseEvent* ev);

    };

}; // namespace mp
////////////////////////////////////////////////////////////
