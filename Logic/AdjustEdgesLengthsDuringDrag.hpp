////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Component.hpp>
#include "Graph/EdgeLayout.hpp"
#include <The2D/Connectors.hpp>
#include <The2D/CValueVector.hpp>


using namespace the;

////////////////////////////////////////////////////////////
namespace mp {

    class EdgeLayout;
    class GraphItem;

    class AdjustEdgesLengthsDuringDrag : public the::Component
    {
        public:
            THE_ENTITY(AdjustEdgesLengthsDuringDrag, the::Component)

            ToValueVectorR<GraphItem*> toSelectedItems;

            CCommand DoDragStart;
            CCommand DoDragFinish;

            AdjustEdgesLengthsDuringDrag();
            virtual ~AdjustEdgesLengthsDuringDrag();

        private:
            eastl::vector<EdgeLayout*> mCurrentEdges;

            InitState onInit() override;
            void onDeinit() override;

            void dragStart();
            void dragFinish();

    };
}; // namespace mp
////////////////////////////////////////////////////////////
