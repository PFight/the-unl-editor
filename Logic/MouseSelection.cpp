#include "Logic/MouseSelection.hpp"
#include "Logic/Graph/Node.hpp"
#include "Logic/Graph/Edge.hpp"

using namespace the;
using namespace mp;



MouseSelection::MouseSelection()
{
    REGISTER(toSelectedItems);
    REGISTER(toMouseHoverGraph);
    REGISTER(toActiveGraph);
    REGISTER(DoSelectUnderMouse).setFunc(std::bind(&MouseSelection::selectUnderMouse, this, true));
    REGISTER(DoAddToSelectionUnderMouse).setFunc(std::bind(&MouseSelection::selectUnderMouse, this, false));
}

the::InitState MouseSelection::onInit()
{
    return true;
}

void MouseSelection::onDeinit()
{
}

void MouseSelection::selectUnderMouse(bool resetSelection)
{
    if (toMouseHoverGraph->CHoverNode != nullptr)
    {
        if (resetSelection)
        {
            toSelectedItems.clear();
            toSelectedItems.push_back(toMouseHoverGraph->CHoverNode());
        }
        else
        {
            if (toSelectedItems.contains(toMouseHoverGraph->CHoverNode()))
            {
                toSelectedItems.remove(toMouseHoverGraph->CHoverNode());
            }
            else
            {
                toSelectedItems.push_back(toMouseHoverGraph->CHoverNode());
            }
        }
    }
    else if (toMouseHoverGraph->CHoverEdge != nullptr)
    {

        if (resetSelection)
        {
            toSelectedItems.clear();
            toSelectedItems.push_back(toMouseHoverGraph->CHoverEdge());
        }
        else
        {
            if (toSelectedItems.contains(toMouseHoverGraph->CHoverEdge()))
            {
                toSelectedItems.remove(toMouseHoverGraph->CHoverEdge());
            }
            else
            {
                toSelectedItems.push_back(toMouseHoverGraph->CHoverEdge());
            }
        }
    }
    else
    {
        // Click on empty area inside node group selects it
        // Case, when mouse at node or edge catched by previous checks, so
        // Check, if mouse pointer is inside some node group.
        // Check is quite rubbish: foreach all node groups in active graph
        // and check them all.

    }
}