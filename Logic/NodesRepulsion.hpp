////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Component.hpp>
#include <The2D/Connectors.hpp>
#include <muParser.h>

using namespace the;

////////////////////////////////////////////////////////////
namespace mp {

    class Graph;

    class NodesRepulsion : public the::Component
    {
        public:
            THE_ENTITY(NodesRepulsion, the::Component)

            PFloat prmMaxDistance;
            PFloat prmMinForce;
            PFloat prmMaxForce;
            PString prmRepulsionLaw;

            ToValueR<Graph*> toGraph;

            NodesRepulsion();
            virtual ~NodesRepulsion();

            void update() override;

        private:
            mu::Parser mLawParser;
            double mDistanceVar; // For parser
            double mBodyMassVar;
            float mMaxDistanceSqr;


            InitState onInit() override;
            void onDeinit() override;

    };
}; // namespace mp
////////////////////////////////////////////////////////////
