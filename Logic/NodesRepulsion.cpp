////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/Config.hpp>
#include "Logic/NodesRepulsion.hpp"

#include "Logic/Graph/Graph.hpp"

using namespace mp;



NodesRepulsion::NodesRepulsion(): mDistanceVar(0), mBodyMassVar(0)
{
    REGISTER(prmMaxDistance).defval(2);
    REGISTER(prmMinForce).defval(0.1f);
    REGISTER(prmMaxForce).defval(50.0f);
    REGISTER(prmRepulsionLaw).defval("0.1*m/(d*d)");
    REGISTER(toGraph);

    mLawParser.DefineVar("d", &mDistanceVar);
    mLawParser.DefineVar("m", &mBodyMassVar);
}

NodesRepulsion::~NodesRepulsion()
{
}

the::InitState NodesRepulsion::onInit()
{
    mMaxDistanceSqr = prmMaxDistance() * prmMaxDistance();
    mLawParser.SetExpr(prmRepulsionLaw());
    try
    {
        mLawParser.Eval(); // compile expression
    }
    catch(mu::Parser::exception_type &e)
    {
        return NOT_READY("Failed to parse wall escape law '%s':  %s", prmRepulsionLaw(), e.GetMsg());
    }
    return true;
}
void NodesRepulsion::onDeinit()
{

}

void NodesRepulsion::update()
{
    for (auto node1Ptr : toGraph()->getNodes())
    {
        mBodyMassVar = node1Ptr->mLayout.mBody.prmMassData.mass();
        for (auto node2Ptr : toGraph()->getNodes())
        {
            if (node1Ptr.get() != node2Ptr.get())
            {
                b2Vec2 dir = node2Ptr->mLayout.mBody.getBody()->GetPosition() -
                    node1Ptr->mLayout.mBody.getBody()->GetPosition();
                // Many nodes will not pass next condition, so use fast lengthSquared there
                float distanceSqr = dir.LengthSquared() + 0.01;
                if (distanceSqr < mMaxDistanceSqr)
                {
                    float d = sqrt(distanceSqr);
                    mDistanceVar = d;
                    double forceAbs = mLawParser.Eval();
                    if (forceAbs > prmMaxForce)
                    {
                        forceAbs = prmMaxForce;
                    }
                    if (forceAbs > prmMinForce)
                    {
                        // Normalize and scale dir
                        b2Vec2 force(forceAbs*dir.x/d, forceAbs*dir.y/d);
                        node2Ptr->mLayout.mBody.getBody()->ApplyForceToCenter(force, true);
                    }
                }
            }
        }
    }
}




