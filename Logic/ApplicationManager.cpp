////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include "Logic/ApplicationManager.hpp"
#include <thread>

// -----------------------------------------------------------------------------------



mp::Application::Application(int &argc, char **argv, int flags)
    :QApplication(argc, argv, flags)
{
}

bool mp::Application::notify(QObject *obj, QEvent * event)
{
   SigEvent(event);
   return QApplication::notify(obj, event);
}

mp::ApplicationManager::ApplicationManager()
{
    REGISTER(DoExit).setFunc(std::bind(&ApplicationManager::exit, this));
    setInitOrder(the::BY_PARENT);
    prmArgc = 0;
}


void mp::ApplicationManager::draw()
{
    if (isInitialized())
    {
        mApp->processEvents();
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
}

the::InitState mp::ApplicationManager::onInit()
{
    mApp = new Application(prmArgc, prmArgv);
    return true;
}

void mp::ApplicationManager::onDeinit()
{
    mApp->closeAllWindows();
    mApp->processEvents();
}

void mp::ApplicationManager::exit()
{
    core()->exitGame();
    core()->tree()->deinit();
}