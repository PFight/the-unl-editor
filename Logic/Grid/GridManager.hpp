////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Manager.hpp>
#include <The2D/Connectors.hpp>
#include <The2D/Parameters.hpp>
#include <The2D/CValueVector.hpp>
#include <QRect>
#include <The2D/Converters.hpp>

using namespace the;

////////////////////////////////////////////////////////////
namespace mp {

    struct GridCell
    {
        int x;
        int y;

        GridCell(int px, int py): x(px), y(py)
        {
        }

        GridCell(): x(0), y(0)
        {
        }

        bool operator==(const GridCell& other)
        {
            return x == other.x && y == other.y;
        }
        bool operator!=(const GridCell& other)
        {
            return x != other.x || y != other.y;
        }
    };

    typedef eastl::vector<GridCell> GridCellVector;

    class GridManager: public the::Manager
    {
        public:
            THE_ENTITY(GridManager, the::Manager)

            PSize prmInitialCellSize;
            struct PGridCell: ParameterGroup
            {
                PInt x;
                PInt y;

                PGridCell();
            } prmInitialSelection;

            CVec2 CCellSize;
            CEvent EvSelectionChanged;
            CEvent EvCellSizeChanged;
            CValueVector<GridCell> CSelectedCells;
            ValueConversion<GridCellVector, QRectF> GetRect;

            GridManager();
            virtual ~GridManager();

            static Vec2 getUpperLeft(int cellX, int cellY, Vec2 cellSize);
            static Vec2 getUpperLeft(GridCell cell, Vec2 cellSize);
            static Vec2 getCenter(int cellX, int cellY, Vec2 cellSize);
            static Vec2 getCenter(GridCell cell, Vec2 cellSize);
            static Vec2 getCenter(const GridCellVector& selection, Vec2 cellSize);
            static Vec2 getUpperLeft(const GridCellVector& selection, Vec2 cellSize);
            static Vec2 getLowerRight(const GridCellVector& selection, Vec2 cellSize);
            static GridCell getUpperLeftCell(const GridCellVector& selection);
            static GridCell getLowerRightCell(const GridCellVector& selection);

        private:
            GridCellVector mSelection;

            InitState onInit() override;
            void onDeinit() override;

    };
}; // namespace mp
////////////////////////////////////////////////////////////
