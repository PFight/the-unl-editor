////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/Config.hpp>
#include "Logic/Grid/GridResize.hpp"

using namespace mp;



GridResize::GridResize() 
{
    REGISTER(toCellSize);
    REGISTER(toSelectedCells);

    REGISTER(DoIncreaseHeight).setFunc(std::bind(&GridResize::increaseHeight, this));
        //DoIncreaseHeight.prmActivateKey.defval(Qt::SHIFT +  Qt::CTRL + Qt::Key_Up);
    REGISTER(DoDecreaseHeight).setFunc(std::bind(&GridResize::decreaseHeight, this));
        //DoDecreaseHeight.prmActivateKey.defval(Qt::SHIFT +  Qt::CTRL + Qt::Key_Down);
    REGISTER(DoIncreaseWidth).setFunc(std::bind(&GridResize::increaseWidth, this));
        //DoIncreaseWidth.prmActivateKey.defval(Qt::SHIFT +  Qt::CTRL + Qt::Key_Right);
    REGISTER(DoDecreaseWidth).setFunc(std::bind(&GridResize::decreaseWidth, this));
        //DoDecreaseWidth.prmActivateKey.defval(Qt::SHIFT +  Qt::CTRL + Qt::Key_Left);
}

GridResize::~GridResize()
{
}

void GridResize::onDeinit()
{

}

the::InitState GridResize::onInit()
{

    return true;
}

void GridResize::increaseHeight()
{
    toCellSize = Vec2(toCellSize().x, toCellSize().y * 2.0f);
    for (int i = 0; i < toSelectedCells.size(); i++)
    {
        GridCell cell = toSelectedCells.at(i);
        toSelectedCells.set(i, GridCell(cell.x, cell.y / 2));
    }
}
void GridResize::decreaseHeight()
{
    toCellSize = Vec2(toCellSize().x, toCellSize().y * 0.5f);
    for (int i = 0; i < toSelectedCells.size(); i++)
    {
        toSelectedCells.set(i, GridCell(toSelectedCells[i].x,  toSelectedCells[i].y * 2));
    }
}
void GridResize::increaseWidth()
{
    toCellSize = Vec2(toCellSize().x * 2.0f, toCellSize().y );
    for (int i = 0; i < toSelectedCells.size(); i++)
    {
        toSelectedCells.set(i, GridCell(toSelectedCells[i].x  / 2,  toSelectedCells[i].y));
    }
}
void GridResize::decreaseWidth()
{
    toCellSize = Vec2(toCellSize().x / 2.0f, toCellSize().y );
    for (int i = 0; i < toSelectedCells.size(); i++)
    {
        toSelectedCells.set(i, GridCell(toSelectedCells[i].x * 2,  toSelectedCells[i].y));
    }
}


