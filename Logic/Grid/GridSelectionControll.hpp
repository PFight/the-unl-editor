#pragma once

#include <The2D/Component.hpp>
#include <The2D/CValueVector.hpp>

#include "Logic/Grid/GridManager.hpp"

using namespace the;

namespace mp
{
    class GridSelectionControll : public the::Component
    {
        public:
            THE_ENTITY(GridSelectionControll, the::Component)

            ToValueVector<GridCell> toSelectedCells;

            CBool CEnabled;
            CCommand DoMoveUp;
            CCommand DoMoveDown;
            CCommand DoMoveLeft;
            CCommand DoMoveRight;
            CCommand DoExtendUp;
            CCommand DoExtendDown;
            CCommand DoExtendLeft;
            CCommand DoExtendRight;
            CCommand DoCancelMultiselect;

            GridSelectionControll();

            enum MoveDirection
            {
                MOVE_UP,
                MOVE_DOWN,
                MOVE_LEFT,
                MOVE_RIGHT
            };

            void moveSelection(MoveDirection dir, bool extend = false);
            void cancelMultiSelect();

        private:
            bool mLastExtendYIsUp;
            bool mLastExtendXIsRight;
            GridCell mMultiSelectBegin;

            the::InitState onInit();
            void onDeinit();


            void enable(bool enable);
    };
}