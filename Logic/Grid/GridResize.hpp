////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Component.hpp>
#include <Logic/UserInput/InputEvent.hpp>
#include <Logic/Grid/GridManager.hpp>

using namespace the;

////////////////////////////////////////////////////////////
namespace mp {

    class GridResize : public the::Component
    {
        public:
            THE_ENTITY(GridResize, the::Component)

            ToVec2 toCellSize;
            ToValueVector<GridCell> toSelectedCells;

            CCommand DoIncreaseHeight;
            CCommand DoDecreaseHeight;
            CCommand DoIncreaseWidth;
            CCommand DoDecreaseWidth;

            GridResize();
            virtual ~GridResize();

        private:
            InitState onInit() override;
            void onDeinit() override;

            void increaseHeight();
            void decreaseHeight();
            void increaseWidth();
            void decreaseWidth();

    };
}; // namespace mp
////////////////////////////////////////////////////////////
