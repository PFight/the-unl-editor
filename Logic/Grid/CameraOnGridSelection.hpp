#pragma once

#include <The2D/Parameters.hpp>
#include <The2D/Bind.hpp>

#include "Logic/Grid/GridManager.hpp"
#include "Base/ToggleComponent.hpp"

using namespace the;

namespace mp
{    
    class CameraOnGridSelection : public ToggleComponent
    {
        public:
            THE_ENTITY(CameraOnGridSelection, the::Component)

            ToValueR<QRectF> toSelectionRect;
            ToVec2 toCameraPos;

            PFloat prmSpeed;

            CameraOnGridSelection();

            void draw();

        private:

    };
}