////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/Config.hpp>
#include "Logic/Grid/GridSelectionVisualisation.hpp"

using namespace mp;



GridSelectionVisualisation::GridSelectionVisualisation()
{
    REGISTER(toSelectionRect).onValueChange([&]{ if (isInitialized()) updateSelection(); });
    REGISTER(toScene);

    REGISTER(prmSelectionColor);
        prmSelectionColor.a.defval(64);
        prmSelectionColor.r.defval(128);
        prmSelectionColor.g.defval(128);
        prmSelectionColor.b.defval(128);
    REGISTER(DoUpdate).setFunc(std::bind(&GridSelectionVisualisation::updateSelection, this));
}

GridSelectionVisualisation::~GridSelectionVisualisation()
{
}

void GridSelectionVisualisation::onDeinit()
{
    toScene->getScene()->removeItem(mSelectionRect);
    mSelectionRect = nullptr;
}

the::InitState GridSelectionVisualisation::onInit()
{
    // Create selection rect
    mSelectionRect = new QGraphicsRectItem();
    mSelectionRect->setPen(QPen(Qt::transparent));
    mSelectionRect->setBrush(QBrush(
        QColor(prmSelectionColor.r, prmSelectionColor.g, prmSelectionColor.b, prmSelectionColor.a)));
    toScene->getScene()->addItem(mSelectionRect);

    updateSelection();

    return true;
}

void GridSelectionVisualisation::updateSelection()
{
    mSelectionRect->setRect(toSelectionRect());
}
