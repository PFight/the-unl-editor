#include "Logic/Grid/CameraOnGridSelection.hpp"
#include "GridManager.hpp"

#include <The2D/GameLoopProcessor.hpp>
#include <The2D/Vec2.hpp>
#include "Config.hpp"

using namespace the;
using namespace mp;



CameraOnGridSelection::CameraOnGridSelection()
{
    REGISTER(toSelectionRect);
    REGISTER(toCameraPos);
    REGISTER(prmSpeed).defval(0.01);

    Enabled.EvActivated.SigRaised.add(this, [&](){ core()->loop()->subscribeToDraw(this); });
    Enabled.EvDeactivated.SigRaised.add(this, [&](){ core()->loop()->unsubscribeFromDraw(this); });
}

void CameraOnGridSelection::draw()
{
    Vec2 selPos = toVec2(toSelectionRect().center());
    //INFO ("sel pos: (%f, %f)", selScreenPos.x, selScreenPos.y);
    Vec2 cameraPos = toCameraPos();
    //INFO ("camera pos: (%f, %f)", cameraPos.x, cameraPos.y);

    Vec2 moveVector = selPos - cameraPos;
    //float moveVectorLength = sqrt(moveVector.x*moveVector.x + moveVector.y *moveVector.y);
    if(abs(moveVector.x) > 5 || abs(moveVector.y) > 5)
    {
        Vec2 shift = prmSpeed() * moveVector;
        toCameraPos = (cameraPos + shift);
        //INFO ("shift: (%f, %f)", shift.x, shift.y);
    }
}

