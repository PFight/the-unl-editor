////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/Config.hpp>
#include "Logic/Grid/GridManager.hpp"

using namespace mp;
using namespace the;

GridManager::GridManager()
{
    REGISTER(prmInitialCellSize).defval(50, 30);
    REGISTER(prmInitialSelection);
        prmInitialSelection.x.defval(0);
        prmInitialSelection.y.defval(0);
    
    REGISTER(CCellSize);
        CCellSize.defval(Vec2(prmInitialCellSize.width(), prmInitialCellSize.height()));
        CCellSize.setValueChangedEvent(&EvCellSizeChanged);

    REGISTER(CSelectedCells)
        .setAccessors(&mSelection)
        .setValueChangedEvent(EvSelectionChanged);
    REGISTER(GetRect).setForwardConv([&](const GridCellVector& vec){
        Vec2 upperLeft = getUpperLeft(vec, CCellSize());
        Vec2 lowerRight = getLowerRight(vec, CCellSize());
        QRectF rect;
        rect.setTopLeft(QPointF(upperLeft.x, upperLeft.y));
        rect.setBottomRight(QPointF(lowerRight.x, lowerRight.y));
        return rect;
    });

    REGISTER(EvSelectionChanged);
    REGISTER(EvCellSizeChanged);
}

GridManager::PGridCell::PGridCell()
{
    REGISTER(x);
    REGISTER(y);
}

GridManager::~GridManager()
{
}

the::InitState GridManager::onInit()
{
    CSelectedCells.push_back(GridCell(prmInitialSelection.x, prmInitialSelection.y));
    
    return true;
}

void GridManager::onDeinit()
{

}

Vec2 GridManager::getUpperLeft(int x, int y, Vec2 cellSize)
{
    return Vec2(cellSize.x * x, cellSize.y * y);
}

Vec2 GridManager::getUpperLeft(GridCell cell, Vec2 cellSize)
{
    return getUpperLeft(cell.x, cell.y, cellSize);
}

Vec2 GridManager::getCenter(int x, int y, Vec2 cellSize)
{
    return getUpperLeft(x, y, cellSize) + 0.5f*cellSize;
}

Vec2 GridManager::getCenter(GridCell cell, Vec2 cellSize)
{
    return getCenter(cell.x, cell.y, cellSize);
}

Vec2 GridManager::getCenter(const GridCellVector& selection, Vec2 cellSize)
{
    return 0.5*(getUpperLeft(selection, cellSize) + getLowerRight(selection, cellSize));
}
Vec2 GridManager::getUpperLeft(const GridCellVector& selection, Vec2 cellSize)
{
    return getUpperLeft(getUpperLeftCell(selection), cellSize);
}
Vec2 GridManager::getLowerRight(const GridCellVector& selection, Vec2 cellSize)
{
    return getUpperLeft(getLowerRightCell(selection), cellSize) + cellSize;
}
GridCell GridManager::getUpperLeftCell(const GridCellVector& selection)
{
    if (selection.size() > 0)
    {
        // Assume, that selection is rectangle
        int minX = 1000000000;
        int minY = 1000000000;
        for (auto cell : selection)
        {
            minX = eastl::min(cell.x, minX);
            minY = eastl::min(cell.y, minY);
        }
        return { minX, minY};
    }
    else
    {
        return {0, 0};
    }
}
GridCell GridManager::getLowerRightCell(const GridCellVector& selection)
{
    if (selection.size() > 0)
    {
        // Assume, that selection is rectangle
        int maxX = -100000000;
        int maxY = -1000000000;
        for (auto cell : selection)
        {
            maxX = eastl::max(cell.x, maxX);
            maxY = eastl::max(cell.y, maxY);
        }
        return { maxX, maxY};
    }
    else
    {
        return {0, 0};
    }
}



