#include "Logic/Grid/GridSelectionControll.hpp"


using namespace the;
using namespace mp;



GridSelectionControll::GridSelectionControll()
{
    REGISTER(toSelectedCells);
    REGISTER(DoMoveUp).setFunc(std::bind(&GridSelectionControll::moveSelection, this, MOVE_UP, false));
        //DoMoveUp.prmActivateKey.defval(Qt::Key_Up);
    REGISTER(DoMoveDown).setFunc(std::bind(&GridSelectionControll::moveSelection, this, MOVE_DOWN, false));
        //DoMoveDown.prmActivateKey.defval(Qt::Key_Down);
    REGISTER(DoMoveLeft).setFunc(std::bind(&GridSelectionControll::moveSelection, this, MOVE_LEFT, false));
        //DoMoveLeft.prmActivateKey.defval(Qt::Key_Left);
    REGISTER(DoMoveRight).setFunc(std::bind(&GridSelectionControll::moveSelection, this, MOVE_RIGHT, false));
        //DoMoveRight.prmActivateKey.defval(Qt::Key_Right);
    REGISTER(DoExtendUp).setFunc(std::bind(&GridSelectionControll::moveSelection, this, MOVE_UP, true));
        //DoExtendUp.prmActivateKey.defval(Qt::SHIFT + Qt::Key_Up);
    REGISTER(DoExtendDown).setFunc(std::bind(&GridSelectionControll::moveSelection, this, MOVE_DOWN, true));
        //DoExtendDown.prmActivateKey.defval(Qt::SHIFT + Qt::Key_Down);
    REGISTER(DoExtendLeft).setFunc(std::bind(&GridSelectionControll::moveSelection, this, MOVE_LEFT, true));
        //DoExtendLeft.prmActivateKey.defval(Qt::SHIFT + Qt::Key_Left);
    REGISTER(DoExtendRight).setFunc(std::bind(&GridSelectionControll::moveSelection, this, MOVE_RIGHT, true));
        //DoExtendRight.prmActivateKey.defval(Qt::SHIFT + Qt::Key_Right);
    REGISTER(DoCancelMultiselect).setFunc(std::bind(&GridSelectionControll::cancelMultiSelect, this));
        //DoCancelMultiselect.prmActivateKey.defval(Qt::Key_Escape);

    REGISTER(CEnabled) = true;
}

the::InitState GridSelectionControll::onInit()
{
    return true;
}
void GridSelectionControll::onDeinit()
{
}

void GridSelectionControll::cancelMultiSelect()
{
    if (!toSelectedCells.empty())
    {
        GridCell lastCell = toSelectedCells.back();
        toSelectedCells.clear();
        toSelectedCells.push_back(lastCell);
    }
}

void GridSelectionControll::moveSelection(MoveDirection dir, bool extend)
{
    if (!extend)
    {
        eastl::vector<GridCell> cells;

        cells = toSelectedCells();

        for (int i =0; i<toSelectedCells.size(); i++)
        {
            GridCell cur = toSelectedCells[i];
            switch(dir)
            {
                CASE(MOVE_UP) cur.y--; END_CASE;
                CASE(MOVE_DOWN) cur.y++; END_CASE;
                CASE(MOVE_LEFT) cur.x--; END_CASE;
                CASE(MOVE_RIGHT) cur.x++; END_CASE;
            }
            toSelectedCells.set(i, cur);

            mMultiSelectBegin = cur;
        }
        // INFO("Selected: %i, %i ", cur.x, cur.y);
    }
    else
    {
        // Assume, that selection is rectangle
        int maxX = -100000000;
        int minX = 1000000000;
        int maxY = -1000000000;
        int minY = 1000000000;
        for (auto cell : toSelectedCells())
        {
            maxX = eastl::max(cell.x, maxX);
            minX = eastl::min(cell.x, minX);
            maxY = eastl::max(cell.y, maxY);
            minY = eastl::min(cell.y, minY);
        }
        switch(dir)
        {
            CASE(MOVE_UP)
                for (int x=minX; x <= maxX; x++)
                {
                    if (minY == maxY || mLastExtendYIsUp)
                    {
                        toSelectedCells.push_back({x, minY - 1});
                        mLastExtendYIsUp = true;

                    }
                    else
                    {
                        toSelectedCells.remove({x, maxY});
                    }
                }
            END_CASE;
            CASE(MOVE_DOWN)
                for (int x=minX; x <= maxX; x++)
                {
                    if (minY == maxY || !mLastExtendYIsUp)
                    {
                        toSelectedCells.push_back({x, maxY + 1});
                        mLastExtendYIsUp = false;
                    }
                    else
                    {
                        toSelectedCells.remove({x, minY});
                    }
                }
            END_CASE;
            CASE(MOVE_LEFT)
                for (int y=minY; y <= maxY; y++)
                {
                    if (minX == maxX || !mLastExtendXIsRight)
                    {
                        toSelectedCells.push_back({minX-1, y});
                        mLastExtendXIsRight = false;
                    }
                    else
                    {
                        toSelectedCells.remove({maxX, y});
                    }
                }
            END_CASE;
            CASE(MOVE_RIGHT)
                for (int y=minY; y <=maxY; y++)
                {
                    if (minX == maxX || mLastExtendXIsRight)
                    {
                        toSelectedCells.push_back({maxX+1, y});
                        mLastExtendXIsRight = true;
                    }
                    else
                    {
                        toSelectedCells.remove({minX, y});
                    }
                }
            END_CASE;
        }

    }
}