////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/Config.hpp>
#include "Logic/Grid/GridVisualisation.hpp"
#include <QDebug>

using namespace mp;



GridVisualisation::GridVisualisation()
{
    REGISTER(toCellSize).onValueChange([&]{ if (isInitialized()) updateGrid(); });
    REGISTER(toScene);
    REGISTER(toLayout);

    REGISTER(prmGridColor);
        prmGridColor.a.defval(128);
        prmGridColor.r.defval(128);
        prmGridColor.g.defval(128);
        prmGridColor.b.defval(128);
    REGISTER(prmLineWidth).defval(1);

    REGISTER(DoUpdateGrid).setFunc(std::bind(&GridVisualisation::updateGrid, this));
}

GridVisualisation::~GridVisualisation()
{
}

void GridVisualisation::onDeinit()
{
}

the::InitState GridVisualisation::onInit()
{
    updateGrid();

    return true;
}

void GridVisualisation::updateGrid()
{
    // Calculate count of lines

    float width = toScene->getView()->viewport()->width();
    int verticalCount = (int)(width / toCellSize().x) + 4;

    float height = toScene->getView()->viewport()->height();
    int horizontalCount = (int)(height / toCellSize().y) + 4;

    // Add or remove lines to match desired count

    if ((int)mHorizontalLines.size() < horizontalCount)
    {
        while((int)mHorizontalLines.size() < horizontalCount)
        {
            auto line = new QGraphicsLineItem();

            line->setPen(QPen(
                QBrush(QColor(prmGridColor.r, prmGridColor.g, prmGridColor.b, prmGridColor.a)),
                prmLineWidth()));
            toScene->getScene()->addItem(line);
            mHorizontalLines.push_back(line);
        }
    }
    else
    {
        while ((int)mHorizontalLines.size() > horizontalCount)
        {
            auto line = mHorizontalLines.back();
            toScene->getScene()->removeItem(line);
            mHorizontalLines.pop_back();
        }
    }

    if ((int)mVerticalLines.size() < verticalCount)
    {
        while((int)mVerticalLines.size() < verticalCount)
        {
            auto line = new QGraphicsLineItem();
            line->setPen(QPen(
                QBrush(QColor(prmGridColor.r, prmGridColor.g, prmGridColor.b, prmGridColor.a)),
                prmLineWidth()));
            toScene->getScene()->addItem(line);
            mVerticalLines.push_back(line);
        }
    }
    else
    {
        while ((int)mVerticalLines.size() > verticalCount)
        {
            auto line = mVerticalLines.back();
            toScene->getScene()->removeItem(line);
            mVerticalLines.pop_back();
        }
    }

    // Update positions of the lines


    QPointF viewportPos = toScene->getView()->mapToScene(QPoint(0,0));
    float viewportWidth = toScene->getView()->viewport()->width();
    float viewportHeight = toScene->getView()->viewport()->height();
    // Find first visible line at the top
    int mostTop = (int) (viewportPos.y() / toCellSize().y)-2;
    // Find most left`
    int mostLeft = (int) (viewportPos.x() / toCellSize().x)-2;
    // Viewport rectangle
    float y1 = GridManager::getUpperLeft(mostLeft, mostTop, toCellSize()).y;
    float x1 = GridManager::getUpperLeft(mostLeft, mostTop, toCellSize()).x;
    float y2 = y1 + viewportHeight + 4*toCellSize().y;
    float x2 = x1 + viewportWidth + 4*toCellSize().x;
    // Fill viewport with lines
    float y = y1;
    for (auto line : mHorizontalLines)
    {
        QPointF from = toLayout->pointToItem(Vec2(x1, y), line);
        QPointF to = toLayout->pointToItem(Vec2(x2, y), line);
        line->setLine(from.x(), from.y(), to.x(), to.y());
        y += toCellSize().y;
    }
    float x = x1;
    for (auto line : mVerticalLines)
    {
        QPointF from = toLayout->pointToItem(Vec2(x, y1), line);
        QPointF to = toLayout->pointToItem(Vec2(x, y2), line);
        line->setLine(from.x(), from.y(), to.x(), to.y());
        x += toCellSize().x;
    }
}