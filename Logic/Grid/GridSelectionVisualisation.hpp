////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Bind.hpp>
#include <The2D/Component.hpp>
#include <The2D/Connectors.hpp>
#include <Logic/Grid/GridManager.hpp>
#include <Logic/SceneManager.hpp>

#include <QtWidgets/QGraphicsLineItem>
#include <QtWidgets/QGraphicsRectItem>

using namespace the;

////////////////////////////////////////////////////////////
namespace mp {

    class GridSelectionVisualisation : public the::Component
    {
        public:
            THE_ENTITY(GridSelectionVisualisation, the::Component)

            ToValueR<QRectF> toSelectionRect;
            ManagerBind<SceneManager> toScene;

            PColor prmSelectionColor;

            CCommand DoUpdate;

            GridSelectionVisualisation();
            virtual ~GridSelectionVisualisation();

        private:
            QGraphicsRectItem* mSelectionRect;

            InitState onInit() override;
            void onDeinit() override;

            void updateSelection();

    };
}; // namespace mp
////////////////////////////////////////////////////////////
