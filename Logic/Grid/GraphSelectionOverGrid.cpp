////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/Config.hpp>
#include "Logic/Grid/GraphSelectionOverGrid.hpp"
#include "Logic/Graph/Node.hpp"
#include "Logic/Graph/EdgeView.hpp"
#include "Logic/Graph/Edge.hpp"
#include <QGraphicsObject>

using namespace mp;



GraphSelectionOverGrid::GraphSelectionOverGrid()
{
    REGISTER(toSelectionRect);
    REGISTER(toSelectedItems);
    REGISTER(toScene);
    REGISTER(prmAutoSelect).defval(false);
    REGISTER(DoSelect).setFunc(std::bind(&GraphSelectionOverGrid::select, this));
        //DoSelect.prmActivateKey.defval(Qt::Key_Space);
}

GraphSelectionOverGrid::~GraphSelectionOverGrid()
{
}

void GraphSelectionOverGrid::onDeinit()
{
    if (prmAutoSelect)
    {
        toSelectionRect.SigValueChanged.remove(this);
    }
}

the::InitState GraphSelectionOverGrid::onInit()
{
    if (prmAutoSelect)
    {
        toSelectionRect.SigValueChanged.add(this, std::bind(&GraphSelectionOverGrid::select, this));
        select();
    }
    
    return true;
}

void GraphSelectionOverGrid::select()
{
    // Find items under selected cell
    QList<QGraphicsItem *> underSelectedCells = toScene->getScene()->items(toSelectionRect());

    // No way to call clearSelection, because already selected items should not be unselected
    eastl::vector<GraphItem*> diff = toSelectedItems();
    //toSelection->clearSelection();

    // Select first edge or node
    // TODO: select all edges and nodes
    for (auto item : underSelectedCells)
    {
        if (item->data(0) == NodeGraphicsItem::NodeGraphicsItemID)
        {
            NodeGraphicsItem* nodeItem = static_cast<NodeGraphicsItem*>(item);

            Node* node = nodeItem->getParentNodeView()->getNode();
            // Some components use "fantom nodes", that has only NodeView
            if (node != nullptr)
            {
                diff.remove(node);
                toSelectedItems.push_back(node);
            }
        }
        else if (item->data(0) == EdgeGraphicsItem::EdgeGraphicsItemID)
        {
            EdgeGraphicsItem* edgeItem = static_cast<EdgeGraphicsItem*>(item);
            Edge* edge = edgeItem->getParentEdgeView()->getEdge();
            // Some components use "fantom edges", that has only EdgeView
            if (edge != nullptr)
            {
                diff.remove(edge);
                toSelectedItems.push_back(edge);
            }
        }
    }
    for (auto item : diff)
    {
        toSelectedItems.remove(item);
    }
}
