////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/Config.hpp>
#include "Logic/AdjustEdgesLengthsDuringDrag.hpp"
#include "Graph/Node.hpp"
#include "Logic/Graph/Edge.hpp"

using namespace mp;
using namespace the;



AdjustEdgesLengthsDuringDrag::AdjustEdgesLengthsDuringDrag() 
{
    REGISTER(toSelectedItems);
    REGISTER(DoDragStart).setFunc(std::bind(&AdjustEdgesLengthsDuringDrag::dragStart, this));
    REGISTER(DoDragFinish).setFunc(std::bind(&AdjustEdgesLengthsDuringDrag::dragFinish, this));
}

AdjustEdgesLengthsDuringDrag::~AdjustEdgesLengthsDuringDrag()
{
}

void AdjustEdgesLengthsDuringDrag::onDeinit()
{

}

the::InitState AdjustEdgesLengthsDuringDrag::onInit()
{

    return true;
}

void AdjustEdgesLengthsDuringDrag::dragStart()
{
    mCurrentEdges.clear();
    
    for (auto item : toSelectedItems())
    {
        if (Node* node = item->toNode())
        {
            for (auto edge : node->mData.getEdges())
            {
                edge->mLayout.mJoint.deinit();
                mCurrentEdges.push_back(&edge->mLayout);                
            }
        }
    }
}

void AdjustEdgesLengthsDuringDrag::dragFinish()
{
    for (auto edgeLayout : mCurrentEdges)
    {
        if (!edgeLayout->mJoint.isInitialized())
        {
            edgeLayout->mJoint.prmForceAutoLength = true;
            edgeLayout->mJoint.init();
        }
    }
    mCurrentEdges.clear();
}