////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once


// Headers
#include <The2D/Bind.hpp>
#include <The2D/Component.hpp>
#include <The2D/Parameters.hpp>
#include <The2D/Connectors.hpp>
#include <The2D/CValueVector.hpp>
#include "Logic/AdjustEdgesLengthsDuringDrag.hpp"


using namespace the;

////////////////////////////////////////////////////////////
namespace mp {

    class GraphItem;

    class NodeKeyboardDrag : public Component
    {
        public:
            THE_ENTITY(NodeKeyboardDrag, Component)

            // Binds
            ToValueVectorR<GraphItem*> toSelectedItems;
            Bind<AdjustEdgesLengthsDuringDrag> toEdgeCorrector;
            CVec2 toCellSize;
            
            // Parameters
            // Connectors

            CCommand DoMoveUp;
            CCommand DoMoveDown;
            CCommand DoMoveLeft;
            CCommand DoMoveRight;

            NodeKeyboardDrag();
            virtual ~NodeKeyboardDrag();



        private:
            InitState onInit() override;
            void onDeinit() override;

            enum Direction
            {
                UP,
                DOWN,
                LEFT,
                RIGHT
            };

            void move(Direction dir);
            
    };
}; // namespace mp
////////////////////////////////////////////////////////////