#include "Config.hpp"
#include "Logic/EdgeCreation.hpp"
#include "Logic/Graph/Graph.hpp"

#include "Logic/MainWindowManager.hpp"
#include "Logic/Graph/Graph.hpp"

#include <QtWidgets/QMessageBox>

using namespace mp;
using namespace the;



EdgeCreation::EdgeCreation(): mEnableSelectionMonitoring(false)
{
    REGISTER(toSelectedItems).onValueChange([&]{ onGraphSelectionChanged(); });
    //REGISTER(prmEdgeCreationModeNotify);
    REGISTER(toMainWindow);
    REGISTER(toGraph);
    REGISTER(toSelectionRect).onValueChange([&]{ onGridSelectionChanged(); });
    REGISTER(toGraphHelper);
    REGISTER(DoStart).setFunc(std::bind(&EdgeCreation::startAddEdge, this));
    REGISTER(DoFinish).setFunc(std::bind(&EdgeCreation::finishAddEdge, this));
    REGISTER(DoCancel).setFunc(std::bind(&EdgeCreation::cancelAddEdge, this));
    REGISTER(mFantomNode).setInitOrder(BEFORE_PARENT);
}

the::InitState EdgeCreation::onInit()
{
    mFantomNode.getSceneItem()->setOpacity(0);

    return true;
}
void EdgeCreation::onDeinit()
{
}

void EdgeCreation::startAddEdge()
{
    ASSERT(mNewEdgeBegin == nullptr, "Previous edge creation was not finished.");

    for (auto item : toSelectedItems())
    {
        if (Node* node = item->toNode())
        {
            EdgeView* fantomEdge = new EdgeView();
            REGISTER(fantomEdge);
            fantomEdge->toFirstNode.set(&node->mView);
            fantomEdge->toSecondNode.set(&mFantomNode);
            fantomEdge->toText.set("*");
            mFantomEdges.push_back(make_sptr(fantomEdge));
            fantomEdge->init();
        }
    }

    if (mFantomEdges.size() == 0)
    {
        WARN("No selected nodes, can't add edge");
    }
    else
    {
        mEnableSelectionMonitoring = true;
    }
}


void EdgeCreation::finishAddEdge()
{
    mEnableSelectionMonitoring = false;

    if (mFantomEdges.size() > 0)
    {
        // Target node is end point of new edges
        Node* targetNode = nullptr;
        if (mFantomEdges.front()->toSecondNode.Get() == &mFantomNode)
        {
            // No target node selected, create new one
            auto newNodeSptr = toGraphHelper->createNode();
            newNodeSptr->mLayout.prmPosition.set(mFantomNode.toPosition().x, mFantomNode.toPosition().y);
            toGraph()->addNode(newNodeSptr, true);
            targetNode = newNodeSptr.get();
        }
        else
        {
            targetNode = mFantomEdges.front()->toSecondNode.Get()->getNode();
        }

        toSelectedItems.clear();

        for (auto fantomEdge : mFantomEdges)
        {
            sptr<Edge> newEdge = toGraphHelper->createEdge(
                fantomEdge->toFirstNode()->getNode(), targetNode);
            toGraph()->addEdge(newEdge);
            toSelectedItems.push_back(newEdge.get());
            fantomEdge->deinit();
            UNREGISTER(fantomEdge);
        }
        mFantomEdges.clear();
    }


}

void EdgeCreation::cancelAddEdge()
{
    mEnableSelectionMonitoring = false;

    for (auto fantomEdge : mFantomEdges)
    {
        fantomEdge->deinit();
        UNREGISTER(fantomEdge);
    }
    mFantomEdges.clear();
}

void EdgeCreation::onGraphSelectionChanged()
{
    if (mEnableSelectionMonitoring)
    {
        NodeView* fantomEdgesTarget = &mFantomNode;

        for (auto item : toSelectedItems())
        {
            if (Node* node = item->toNode())
            {
                // Do not create circular edges
                bool nodeIsOneOfSource  = false;
                for (auto fantomEdge : mFantomEdges)
                {
                    if (fantomEdge->toFirstNode->getNode() == node)
                    {
                        nodeIsOneOfSource = true;
                        break;
                    }
                }
                if (!nodeIsOneOfSource)
                {
                    fantomEdgesTarget = &node->mView;
                }
            }
        }

        for (auto& fantomEdge : mFantomEdges)
        {
            fantomEdge->deinit();
            fantomEdge->toSecondNode.set(fantomEdgesTarget);
            fantomEdge->init();
        }
    }
}

void EdgeCreation::onGridSelectionChanged()
{
    mFantomNode.toPosition = toVec2(toSelectionRect().center());

    for (auto& fantomEdge : mFantomEdges)
    {
        fantomEdge->DoUpdatePosition();
    }
}