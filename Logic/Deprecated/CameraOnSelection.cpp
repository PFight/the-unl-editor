#include "Logic/CameraOnSelection.hpp"
#include "Logic/Graph/Node.hpp"
#include <The2D/GameLoopProcessor.hpp>
#include <The2D/Vec2.hpp>

using namespace the;
using namespace mp;



CameraOnSelection::CameraOnSelection(): mEnabled(false)
{
    REGISTER(CEnabled)
        .setGetter(&mEnabled)
        .setSetter([&](bool val){
            if (val) core()->loop()->subscribeToDraw(this);
            else core()->loop()->unsubscribeFromDraw(this);
            mEnabled = val;
        });
    REGISTER(toSelectionManager);
    REGISTER(toCameraManager);
    REGISTER(prmSpeed).defval(0.01);
}

the::InitState CameraOnSelection::onInit()
{
    return true;
}

void CameraOnSelection::draw()
{
    if (toSelectionManager->getSelectedNode() != nullptr)
    {
        Vec2 selPos = toSelectionManager->getSelectedNode()->mLayout.CPosition;
        //INFO ("sel pos: (%f, %f)", selScreenPos.x, selScreenPos.y);
        Vec2 cameraPos = toCameraManager->CPosition;
        //INFO ("camera pos: (%f, %f)", cameraPos.x, cameraPos.y);

        Vec2 moveVector = selPos - cameraPos;
        //float moveVectorLength = sqrt(moveVector.x*moveVector.x + moveVector.y *moveVector.y);
        if(abs(moveVector.x) > 5 || abs(moveVector.y) > 5)
        {
            Vec2 shift = prmSpeed() * moveVector;
            toCameraManager->CPosition = (cameraPos + shift);
            //INFO ("shift: (%f, %f)", shift.x, shift.y);
        }
    }
}

void CameraOnSelection::onSelectionChanged()
{
}

void CameraOnSelection::onDeinit()
{
}
