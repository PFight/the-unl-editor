#include "Logic/ArrowSelection.hpp"
#include "Logic/Graph/Node.hpp"
#include "Logic/Graph/Edge.hpp"

using namespace the;
using namespace mp;



ArrowSelection::ArrowSelection()
{
    REGISTER(toSelection);
    REGISTER(toScene);
    REGISTER(CEnabled)
        .setGetter(&mEnabled)
        .setSetter(std::bind(&ArrowSelection::enable, this, std::placeholders::_1));
}

the::InitState ArrowSelection::onInit()
{
    return true;
}

void ArrowSelection::moveSelection(MoveDirection dir)
{
    Node* curSelNode = toSelection->getSelectedNode();
    if(curSelNode != nullptr)
    {
        Edge* selEdge = toSelection->getSelectedEdge();
        // If no edge selected, then select nearest to direction edge
        if(selEdge == nullptr)
        {
            if(curSelNode->mData.getEdges().size() > 0)
            {
                b2Vec2 desiredDir(0,0);
                switch(dir)
                {
                    case MOVE_UP: desiredDir = b2Vec2(0, 1); break;
                    case MOVE_DOWN:  desiredDir = b2Vec2(0, -1); break;
                    case MOVE_LEFT: desiredDir = b2Vec2(-1, 0); break;
                    case MOVE_RIGHT:  desiredDir = b2Vec2(1, 0); break;
                }

                Edge* mostGood = curSelNode->mData.getEdges().front();
                float mostGoodCosine = -1; // worsest case
                for(Edge* edge : curSelNode->mData.getEdges())
                {
                    Node* other = (edge->toFirstNode() == curSelNode)? edge->toSecondNode() : edge->toFirstNode();
                    b2Vec2 edgeDir = other->mLayout.mBody.getBody()->GetPosition() - curSelNode->mLayout.mBody.getBody()->GetPosition();
                    edgeDir.Normalize();
                    float cosine = b2Dot(desiredDir, edgeDir);
                    // If cosine bigger, then angle smaller and dirrection more accurate
                    if(cosine > mostGoodCosine)
                    {
                        mostGood = edge;
                        mostGoodCosine = cosine;
                    }
                }
                const float cosPi_div_4 = 0.7;
                if(mostGoodCosine > cosPi_div_4)
                {
                    toSelection->setSelectedEdge(mostGood);
                }
            }
            else
            {
                WARN("Node without edges. I don't know what to do.");
            }
        }
        // Some edge already selected, so move round or to the next node
        else
        {
            Node* other = (selEdge->toFirstNode() == curSelNode)? selEdge->toSecondNode() : selEdge->toFirstNode();
            b2Vec2 edgeDir = other->mLayout.mBody.getBody()->GetPosition() - curSelNode->mLayout.mBody.getBody()->GetPosition();
            edgeDir.Normalize();

            float cosPi_div_4 = 0.7;
            bool edgeUpDir = b2Dot(edgeDir, b2Vec2(0, 1)) >= cosPi_div_4;
            bool edgeDownDir =  b2Dot(edgeDir, b2Vec2(0, -1)) > cosPi_div_4;
            bool edgeRightDir =  b2Dot(edgeDir, b2Vec2(1, 0)) >= cosPi_div_4;
            bool edgeLeftDir =  b2Dot(edgeDir, b2Vec2(-1, 0)) > cosPi_div_4;

            // If continue moving to the direction, then go to the next node
            if((edgeUpDir && dir == MOVE_UP) ||
               (edgeDownDir && dir == MOVE_DOWN) ||
               (edgeRightDir && dir == MOVE_RIGHT) ||
               (edgeLeftDir && dir == MOVE_LEFT))
            {
                toSelection->setSelectedEdge(nullptr);
                toSelection->setSelectedNode(other);
            }
            // Opposite moving direction, deselect edge (select node)
            else if((edgeUpDir && dir == MOVE_DOWN) ||
                    (edgeDownDir && dir == MOVE_UP) ||
                    (edgeRightDir && dir == MOVE_LEFT) ||
                    (edgeLeftDir && dir == MOVE_RIGHT))
            {
                toSelection->setSelectedEdge(nullptr);
            }
            // Move direction different from edge direction, so move round to the next edge
            else
            {
                if(edgeUpDir)
                {
                    auto newEdgeSel = moveRound(dir == MOVE_LEFT, selEdge, curSelNode);
                    toSelection->setSelectedEdge(newEdgeSel);
                }
                else if(edgeDownDir)
                {
                    auto newEdgeSel = moveRound(dir == MOVE_RIGHT, selEdge, curSelNode);
                    toSelection->setSelectedEdge(newEdgeSel);
                }
                else if(edgeLeftDir)
                {
                    auto newEdgeSel = moveRound(dir == MOVE_DOWN, selEdge, curSelNode);
                    toSelection->setSelectedEdge(newEdgeSel);
                }
                else // if (edgeRightDir)
                {
                    auto newEdgeSel = moveRound(dir == MOVE_UP, selEdge, curSelNode);
                    toSelection->setSelectedEdge(newEdgeSel);
                }
            }
        }
    }
    else
    {
        WARN("Lost selection. I don't know what to do.");
    }
}

Edge* ArrowSelection::moveRound(bool toRight, Edge* selEdge, Node* curSelNode)
{
    Node* other = (selEdge->toFirstNode() == curSelNode)? selEdge->toSecondNode() : selEdge->toFirstNode();
    b2Vec2 desiredDir = other->mLayout.mBody.getBody()->GetPosition() - curSelNode->mLayout.mBody.getBody()->GetPosition();
    desiredDir.Normalize();
    float cosPi_div_4 = 0.7;
    int desiredDirNum = 0;
    if(b2Dot(desiredDir, b2Vec2(0, -1)) >= cosPi_div_4) desiredDirNum = 0;
    else if(b2Dot(desiredDir, b2Vec2(0, 1)) > cosPi_div_4) desiredDirNum = 1;
    else if(b2Dot(desiredDir, b2Vec2(1, 0)) >= cosPi_div_4) desiredDirNum = 2;
    else if(b2Dot(desiredDir, b2Vec2(-1, 0)) > cosPi_div_4) desiredDirNum = 3;

    Edge* mostGood = nullptr;
    float mostGoodCosine = -1; // worsest case
    for(Edge* edge : curSelNode->mData.getEdges())
    {
        if(edge != selEdge)
        {
            Node* other = (edge->toFirstNode() == curSelNode)? edge->toSecondNode() : edge->toFirstNode();
            b2Vec2 edgeDir = other->mLayout.mBody.getBody()->GetPosition() - curSelNode->mLayout.mBody.getBody()->GetPosition();
            edgeDir.Normalize();
            int edgeDirNum = 0;
            if(b2Dot(edgeDir, b2Vec2(0, -1)) >= cosPi_div_4) edgeDirNum = 0;
            else if(b2Dot(edgeDir, b2Vec2(0, 1)) > cosPi_div_4) edgeDirNum = 1;
            else if(b2Dot(edgeDir, b2Vec2(1, 0)) >= cosPi_div_4) edgeDirNum = 2;
            else if(b2Dot(edgeDir, b2Vec2(-1, 0)) > cosPi_div_4) edgeDirNum = 3;

            // Look only for the edges at same side
            if(desiredDirNum == edgeDirNum)
            {
                // Look only at vectors the needed direction
                if((b2Cross(edgeDir, desiredDir) < 0 && toRight) || (b2Cross(edgeDir, desiredDir) > 0 && !toRight))
                {
                    float cosine = b2Dot(desiredDir, edgeDir);
                    // If cosine bigger, then angle smaller and direction more accurate
                    if(cosine > mostGoodCosine)
                    {
                        mostGood = edge;
                        mostGoodCosine = cosine;
                    }
                }
            }
        }
    }

    // At the needed direction doesn't any edge, so look for bigges angle in opposite direction
    /*if(mostGood == nullptr)
    {

        mostGoodCosine = 1; // worsest case
        for(Edge* edge : curSelNode->mEdges)
        {
            Node* other = (edge->prmFrom == curSelNode)? edge->prmTo : edge->prmFrom;
            b2Vec2 edgeDir = other->prmNodeBody.getBody()->GetPosition() - curSelNode->prmNodeBody.getBody()->GetPosition();
            edgeDir.Normalize();
            // Look only at vectors the needed direction
            if(b2Cross(edgeDir, desiredDir) < 0 && toRight || b2Cross(edgeDir, desiredDir) > 0 && !toRight)
            {
                float cosine = b2Dot(desiredDir, edgeDir);
                // If cosine smaller, then angle bigger
                if(cosine < mostGoodCosine)
                {
                    mostGood = edge;
                    mostGoodCosine = cosine;
                }
            }
        }
    }*/

    if(mostGood == nullptr)
    {
        // Looks like no good edges at all
        mostGood = selEdge;
    }

    return mostGood;
}

void ArrowSelection::onDeinit()
{
}

bool ArrowSelection::eventFilter(QObject * watched, QEvent * event)
{
    if (event->type() == QEvent::KeyPress)
    {
        QKeyEvent* ev = static_cast<QKeyEvent*>(event);
        bool catched = true;
        switch(ev->key())
        {
            case Qt::Key_Up: moveSelection(MOVE_UP); break;
            case Qt::Key_Down: moveSelection(MOVE_DOWN); break;
            case Qt::Key_Left: moveSelection(MOVE_LEFT); break;
            case Qt::Key_Right: moveSelection(MOVE_RIGHT); break;
            default: catched = false; break;
        }
        return catched;
    }
    else
    {
        return false;
    }
}

void ArrowSelection::enable(bool enable)
 {
    if (mEnabled != enable)
    {
        mEnabled = enable;
        if (enable)
        {
           toScene->getScene()->installEventFilter(this);
        }
        else
        {
           toScene->getScene()->removeEventFilter(this);
        }
    }
 }