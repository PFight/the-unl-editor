#pragma once

#include <The2D/Component.hpp>
#include <The2D/Connectors.hpp>
#include <The2D/Bind.hpp>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QLabel>

#include "Logic/SceneManager.hpp"
#include "Logic/UserInput/InputEvent.hpp"

using namespace the;

class QResizeEvent;

namespace mp
{
    class MainWindowManager;
    class CurrentSentenceEditor;

    class CurrentSentenceEditorWidget : public QTextEdit
    {
        Q_OBJECT
        public:
            CurrentSentenceEditorWidget(CurrentSentenceEditor* parentComponent);

            CurrentSentenceEditor* getParentComponent();

        protected:
            CurrentSentenceEditor* mParentComponent;

            bool event ( QEvent * event ) override;

            friend class CurrentSentenceEditor;
    };

    class CurrentSentenceEditor : public the::Component
    {
        public:
            THE_ENTITY(CurrentSentenceEditor, the::Component)

            ManagerBind<mp::MainWindowManager> toMainWindow;
            ManagerBind<SceneManager> toScene; // Should be initialized before me

            CString CText;
            CBool CHasFocus;

            CEvent EvTextChanged;

            CurrentSentenceEditor();

            QLabel* getEditWidget() const
            {
                return mEdit;
            }

        private:
            QLabel* mEdit;

            the::InitState onInit();
            void onDeinit();

            void mainWindowSizeChanged(QResizeEvent* ev);

            void setFocus(bool focus);
    };
}