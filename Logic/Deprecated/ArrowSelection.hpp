#pragma once

#include <The2D/Component.hpp>
#include <The2D/Bind.hpp>
#include <The2D/Connectors.hpp>

#include "Logic/SelectionManager.hpp"
#include "Logic/SceneManager.hpp"

using namespace the;

namespace mp
{
    class Edge;
    class Node;

    class ArrowSelection : public QObject, public the::Component
    {
        Q_OBJECT
        
        public:
            THE_ENTITY(ArrowSelection, the::Component)

            ManagerBind<SceneManager> toScene;
            ManagerBind<SelectionManager> toSelection;

            CBool CEnabled;

            ArrowSelection();

            enum MoveDirection
            {
                MOVE_UP,
                MOVE_DOWN,
                MOVE_LEFT,
                MOVE_RIGHT
            };

            void moveSelection(MoveDirection dir);

        private:
            bool mEnabled;
            Edge* moveRound(bool toRight, Edge* edge, Node* node);

            the::InitState onInit();
            void onDeinit();

            bool eventFilter(QObject * watched, QEvent * event) override;

            void enable(bool enable);
    };
}