#pragma once

#include <The2D/Component.hpp>
#include <The2D/Parameters.hpp>
#include <The2D/Bind.hpp>

#include "Logic/SelectionManager.hpp"
#include "Logic/CameraManager.hpp"

using namespace the;

namespace mp
{    
    class CameraOnSelection : public the::Component
    {
        public:
            THE_ENTITY(CameraOnSelection, the::Component)

            CBool CEnabled;

            ManagerBind<SelectionManager> toSelectionManager;
            ManagerBind<CameraManager> toCameraManager;

            PFloat prmSpeed;

            CameraOnSelection();

            void draw();
            void onSelectionChanged();

        private:
            bool mEnabled;
            the::InitState onInit();
            void onDeinit();
    };
}