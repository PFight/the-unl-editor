#include "Logic/UserInput/CurrentSentenceEditor.hpp"
#include "Logic/MainWindowManager.hpp"
#include <QResizeEvent>


using namespace the;
using namespace mp;



CurrentSentenceEditorWidget::CurrentSentenceEditorWidget(CurrentSentenceEditor* parent): mParentComponent(parent)
{
}

CurrentSentenceEditor* CurrentSentenceEditorWidget::getParentComponent()
{
    return mParentComponent;
}

bool CurrentSentenceEditorWidget::event ( QEvent * event )
{
    // mParentComponent->SigSceneEvent(event);
    return QTextEdit::event(event);
}

CurrentSentenceEditor::CurrentSentenceEditor(): mEdit(nullptr)
{
    REGISTER(toMainWindow);
    REGISTER(CText);
        //.setGetter([&]{ mEdit
    REGISTER(CHasFocus)
        .setGetter([&](bool& result){ result = mEdit->hasFocus(); })
        .setSetter(std::bind(&CurrentSentenceEditor::setFocus, this, std::placeholders::_1));
}

the::InitState CurrentSentenceEditor::onInit()
{
    mEdit = new QLabel(nullptr);
    mEdit->setText("there threr hoi lal la ");
    mEdit->setParent(toMainWindow->getWindow());
    mEdit->setSizeIncrement(1, 1);
    mEdit->show();
    toMainWindow->getWindow()->SigResize.add(this, std::bind(&CurrentSentenceEditor::mainWindowSizeChanged,
        this, std::placeholders::_1));
    QSize winSize = toMainWindow->getWindow()->size();
    mEdit->move(winSize.width()/2.0f - mEdit->size().width()/2.0f, 2);
    mEdit->raise();

    CHasFocus = false;

    return true;
}

void CurrentSentenceEditor::onDeinit()
{
    toMainWindow->getWindow()->SigResize.remove(this);
    delete mEdit;
}

void CurrentSentenceEditor::mainWindowSizeChanged(QResizeEvent* ev)
{
    mEdit->move(ev->size().width()/2.0f - mEdit->size().width()/2.0f, 2);
}

void CurrentSentenceEditor::setFocus(bool focus)
{
    if (focus)
    {
        mEdit->setFocusPolicy(Qt::FocusPolicy::StrongFocus);
        mEdit->setFocus();
    }
    else
    {
        mEdit->setFocusPolicy(Qt::FocusPolicy::NoFocus);
    }
}