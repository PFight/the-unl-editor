#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Bind.hpp>
#include <The2D/Vec2.hpp>

#include "Logic/SceneManager.hpp"

using namespace the;

////////////////////////////////////////////////////////////
namespace mp {

    class CameraManager : public the::Manager
    {
        public:
            THE_ENTITY(CameraManager, the::Manager)

            ManagerBind<SceneManager> toSceneManager;

            CVec2 CPosition;

            CEvent EvPositionChanged;

            CameraManager();
            virtual ~CameraManager();

        private:
            Vec2 mPosition;
            InitState onInit() override;
            void onDeinit() override;

            void setPosition(Vec2 pos);
            void getPosition(Vec2& result);

    };
}; // namespace mp
////////////////////////////////////////////////////////////
