////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Manager.hpp>
#include <The2D/Bind.hpp>
#include "Logic/MainWindowManager.hpp"

#include <QGraphicsView>
#include <QGraphicsScene>

////////////////////////////////////////////////////////////
namespace mp
{

    using namespace the;

    class SceneManager: public QObject, public the::Manager
    {
        Q_OBJECT
        
        public:

            THE_ENTITY(SceneManager, the::Manager)

            the::ManagerBind<MainWindowManager> toMainWindow;

            SceneManager();

            QGraphicsScene* getScene(){ return mScene; }
            QGraphicsView* getView() { return mView; }

        private:
            QGraphicsScene* mScene;
            QGraphicsView* mView;

            void windowResize(QResizeEvent * event);

            bool eventFilter(QObject* watcher, QEvent* ev);

            the::InitState onInit();
            void onDeinit();

    };

}; // namespace mp
////////////////////////////////////////////////////////////
