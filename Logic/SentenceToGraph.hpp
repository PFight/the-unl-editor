////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Bind.hpp>
#include <The2D/Component.hpp>
#include <The2D/Connectors.hpp>
#include "Logic/MainWindowManager.hpp"
#include "Logic/GraphManager.hpp"
#include "Logic/Graph/Graph.hpp"

using namespace the;


////////////////////////////////////////////////////////////
namespace mp {

    // Extracts current sentence from text edit, creates (if not exists) for it graph,
    // and activates it (with deactivating previous graph)
    class SentenceToGraph : public the::Component
    {
        public:
            THE_ENTITY(SentenceToGraph, the::Component)

            ManagerBind<MainWindowManager> toMainWindow;
            ToValue<Graph*> toGraph;
            ToFunc<void()> toCreateBlankGraph;
            ToFunc<sptr<Graph>()> toCreateGraph;

            CFuncBase<Graph*(QString)> CGraphBySentence;
            CValueR<eastl::vector<QString>> CSentences;
            CValue<QString> CText;
            CCommand DoActivateSelected;

            CString CCurrentFile;

            CEvent EvSentencesChanged;

            SentenceToGraph();
            virtual ~SentenceToGraph();

        private:
            QString mAllText;
            eastl::vector<QString> mSentences;
            std::map<QString, sptr<Graph>> mSentenceToGraph;
            QMetaObject::Connection mTextChangedConnection;
            QMetaObject::Connection mSaveConnection;
            QMetaObject::Connection mSaveAsConnection;
            QMetaObject::Connection mOpenConnection;

            InitState onInit() override;
            void onDeinit() override;

            void parseSentences();
            QString getSentenceFromPos(int pos, int* startPos, int* endPos);

            void activateSentence();

            void save();
            void saveAs();
            void open();

    };
}; // namespace mp
////////////////////////////////////////////////////////////
