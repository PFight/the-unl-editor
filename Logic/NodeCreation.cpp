#include "Logic/NodeCreation.hpp"

#include "Logic/NodeEdgeEdit.hpp"
#include "Logic/Graph/Graph.hpp"
#include "Logic/Graph/Node.hpp"
#include "Logic/Graph/Edge.hpp"

#include <cstdlib>
#include <time.h>

using namespace the;
using namespace mp;



NodeCreation::NodeCreation()
{
    REGISTER(toSelectedItems);
    REGISTER(toSelectionRect);
    REGISTER(toScene);
    REGISTER(toEditor);
    REGISTER(toGraph);
    REGISTER(DoAddNode).setFunc(std::bind(&NodeCreation::addNode, this));
    REGISTER(EvNodeCreated);

    srand ( time(NULL) );
}

void NodeCreation::addNode()
{
    std::shared_ptr<Node> node(new Node());
    node->mData.CText = "New node";

    // Find new node position
    Vec2 nodePos;
    if (!toSelectionRect().isEmpty())
    {
        nodePos = toVec2(toSelectionRect().center());
    }
    else if (toSelectedItems.size() > 0)
    {
        Vec2 summ(0,0);
        int count = 0;
        for (GraphItem* item : toSelectedItems())
        {
            if (Node* node = item->toNode())
            {
                summ += node->mLayout.CPosition + Vec2((rand() % 10 - 5)/50.0f, (rand() % 10 - 5)/50.0f);
                count++;
            }
        }
        nodePos = (1.0f/count) * summ;
    }
    else
    {
        nodePos = Vec2(toScene->getView()->rect().center().x(), toScene->getView()->rect().center().y());
    }
    node->mLayout.prmPosition.set(nodePos.x, nodePos.y);

    toGraph()->addNode(node);

    // Create edges with all selected nodes
    if (toSelectedItems.size() > 0)
    {
        for (auto item : toSelectedItems)
        {
            if (Node* other = item->toNode())
            {
                std::shared_ptr<Edge> edge(new Edge());
                node->mData.addEdge(edge.get());
                other->mData.addEdge(edge.get());
                edge->mData.CText = "*";
                edge->toFirstNode.set(other);
                edge->toSecondNode.set(node.get());
                toGraph()->addEdge(edge);
            }
        }
    }

    toSelectedItems.clear();
    toSelectedItems.push_back(node.get());

    EvNodeCreated();

      // If rejected, remove knot and edge
        /*if(!edgeAccepted || !nodeAccepted)
        {
            toSelection->setSelectedNode(selNode);
            toSelection->setSelectedEdge(nullptr);
            node->mData.removeEdge(edge.get());
            selNode->mData.removeEdge(edge.get());
            toGraph->removeNode(node.get());
            toGraph->removeEdge(edge.get());
        }*/
}