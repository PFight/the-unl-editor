////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/Config.hpp>
#include "Logic/UserInput/KeyDragEvents.hpp"

using namespace mp;
using namespace the;

KeyDragEvents::KeyDragEvents()
{
    REGISTER(EvMoveNodesUp).prmKey.defval(Qt::Key_Control + Qt::Key_Up);
    REGISTER(EvMoveNodesDown).prmKey.defval(Qt::Key_Control + Qt::Key_Down);
    REGISTER(EvMoveNodesLeft).prmKey.defval(Qt::Key_Control + Qt::Key_Left);
    REGISTER(EvMoveNodesRight).prmKey.defval(Qt::Key_Control + Qt::Key_Right);

    REGISTER(DoProcessSceneKeyEvent).setFunc(std::bind(&KeyDragEvents::processSceneKeyEvent, this,
        std::placeholders::_1, std::placeholders::_2));

    mMyEvents.push_back(&EvMoveNodesUp);
    mMyEvents.push_back(&EvMoveNodesDown);
    mMyEvents.push_back(&EvMoveNodesLeft);
    mMyEvents.push_back(&EvMoveNodesRight);

    REGISTER(EvMoveEvent);
        Bind<CEvent>* b = new Bind<CEvent>();
        b->set(&EvMoveNodesDown);
        EvMoveEvent.add(make_sptr(b));
        b = new Bind<CEvent>();
        b->set(&EvMoveNodesUp);
        EvMoveEvent.add(make_sptr(b));
        b = new Bind<CEvent>();
        b->set(&EvMoveNodesLeft);
        EvMoveEvent.add(make_sptr(b));
        b = new Bind<CEvent>();
        b->set(&EvMoveNodesRight);
        EvMoveEvent.add(make_sptr(b));
}

KeyDragEvents::~KeyDragEvents()
{
}

void KeyDragEvents::processSceneKeyEvent(QKeyEvent * event, QKeySequence seq)
{
    for (CInputEvent* inputEvent : mMyEvents)
    {
        if (inputEvent->prmKey().matches(seq) && inputEvent->prmType == event->type())
        {
            inputEvent->raise();
        }
    }
}