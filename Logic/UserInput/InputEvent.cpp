
#include "Logic/UserInput/InputEvent.hpp"

using namespace the;
using namespace mp;




CInputEvent::CInputEvent()
{
    REGISTER(prmKey).setRequired(false);
    REGISTER(prmType).defval(QEvent::KeyPress)
        .addToMap("KeyPress", QEvent::KeyPress)
        .addToMap("KeyRelease", QEvent::KeyRelease);
}
