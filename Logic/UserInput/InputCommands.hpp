
#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Bind.hpp>
#include <The2D/Component.hpp>
#include <The2D/Connectors.hpp>
#include "Logic/SceneManager.hpp"
#include "Logic/MainWindowManager.hpp"
#include "Logic/UserInput/InputEvent.hpp"
#include "Logic/MouseHoverGraph.hpp"

#include <QKeySequence>


////////////////////////////////////////////////////////////
namespace mp {


    class InputCommands : public QObject, public the::ChildList<CInputEvent>
    {
        Q_OBJECT

        public:
            THE_ENTITY(InputCommands, the::ChildList<CInputEvent>)

            ManagerBind<SceneManager> toScene;
            ManagerBind<MainWindowManager> toMainWindow;
            Bind<MouseHoverGraph> toMouseProcessor;

            CArgEvent<void(QKeyEvent * event, QKeySequence seq)> EvSceneKeyEvent;

            CEvent EvAnyKey;
            CEvent EvMainMouseDownOnEmpty;
            CEvent EvMainMouseDownOnNode;
            CEvent EvMouseSelectItem;
            CEvent EvMainMouseUp;
            CEvent EvMouseAddToSelection;
            CEvent EvSelectSentence;

            CInputEvent EvAccept;
            CInputEvent EvCancel;

            CInputEvent EvSwitchTextWorkspace;
            CEvent EvTextEditorFocused;
            CEvent EvWorkspaceFocused;

            CEvent EvWindowClose;
            CEvent EvWindowResize;
            CEvent EvWindowMove;
            
            InputCommands();
            virtual ~InputCommands();

        public slots:

            void selectButtonClicked(bool checked = false);

        private:
            InitState onInit() override;
            void onDeinit() override;

            bool eventFilter(QObject * watched, QEvent * event) override;

            QKeySequence getKeySequence(QKeyEvent* ev);

    };
}; // namespace mp
////////////////////////////////////////////////////////////
