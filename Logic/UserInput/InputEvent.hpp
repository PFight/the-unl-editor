#pragma once


#include <The2D/Connectors.hpp>
#include "QtParams/PQtKeySequence.hpp"
#include <QEvent>

using namespace the;

namespace mp
{
    
    class CInputEvent : public the::CEvent
    {
        public:
            THE_ENTITY(CInputEvent, the::CEvent)

            PQtKeySequence prmKey;
            PMapped<QEvent::Type> prmType;

            CInputEvent();

    };
}