#include "Logic/UserInput/InputCommands.hpp"
#include "Logic/MouseHoverGraph.hpp"
#include <QGraphicsScene>
#include <QEvent>
#include <QKeyEvent>
#include <QApplication>
#include <QGraphicsSceneMouseEvent>
#include "mainwindow.h"
#include "ui_mainwindow.h"

using namespace mp;



InputCommands::InputCommands()
{
    REGISTER(toScene);
    REGISTER(toMainWindow);
    REGISTER(EvAnyKey);
    REGISTER(toMouseProcessor);
    REGISTER(EvSceneKeyEvent);
    REGISTER(EvMainMouseDownOnEmpty);
    REGISTER(EvMainMouseDownOnNode);
    REGISTER(EvMainMouseUp);
    REGISTER(EvMouseAddToSelection);
    REGISTER(EvMouseSelectItem);
    REGISTER(EvSelectSentence);
    REGISTER(EvSwitchTextWorkspace).prmKey.defval(Qt::Key_Tab);
    REGISTER(EvTextEditorFocused);
    REGISTER(EvWorkspaceFocused);
    REGISTER(EvAccept).prmKey.defval(Qt::Key_Return);
    REGISTER(EvCancel).prmKey.defval(Qt::Key_Escape);

    REGISTER(EvWindowClose);
    REGISTER(EvWindowResize);
    REGISTER(EvWindowMove);
}

InputCommands::~InputCommands()
{
}

void InputCommands::onDeinit()
{
    toMainWindow->getWindow()->removeEventFilter(this);
    toMainWindow->getWindow()->ui->textEditMain->removeEventFilter(this);
    toScene->getScene()->removeEventFilter(this);
    toScene->getView()->removeEventFilter(this);

    QObject::disconnect(toMainWindow->getWindow()->ui->btnChoose, SIGNAL(clicked(bool)),
        this, SLOT(selectButtonClicked(bool)));
    toMainWindow->getWindow()->SigCloseEvent.remove(this);
    toMainWindow->getWindow()->SigResize.remove(this);
    toMainWindow->getWindow()->SigMove.remove(this);
}

the::InitState InputCommands::onInit()
{
    toMainWindow->getWindow()->installEventFilter(this);
    toMainWindow->getWindow()->ui->textEditMain->installEventFilter(this);
    toScene->getView()->installEventFilter(this);
    toScene->getScene()->installEventFilter(this);
    QObject::connect(toMainWindow->getWindow()->ui->btnChoose, SIGNAL(clicked(bool)),
        this, SLOT(selectButtonClicked(bool)));
    toMainWindow->getWindow()->SigCloseEvent.add(this, std::bind(&CEvent::raise, &EvWindowClose));
    toMainWindow->getWindow()->SigResize.add(this, std::bind(&CEvent::raise, &EvWindowResize));
    toMainWindow->getWindow()->SigMove.add(this, std::bind(&CEvent::raise, &EvWindowMove));

    return true;
}

    bool InputCommands::eventFilter(QObject * watched, QEvent * event)
{
    // That fucked Qt is stupid
    // Some events do not pass into main window, but go direct to scene
    // Thats why there processes both, window and scene events

    if (watched == toScene->getView())
    {
        if (event->type() == QEvent::KeyRelease || event->type() == QEvent::KeyPress)
        {
            QKeyEvent* ev = static_cast<QKeyEvent*>(event);
            QKeySequence seq = getKeySequence(ev);

            EvSceneKeyEvent(ev, seq);

            if (event->type() == QEvent::KeyPress)
            {
                EvAnyKey();

                if (EvSwitchTextWorkspace.prmKey().matches(seq))
                {
                    EvSwitchTextWorkspace();
                    return true;
                }
            }

            for (auto inputEvent : this->get())
            {
                if (inputEvent->prmKey().matches(seq) && inputEvent->prmType == event->type())
                {
                    inputEvent->raise();
                }
            }
            
            //QApplication::sendEvent(toScene->getScene(), event);
        }
        else if (event->type() == QEvent::FocusIn)
        {
            EvWorkspaceFocused();
        }
    }
    else if (watched == toScene->getScene())
    {
        if (event->type() == QEvent::GraphicsSceneMousePress)
        {
            QGraphicsSceneMouseEvent* mev = static_cast<QGraphicsSceneMouseEvent*>(event);
            if (mev->button() == Qt::MouseButton::LeftButton)
            {
                if (toMouseProcessor->CHoverNode != nullptr || toMouseProcessor->CHoverEdge != nullptr )
                {
                    if (QApplication::keyboardModifiers().testFlag(Qt::ControlModifier) == true)
                    {
                        EvMouseAddToSelection();
                    }
                    else
                    {
                        EvMouseSelectItem();
                    }
                }
                if (toMouseProcessor->CHoverNode != nullptr)
                {
                    EvMainMouseDownOnNode();
                }
                else
                {
                    EvMainMouseDownOnEmpty();
                }
            }
        }
        else if (event->type() == QEvent::GraphicsSceneMouseRelease)
        {
            QGraphicsSceneMouseEvent* mev = static_cast<QGraphicsSceneMouseEvent*>(event);
            if (mev->button() == Qt::MouseButton::LeftButton)
            {
                EvMainMouseUp();
            }
        }
    }
    else if (watched == toMainWindow->getWindow()->ui->textEditMain)
    {
        if (event->type() == QEvent::KeyPress)
        {
            QKeyEvent* ev = static_cast<QKeyEvent*>(event);
            QKeySequence seq = getKeySequence(ev);
            if (EvSwitchTextWorkspace.prmKey().matches(seq))
            {
                EvSwitchTextWorkspace();
                return true;
            }
            else if (EvAccept.prmKey().matches(seq))
            {
                EvAccept();
                return true;
            }
            else if (EvCancel.prmKey().matches(seq))
            {
                EvCancel();
                return true;
            }
        }
        else if (event->type() == QEvent::FocusIn)
        {
            EvTextEditorFocused();
        }
    }
    return false;
}


void InputCommands::selectButtonClicked(bool checked)
{
    EvSelectSentence();
}

QKeySequence InputCommands::getKeySequence(QKeyEvent* ev)
{
    int keyInt = ev->key();
    Qt::Key key = static_cast<Qt::Key>(keyInt);
    if (key != Qt::Key_Control &&
        key != Qt::Key_Shift &&
        key != Qt::Key_Alt &&
        key != Qt::Key_Meta)
    {
        Qt::KeyboardModifiers modifiers = ev->modifiers();
        if(modifiers & Qt::ShiftModifier)
            keyInt += Qt::SHIFT;
        if(modifiers & Qt::ControlModifier)
            keyInt += Qt::CTRL;
        if(modifiers & Qt::AltModifier)
            keyInt += Qt::ALT;
        if(modifiers & Qt::MetaModifier)
            keyInt += Qt::META;
    }

    return QKeySequence(keyInt);
}