////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/Config.hpp>
#include "Logic/UserInput/CEventGroup.hpp"

using namespace mp;
using namespace the;

CEventGroup::CEventGroup(): mLastRaisedChild(nullptr), mInternalRaise(false)
{
    // Children registration
    REGISTER(EvSomeEvent).SigRaised.add([&]{
        if (!mInternalRaise && mLastRaisedChild != nullptr)
        {
            (*mLastRaisedChild)();
        }
    });
    setChildTagName("Event");
}

CEventGroup::~CEventGroup()
{
}

void CEventGroup::onDeinit()
{
    for (auto ev : get())
    {
        (*ev)->SigRaised.remove(this);
    }
}

the::InitState CEventGroup::onInit()
{
    for (auto ev : get())
    {
        (*ev)->SigRaised.add(this, [&, ev]{
            mInternalRaise = true;
            EvSomeEvent();
            mLastRaisedChild = ev->Get();
            mInternalRaise = false;
        });
    }

    return READY;
}