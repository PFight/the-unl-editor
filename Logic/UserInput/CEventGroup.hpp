////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

// Headers
#include <The2D/ChildList.hpp>
#include <The2D/Connectors.hpp>

using namespace the;

////////////////////////////////////////////////////////////
namespace mp {

    class CEventGroup : public ChildList<Bind<CEvent>>
    {
        public:
            THE_ENTITY(CEventGroup, ChildList<Bind<CEvent>>)

            // Binds
            // Parameters
            // Connectors
            CEvent EvSomeEvent;

            CEventGroup();
            virtual ~CEventGroup();

        private:
            CEvent* mLastRaisedChild;
            bool mInternalRaise;

            InitState onInit() override;
            void onDeinit() override;
    };
}; // namespace mp
////////////////////////////////////////////////////////////