////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Component.hpp>
#include <The2D/Connectors.hpp>
#include "Logic/UserInput/InputEvent.hpp"
#include "Logic/UserInput/CEventGroup.hpp"

#include <QKeySequence>
#include <QKeyEvent>

using namespace the;

////////////////////////////////////////////////////////////
namespace mp {

    class KeyDragEvents : public Component
    {
        public:
            THE_ENTITY(KeyDragEvents, Component)

            CInputEvent EvMoveNodesUp;
            CInputEvent EvMoveNodesDown;
            CInputEvent EvMoveNodesLeft;
            CInputEvent EvMoveNodesRight;
            CEventGroup EvMoveEvent;

            CFuncBase<void(QKeyEvent * event, QKeySequence seq)> DoProcessSceneKeyEvent;

            KeyDragEvents();
            virtual ~KeyDragEvents();

            
        private:
            eastl::vector<CInputEvent*> mMyEvents;

            void processSceneKeyEvent(QKeyEvent * event, QKeySequence seq);
            
    };
}; // namespace mp
////////////////////////////////////////////////////////////