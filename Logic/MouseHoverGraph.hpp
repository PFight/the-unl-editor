////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Component.hpp>
#include <The2D/Connectors.hpp>
#include "Logic/GraphManager.hpp"

using namespace the;

class QEvent;

////////////////////////////////////////////////////////////
namespace mp {

    class Node;
    class Edge;
    class Graph;

    class MouseHoverGraph : public the::Component
    {
        public:
            THE_ENTITY(MouseHoverGraph, the::Component)

            ToValueR<Graph*> toGraph;

            CValue<Node*> CHoverNode;
            CValue<Edge*> CHoverEdge;
            CEvent EvHoverNodeChanged;
            CEvent EvHoverEdgeChanged;

            MouseHoverGraph();
            virtual ~MouseHoverGraph();

        private:
            Graph* mLastGraph;

            InitState onInit() override;

            void graphChanged();

            void nodeAdded(Node* node);
            void edgeAdded(Edge* edge);
            void nodeRemoved(Node* node);
            void edgeRemoved(Edge* edge);

            void nodeEvent(QEvent* ev, Node* node);
            void edgeEvent(QEvent* ev, Edge* edge);          

    };
}; // namespace mp
////////////////////////////////////////////////////////////
