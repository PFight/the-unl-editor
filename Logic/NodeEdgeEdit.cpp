#include "Logic/NodeEdgeEdit.hpp"
#include "Logic/MainWindowManager.hpp"
#include "KnotDialog.hpp"
#include "Logic/Graph/Node.hpp"
#include "Logic/Graph/Edge.hpp"
#include <QtWidgets/QTextEdit>

using namespace the;
using namespace mp;



EditGraphicsItem::EditGraphicsItem(NodeEdgeEdit* parent): mParentComponent(parent)
{
    setData(0, EditGraphicsItemID);
}

NodeEdgeEdit* EditGraphicsItem::getParentComponent()
{
    return mParentComponent;
}

bool EditGraphicsItem::sceneEvent ( QEvent * event )
{
    // mParentComponent->SigSceneEvent(event);
    return QGraphicsProxyWidget::sceneEvent(event);
}

NodeEdgeEdit::NodeEdgeEdit(): mEdit(nullptr), mEditProxy(nullptr), mEditingValue(nullptr)
{
    REGISTER(toSelectedItems);
    REGISTER(toMainWindow);
    REGISTER(prmEditSize).defval(400, 30);
    REGISTER(DoAccept).setFunc(std::bind(&NodeEdgeEdit::accept, this));
    REGISTER(DoCancel).setFunc(std::bind(&NodeEdgeEdit::cancel, this));
    REGISTER(DoEditSelected).setFunc(std::bind(&NodeEdgeEdit::editSelection, this));
}

the::InitState NodeEdgeEdit::onInit()
{
    mEditProxy = new EditGraphicsItem(this);
    mEdit = new QTextEdit(nullptr);
    mEditProxy->setWidget(mEdit);
    mEdit->setSizeIncrement(1, 1);
    mEdit->setFixedSize(prmEditSize.width, prmEditSize.height);
    mEdit->setVisible(false);


    return true;
}

void NodeEdgeEdit::onDeinit()
{
    delete mEditProxy;
    mEditProxy = nullptr;
    mEdit = nullptr;
}


void NodeEdgeEdit::editSelection()
{
    if (toSelectedItems.size() > 0)
    {
        CString* text = nullptr;
        QGraphicsItem* parent = nullptr;
        if (Node* node = toSelectedItems.back()->toNode())
        {
            text = &(node->mData.CText);
            parent = node->mView.getSceneItem();
        }
        else if (Edge* edge = toSelectedItems.back()->toEdge())
        {
            text = &edge->mData.CText;
            parent = edge->mView.getSceneItem();
        }
        mEditingValue = text;
        mEditProxy->setParentItem(parent);
        mEdit->setVisible(true);
        mEdit->setText(text->get().c_str());
        mEdit->setFocus();
    }
}

void NodeEdgeEdit::accept()
{
    mEditingValue->set(mEdit->toHtml().toStdString());
    mEdit->setVisible(false);
    mEdit->setText("");
    mEditProxy->setParentItem(nullptr);
}

void NodeEdgeEdit::cancel()
{
    mEdit->setVisible(false);
    mEdit->setText("");
    mEditProxy->setParentItem(nullptr);
}


