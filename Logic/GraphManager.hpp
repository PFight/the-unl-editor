////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Manager.hpp>
#include <The2D/Connectors.hpp>
#include "Logic/Graph/Graph.hpp"
#include <QString>

////////////////////////////////////////////////////////////
namespace mp {

    class GraphManager : public the::Manager
    {
        public:
            THE_ENTITY(GraphManager, the::Manager)

            CValue<Graph*> CActiveGraph;
            CBool CCurrentMode;

            CEvent EvPreActiveGraphChanged;
            CEvent EvActiveGraphChanged;
            CCommand DoCreateBlank;
            CCommand DoSwitchMode;

            CValue<QString> CActiveSentence;

            //GraphHelper mGraphHelper;

            GraphManager();
            virtual ~GraphManager();

            Graph* getActiveGraph();

            void setAvtiveGraph(Graph* g);

        private:
            Graph* mActiveGraph;

            InitState onInit() override;
            void onDeinit() override;

            void createBlank();
    };
}; // namespace mp
////////////////////////////////////////////////////////////
