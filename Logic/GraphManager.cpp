////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/Config.hpp>
#include "Logic/GraphManager.hpp"

using namespace mp;



GraphManager::GraphManager(): mActiveGraph(nullptr)
{
    REGISTER(CActiveGraph)
        .setGetter([&](Graph*& result){ result = getActiveGraph(); })
        .setSetter([&](Graph* graph){ setAvtiveGraph(graph); });
    REGISTER(CCurrentMode).defval(true);
    REGISTER(DoSwitchMode).setFunc([&]{ CCurrentMode = !CCurrentMode(); });
    REGISTER(EvPreActiveGraphChanged);
    REGISTER(EvActiveGraphChanged);
    REGISTER(DoCreateBlank).setFunc(std::bind(&GraphManager::createBlank, this));
    //REGISTER(mGraphHelper).setInitOrder(BEFORE_PARENT);
    REGISTER(CActiveSentence);
}

GraphManager::~GraphManager()
{
}

InitState GraphManager::onInit()
{
    if (mActiveGraph == nullptr)
    {
        DoCreateBlank();
    }
    return mActiveGraph != nullptr;
}

void GraphManager::onDeinit()
{
}

void GraphManager::createBlank()
{
    Graph* graph = new Graph();
    graph->toCurrentMode.connect(&CCurrentMode);

    manageStateChild(make_sptr(graph));
    graph->init();

    /*auto node1 = mGraphHelper.createNode();
    node1->mData.CText = "First";
    node1->mLayout.prmPosition.x.getLocked();
    node1->mLayout.prmPosition.x = 0;
    node1->mLayout.prmPosition.y = 0;
    node1->mLayout.mBody.prmType = b2_staticBody;
    graph->addNode(node1);

    auto node2 = mGraphHelper.createNode();
    node2->mData.CText = "Second";
    graph->addNode(node2);

    auto node3 = mGraphHelper.createNode();
    node3->mData.CText = "Third";
    graph->addNode(node3);

    auto edge = mGraphHelper.createEdge(node1.get(), node2.get());
    edge->mData.CText = "*";
    graph->addEdge(edge);

    auto edge2 = mGraphHelper.createEdge(node1.get(), node3.get());
    edge2->mData.CText = "*";
    graph->addEdge(edge2);*/

    setAvtiveGraph(graph);
}

void GraphManager::setAvtiveGraph(Graph* g)
{
    ASSERT(g != nullptr, "Call DoCreateBlank instead");
    if (g != mActiveGraph)
    {
        EvPreActiveGraphChanged.raise();
        mActiveGraph = g;
        QString str = g->getSentence();
        CActiveSentence = str;
        EvActiveGraphChanged.raise();
    }
}

Graph* GraphManager::getActiveGraph()
{
    return mActiveGraph;
}