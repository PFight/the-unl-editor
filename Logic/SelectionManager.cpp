////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include "Logic/SelectionManager.hpp"

#include "Logic/Graph/Node.hpp"
#include "Logic/Graph/Edge.hpp"
#include "Logic/Graph/NodeView.hpp"
#include "Logic/Graph/EdgeView.hpp"
#include "Logic/Graph/Graph.hpp"

// -----------------------------------------------------------------------------------

using namespace the;
using namespace mp;



SelectionManager::SelectionManager(): mLastGraph(nullptr)
{
    REGISTER(toGraph).onValueChange([&]{ graphChanged(); });
    REGISTER(EvSelectionChanged);
    REGISTER(CSelectedItems).setAccessors(&mSelection).setValueChangedEvent(EvSelectionChanged)
        .onValueChange([&]{ removeDublicates(); });
    mSelection.reserve(10);
}

void SelectionManager::select(GraphItem* item)
{
    clearSelection();
    addToSelection(item);
}

void SelectionManager::addToSelection(GraphItem* item)
{
    if (item != nullptr && !mSelection.contains(item))
    {
        mSelection.push_back(item);

        CSelectedItems.onValueChanged();
    }
}

void SelectionManager::removeFromSelection(GraphItem* item)
{
    if (item != nullptr)
    {
        mSelection.remove(item);

        CSelectedItems.onValueChanged();
    }
}

void SelectionManager::clearSelection()
{
    mSelection.clear();

    CSelectedItems.onValueChanged();
}
const eastl::vector<GraphItem*>& SelectionManager::getSelectedItems()
{
    return mSelection;
}

Node* SelectionManager::getLastSelectedNode()
{
    return mLastSelectedNode;
}
Edge* SelectionManager::getLastSelectedEdge()
{
    return mLastSelectedEdge;
}

the::InitState mp::SelectionManager::onInit()
{
    graphChanged();

    return true;
}

void mp::SelectionManager::onDeinit()
{
    toGraph()->SigNodeRemoved.remove(this);
    toGraph()->SigEdgeRemoved.remove(this);

    mSelection.clear();
}

void mp::SelectionManager::removeDublicates()
{
    eastl::vector<GraphItem*> items;
    for (auto item : mSelection)
    {
        if (!items.contains(item))
        {
            items.push_back(item);
        }
    }
    mSelection.swap(items);
}

void mp::SelectionManager::graphChanged()
{
    if (mLastGraph != nullptr)
    {
        mLastGraph->SigNodeRemoved.remove(this);
        mLastGraph->SigEdgeRemoved.remove(this);
    }

    toGraph()->SigNodeRemoved.add(this, [&](Node* node){ removeFromSelection(node); });
    toGraph()->SigEdgeRemoved.add(this, [&](Edge* edge){ removeFromSelection(edge); });

    mLastGraph = toGraph();
}