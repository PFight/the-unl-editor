////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Bind.hpp>
#include <The2D/StateMachine/State.hpp>
#include "ETrans.hpp"
#include <The2D/ChildList.hpp>

////////////////////////////////////////////////////////////

using namespace the;

namespace mp {

    class StateManager;

    class EditorState : public the::State
    {
        public:
            THE_ENTITY(EditorState, the::State)

            ManagerBind<StateManager> toStateManager;

            ChildList<Bind<EditorState>> toCoactiveStates;
            ChildList<ETrans> mTransitions;


            EditorState();
            virtual ~EditorState();

        protected:
            virtual void onActivate();
            /// \brief Methods to override in derived classes.
            virtual void onDeactivate();

    };
}; // namespace mp
////////////////////////////////////////////////////////////
