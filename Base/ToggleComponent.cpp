////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/Config.hpp>
#include "Base/ToggleComponent.hpp"

using namespace mp;



ToggleComponent::ToggleComponent()
{
    REGISTER(Enabled);
        Enabled.EvActivated.SigRaised.add(this, std::bind(&ToggleComponent::onEnable, this, true));
        Enabled.EvDeactivated.SigRaised.add(this, std::bind(&ToggleComponent::onEnable, this, false));
}

ToggleComponent::~ToggleComponent()
{
    Enabled.EvActivated.SigRaised.remove(this);
    Enabled.EvDeactivated.SigRaised.remove(this);
}



void ToggleComponent::onEnable(bool enable)
{
}