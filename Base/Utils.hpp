////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Config.hpp>
#include <The2D/GameEntity.hpp>

using namespace the;

////////////////////////////////////////////////////////////
namespace mp
{
   
    #define MY_FUNC(method) std::bind(&EntityType::Type::method, this)

    inline string getNicePath(GameEntity* ent)
    {
        string path = ent->getName().empty()? type(ent).name : ent->getName();
        path += "(";
        for (GameEntity* current = ent->getParent();
            current != nullptr && !current->getName().empty();
            current = current->getParent())
        {
            path += current->getName() + "\\";
        }
        path+= ")";
        return path;
    }

} // namespace mp
////////////////////////////////////////////////////////////
