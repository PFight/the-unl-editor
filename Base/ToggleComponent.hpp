////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Component.hpp>
#include "EditorState.hpp"

////////////////////////////////////////////////////////////
namespace mp {

    class ToggleComponent : public the::Component
    {
        public:
            THE_ENTITY(ToggleComponent, the::Component)

            EditorState Enabled;

            ToggleComponent();
            virtual ~ToggleComponent();

            bool enabled()
            {
                return Enabled.CActive;
            }

        protected:

            virtual void onEnable(bool enable);

    };
}; // namespace mp
////////////////////////////////////////////////////////////
