////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/Config.hpp>
#include "EditorState.hpp"
#include "StateManager.hpp"
#include "Base/Utils.hpp"

using namespace mp;



EditorState::EditorState()
{
    REGISTER(toStateManager);
    REGISTER(toCoactiveStates);
    REGISTER(mTransitions).setConstructor([&](){
        ETrans* t = new ETrans();
        Bind<State>* bind = new Bind<State>();
        bind->set(this);
        t->toStates.add(make_sptr(bind));
        return t;
    });
}

EditorState::~EditorState()
{
}

void EditorState::onActivate()
{

    toStateManager->onStateActivated(this);
}
void EditorState::onDeactivate()
{
    toStateManager->onStateDeactivated(this);
}

