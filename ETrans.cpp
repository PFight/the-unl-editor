////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/Config.hpp>
#include "ETrans.hpp"

#include "StateManager.hpp"

using namespace mp;



ETrans::ETrans()
{
    REGISTER(toStateManager);
    REGISTER(prmRepeatEvent).defval(false);
}

ETrans::~ETrans()
{
}

void ETrans::perform()
{
    // Log stuff
    #ifdef DEBUG
        string nextStates;
        for (auto nextStateBind : toNextStates())
        {
            nextStates += nextStateBind->path()+ ", ";;
        }
        string events;
        for (auto eventBind : toEvents())
        {
            events += eventBind->path()+ ", ";;
        }
        string states;
        for (auto stateBind : toStates())
        {
            states += stateBind->Get()->getName() + ", ";
        }
        string actions;
        for (auto act : mAction())
        {
            actions += type(act).name + ", ";;
        }
        INFO("Trans State: %s Events: %s, Actions: %s, NextStates: %s", states, events, actions, nextStates);
    #endif

    mAction.exec();

    if (toNextStates.size() > 0)
    {
        /*if ((int)toNextStates.size() == 1 && toNextStates().front()->Get() == &toStateManager->mPreviousState)
        {
            for (auto state : toStates())
            {
                toStateManager->queueToPopStatesStack(dynamic_cast<EditorState*>(state->Get()));
            }
        }
        else*/
        {
            for (auto nextState : toNextStates())
            {
                toStateManager->queueToActivate(dynamic_cast<EditorState*>(nextState->Get()));
            }
        }
    }

    if (prmRepeatEvent)
    {
        toStateManager->queueToRepeat(toEvents().front()->Get());
    }
}

