////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Bind.hpp>
#include <The2D/StateMachine/Trans.hpp>

using namespace the;

////////////////////////////////////////////////////////////
namespace mp {

    class StateManager;

    class ETrans : public the::Trans
    {
        public:
            THE_ENTITY(ETrans, the::Trans)

            /// \brief If true, event that activated the transition will be raised again, after transition perform.
            PBool prmRepeatEvent;

            ManagerBind<StateManager> toStateManager;

            ETrans();
            virtual ~ETrans();

            virtual void perform() override;

    };
}; // namespace mp
////////////////////////////////////////////////////////////
