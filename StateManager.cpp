////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/Config.hpp>
#include "StateManager.hpp"
#include "Base/Utils.hpp"

using namespace mp;



StateManager::StateManager()
{
    REGISTER(prmMaxStackSize).defval(100);
    REGISTER(toStartState).setDependency(NOT_A_DEPENDENCY);
    REGISTER(mPrevState);
}

StateManager::~StateManager()
{
}

const eastl::vector<EditorState*>& StateManager::getActiveStates()
{
    return mActiveStates;
}
void StateManager::onStateActivated(EditorState* state)
{
    if (!mActiveStates.contains(state))
    {
        mActiveStates.push_back(state);
    }
}
void StateManager::onStateDeactivated(EditorState* state)
{
    mActiveStates.remove(state);
}
void StateManager::deactivateAll()
{
    string deactivated;
    while(mActiveStates.size() > 0)
    {
        deactivated += getNicePath(mActiveStates.back()) + ", ";
        mActiveStates.back()->CActive = false;
        // OnStateDeactiated continue the job
    }
    INFO("Deactivated: %s", deactivated);
}

void StateManager::onDeinit()
{
}

void StateManager::onPostInit()
{
    queueToActivate(toStartState());
}

void StateManager::update()
{
    bool smthDone = false;
    string msg;
    const char* const ident = "...";
    if (!mStatesToPop.empty())
    {
        string deactivated;
        for (auto state : mStatesToPop)
        {
            setActive(state, false, &deactivated, ident, ident);
        }
        msg += format("**Deactivated via popping:**\n%s", deactivated);
        mStatesToPop.clear();
        smthDone = true;
    }
    if (!mStatesToActivate.empty())
    {
        // Deactivate all states, that are not parent of activating states
        string deactivated;
        eastl::vector<EditorState*> toDeactivate;
        eastl::vector<EditorState*> coactiveOfParents;
        string parents = "";
        for (auto activeState : mActiveStates)
        {
            bool isParent = false;
            for (auto state : mStatesToActivate)
            {
                if (activeState->forefatherOf(state))
                {
                    isParent = true;
                    setActive(activeState, true, &parents, ident, ident, &coactiveOfParents);
                    break;
                }
            }
            if (!isParent)
            {
                toDeactivate.push_back(activeState);;
            }
        }
        for (auto state : toDeactivate)
        {
            if (state->CActive) // can be deactivated as coactive
            {
                // Coactive of parents also should not be deactivated
                if (!coactiveOfParents.contains(state))
                {
                    setActive(state, false, &deactivated, ident, ident);
                }
            }
        }
        if (!deactivated.empty())
        {
            msg += format("**Deactivated:**\n%s", deactivated);
        }

        // Activate states, queued to activate
        string activated;
        for (auto state : mStatesToActivate)
        {
            setActive(state, true, &activated, ident, ident);
        }
        msg += format("**Activated:**\n%s", activated);

        if (!parents.empty())
        {
            msg += format("**Leave active as parents of activated:**\n%s", parents);
        }


         // Activate parents with children
        eastl::vector<EditorState*> toActivateAsParents;
        for (auto state : mStatesToActivate)
        {
            for (GameEntity* parent = state->getParent(); parent != nullptr; parent = parent->getParent())
            {
                if (core()->rtti()->is<EditorState>(parent))
                {
                    if (!static_cast<EditorState*>(parent)->CActive)
                    {
                        toActivateAsParents.push_back(static_cast<EditorState*>(parent));
                    }
                }
            }
        }
        string activatedAsParents;
        for (auto state : toActivateAsParents)
        {
            setActive(state, true, &activatedAsParents, ident, ident);
        }

        if (!activatedAsParents.empty())
        {
            msg += format("**Activated as parents:**\n%s", activatedAsParents);
        }

        mStatesToActivate.clear();
        smthDone = true;
    }

    if (!mEventsToRepeat.empty())
    {
        string raised;
        for (auto ev : mEventsToRepeat)
        {
            ev->raise();
            raised += ident + getNicePath(ev);
        }
        msg += format("**Repeated events:**\n%s", raised);
        mEventsToRepeat.clear();
        smthDone = true;
    }

    if (smthDone)
    {
        string active;
        for (auto state : mActiveStates)
        {
            active += ident + getNicePath(state) + "\n";
        }
        msg += format("\n\n\n**Active states:**\n%s", active);
        INFO("State changed|%s", msg);
    }
}

void StateManager::setActive(EditorState* state, bool activate, string* msg,
    string ident, const char* oneIdent, eastl::vector<EditorState*>* coactiveStates)
{
    string act = string("already ") + (activate? "active" : "not active");
    if (state->CActive() != activate)
    {
        state->CActive = activate;
        act = activate? "activated" : "deactivated";
    }
    *msg += ident + getNicePath(state) + " - " + act + "\n";

    setActivateForCoactiveStates(state, activate, msg, ident+oneIdent, oneIdent, coactiveStates);
}

void StateManager::setActivateForCoactiveStates(EditorState* state, bool activate, string* msg,
    string ident, const char* oneIdent, eastl::vector<EditorState*>* coactiveStates)
{
    for (auto bindPtr : state->toCoactiveStates())
    {
        EditorState* coactive = (*bindPtr).Get();
        string act = string("already ") + (activate? "active" : "not active");
        if (coactive->CActive() != activate)
        {
            coactive->CActive = activate;
            act = activate? "activated" : "deactivated";
        }
        if (coactiveStates != nullptr)
        {
            coactiveStates->push_back(coactive);
        }
        *msg += ident + "coactive: " + getNicePath(coactive) + " - " + act + "\n";
        setActivateForCoactiveStates(coactive, activate, msg, ident + oneIdent, oneIdent, coactiveStates);
    }
}

void StateManager::queueToActivate(EditorState* state)
{
    if (state == &mPrevState)
    {
        queueToPopStatesStack();
    }
    else
    {
        mStatesToActivate.push_back(state);
        mStatesStack.push_back(state);
        if ((int)mStatesStack.size() > prmMaxStackSize)
        {
            mStatesStack.pop_front();
        }
    }
}

void StateManager::queueToRepeat(CEvent* ev)
{
    mEventsToRepeat.push_back(ev);
}

void StateManager::queueToPopStatesStack()
{
    ASSERT(mStatesStack.size() > 0, "");

    mStatesToPop.push_back(mStatesStack.back());
    mStatesStack.pop_back();
    mStatesToActivate.push_back(mStatesStack.back());
}