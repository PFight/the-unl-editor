#ifndef DIALOGSAVEREQUEST_HPP
#define DIALOGSAVEREQUEST_HPP

#include <QDialog>

namespace Ui {
    class DialogSaveRequest;
}

class DialogSaveRequest : public QDialog
{
        Q_OBJECT
        
    public:
        explicit DialogSaveRequest(QWidget *parent = 0);
        ~DialogSaveRequest();
        
    private:
        Ui::DialogSaveRequest *ui;
};

#endif // DIALOGSAVEREQUEST_HPP
