#include "UnlGraphicsView.hpp"

#include <QDebug>

UnlGraphicsView::UnlGraphicsView(QWidget *parent) :
    QGraphicsView(parent)
{
}



UnlGraphicsView::~UnlGraphicsView()
{
}

void UnlGraphicsView::wheelEvent(QWheelEvent * ev)
{
    // do nothing (prevent scrolling)

    // QGraphicsView::wheelEvent(ev);
}

bool UnlGraphicsView::event(QEvent* ev)
{
    //qDebug() << "!!!!!!!!!!!!!!!!" << ev->type();

    return QGraphicsView::event(ev);
}