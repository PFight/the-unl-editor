#include "mainwindow.h"
#include <memory>

Q_DECLARE_METATYPE(TiXmlElement*);
Q_DECLARE_METATYPE(QTreeWidgetItem*);

TiXmlElement* MainWindow::FindComponentTagRec(TiXmlElement* current, const th::string& name)
{
    if (current->ValueStr() == "Param")
    {
        // Get param value
        th::string value;
        if (!current->QueryStringAttribute("value", &value) == TIXML_SUCCESS)
        {
            if (current->GetText() != nullptr)
                value = current->GetText();
            else
                value = "";
        }

        if (value == name)
            return current->Parent()->ToElement();
    }

    for (TiXmlElement* child = current->FirstChildElement();
         child != nullptr;
         child = child->NextSiblingElement())
    {
        TiXmlElement* childResult = FindComponentTagRec(child, name);
        if (childResult != nullptr)
            return childResult;
    }

    return nullptr;
}

// --------------------------------------------------
void MainWindow::floatType(TiXmlElement *i, QTreeWidgetItem *item, QString value)
{
    QDoubleSpinBox *spin = new QDoubleSpinBox(this);
    spin->setSingleStep(0.5);

    if (i->Attribute("min"))
    {
        double minVal = QVariant(i->Attribute("min")).toDouble();
        spin->setMinimum(minVal);
    }
    if (i->Attribute("max"))
    {
        double maxVal = QVariant(i->Attribute("max")).toDouble();
        spin->setMaximum(maxVal);
    }

    spin->setValue(value.toDouble());
    spin->setProperty("Parent", QVariant::fromValue<QTreeWidgetItem*>(item));
    connect(spin, SIGNAL(valueChanged(QString)), this, SLOT(gridItemChanged(QString)));
    this->mPropertyGridTree->setItemWidget(item, 1, spin);
}
// --------------------------------------------------
void MainWindow::intType(TiXmlElement *i, QTreeWidgetItem *item, QString value)
{
    QSpinBox *spin = new QSpinBox(this);

    if (i->Attribute("min"))
    {
        int minVal = QVariant(i->Attribute("min")).toInt();
        spin->setMinimum(minVal);
    }
    if (i->Attribute("max"))
    {
        int maxVal = QVariant(i->Attribute("max")).toInt();
        spin->setMaximum(maxVal);
    }

    spin->setProperty("Parent", QVariant::fromValue<QTreeWidgetItem*>(item));
    spin->setValue(value.toInt());
    connect(spin, SIGNAL(valueChanged(QString)), this, SLOT(gridItemChanged(QString)));
    this->mPropertyGridTree->setItemWidget(item, 1, spin);
}
// --------------------------------------------------
void MainWindow::boolType(TiXmlElement *i, QTreeWidgetItem *item, QString value)
{
    QCheckBox *check = new QCheckBox(this);
    if (value.toLower() == "true") check->setChecked(true);

    check->setProperty("Parent", QVariant::fromValue<QTreeWidgetItem*>(item));
    connect(check, SIGNAL(clicked(bool)), this, SLOT(gridItemChanged(bool)));
    this->mPropertyGridTree->setItemWidget(item, 1, check);
}
// --------------------------------------------------
void MainWindow::stringType(TiXmlElement *i, QTreeWidgetItem *item, QString value)
{
    QLineEdit *edit = new QLineEdit(this);
    if (value == "notset")
        edit->setPlaceholderText("Not set");
    else
        edit->setText(value);

    edit->setProperty("Parent", QVariant::fromValue<QTreeWidgetItem*>(item));
    connect(edit, SIGNAL(textChanged(QString)), this, SLOT(gridItemChanged(QString)));
    this->mPropertyGridTree->setItemWidget(item, 1, edit);
}
// --------------------------------------------------
void MainWindow::vectorType(TiXmlElement *i, QTreeWidgetItem *item, QString value)
{
    QSpinBox *box1 = new QSpinBox(this);
    QSpinBox *box2 = new QSpinBox(this);

    if (i->Attribute("x")) box1->setValue(QString(i->Attribute("x")).toFloat());
    if (i->Attribute("y")) box2->setValue(QString(i->Attribute("y")).toFloat());

    box1->setPrefix("x:"); box2->setPrefix("y:");
    QWidget *layoutParent = new QWidget(this);
    QHBoxLayout *layout = new QHBoxLayout(layoutParent);
    layout->setSpacing(0); layout->setContentsMargins(0, 0, 0, 0);

    layout->addWidget(box1); layout->addWidget(box2);
    layoutParent->setLayout(layout);

    box1->setProperty("Parent", QVariant::fromValue<QTreeWidgetItem*>(item));
    box2->setProperty("Parent", QVariant::fromValue<QTreeWidgetItem*>(item));
    connect(box1, SIGNAL(valueChanged(QString)), this, SLOT(gridItemChanged(QString)));
    connect(box2, SIGNAL(valueChanged(QString)), this, SLOT(gridItemChanged(QString)));
    this->mPropertyGridTree->setItemWidget(item, 1, layoutParent);
}
// --------------------------------------------------
void MainWindow::verticesType(TiXmlElement *i, QTreeWidgetItem *item, QString value)
{
    item->setText(0, QString::fromUtf8("↓Vertices"));
    for (TiXmlElement *v = i->FirstChild("Vertex")->ToElement();
         v != nullptr;
         v = v->NextSiblingElement())
    {
        QTreeWidgetItem *vertices = new QTreeWidgetItem();
        item->addChild(vertices);
        this->vectorType(v, vertices, "");
    }
}
// --------------------------------------------------
void MainWindow::angleType(TiXmlElement *i, QTreeWidgetItem *item, QString value)
{
    QSpinBox *angle = new QSpinBox(this);
    angle->setMinimum(0); angle->setMaximum(360);
    angle->setSuffix(QString::fromUtf8("°"));

    angle->setProperty("Parent", QVariant::fromValue<QTreeWidgetItem*>(item));
    connect(angle, SIGNAL(valueChanged(QString)), this, SLOT(gridItemChanged(QString)));
    this->mPropertyGridTree->setItemWidget(item, 1, angle);
}
// --------------------------------------------------

void MainWindow::gridItemChanged(QString newVal)
{
    // Pointer to item in property grid
    QTreeWidgetItem *p = qvariant_cast<QTreeWidgetItem*>(sender()->property("Parent"));
    TiXmlElement *param = qvariant_cast<TiXmlElement*>(p->data(0, Qt::UserRole));

    qDebug() << "Get value: " << newVal;

    QString type;
    if (param->Attribute("type") != nullptr)
        type = param->Attribute("type");
    else
        qDebug() << "No type attribute!";

    if (type == "float" || type == "int" || type == "angle")
    {
        for (TiXmlNode* textChild=param->FirstChild(); textChild != nullptr;)
        {
            TiXmlNode* nextChild = textChild->NextSibling();
            param->RemoveChild(textChild);
            textChild = nextChild;
        }
        TiXmlText* text = new TiXmlText(newVal.toAscii());
        param->LinkEndChild(text);
    }

    if (mLastChosenEntity != nullptr)
    {
        TiXmlElement* toParse = th::TemplatesProcessor::ProcessParams(param->Parent()->ToElement());
        mLastChosenEntity->Deinit();
        mLastChosenEntity->GetParser()->Parse(toParse);
        mLastChosenEntity->Init();
    }
    else
    {
        qDebug() << "Failed to set new param value - entity pointer is empty!";
    }
}
void MainWindow::gridItemChanged(bool newVal)
{
    gridItemChanged(newVal ? "true" : "false");
}

// Lib - library name (Stc, Dbg)
// Type - component type (PhysicObject)
void MainWindow::processObjectTemplate(th::GameEntity *component)
{
    const char *lib  = component->GetLibName().c_str();
    const char *type = component->GetType().c_str();
    QString path;
//    path = QString("Objects/%1.tpl").arg(lib);
    path = QString("Media/Levels/Test.xml");
    qDebug() << "Load tpl-file for type: " << type;
    qDebug() << "Build path: " << path;

    if (!QFile::exists(path))
    {
        qDebug() << "Such tpl file doesnt exist!";
        return;
    }

    mSceneDoc = std::make_shared<TiXmlDocument>(path.toAscii());
    mSceneDoc->LoadFile();

    TiXmlElement* workingCopyRoot = nullptr;
    try
    {
        workingCopyRoot = th::TemplatesProcessor::ProcessTemplates(mSceneDoc->RootElement(),
                                                                      mSceneDoc, std::string(path.toAscii()));
    }
    catch(const th::InvalidSceneFormatException &e)
    {
        ERR("Failed to parse scene: %s", e.Message.c_str());
        return;
    }

    th::std::shared_ptr<TiXmlDocument> doc(new TiXmlDocument());
    doc->LinkEndChild(workingCopyRoot);
    mWorkingCopy = doc;

//    TiXmlDocument doc;
//    doc.LinkEndChild(mWorkingCopy);
//    doc.SaveFile("workingCopy.xml");


    qDebug() << "We are going to find it" << component->GetName().c_str();
    auto templates = FindComponentTagRec(mWorkingCopy->RootElement(), component->GetName());
    if (!templates) { qDebug() << "NOT FOUND"; return; }

    this->mPropertyGridTree->clear();
    // Iterate component for params
    for (TiXmlElement *i = templates->FirstChild("Param")->ToElement();
         i != nullptr;
         i = i->NextSiblingElement())
    {
        // `i` is current Param tag
        QString name, type, value;

        // NAME
        if (i->Attribute("displayName") != nullptr)
            name = i->Attribute("displayName");
        else
            name = i->Attribute("id");

        if (name.isEmpty()) continue;

        // TYPE
        if (i->Attribute("type") != nullptr)
            type = i->Attribute("type");
        else
            qDebug() << "No type attribute!";

        if (i->Attribute("value") != nullptr)
            value = i->Attribute("value");
        else
            value = i->GetText();

        if (name == "@Fixtures" || name == "@ParentBody") continue;

        QTreeWidgetItem *item = new QTreeWidgetItem();
        item->setText(0, name.remove(0, 1)); // Remove `@` from name
        // Save pointer to param tag in workingCopy xml tree
        item->setData(0, Qt::UserRole, QVariant::fromValue<TiXmlElement*>(i));
        this->mPropertyGridTree->addTopLevelItem(item);

        // If there is fixed variants (useVariant=false), we ignore param type
        // And use ComboBox with passed variants values
        if (i->Attribute("variants"))
        {
            QStringList vars = QString(i->Attribute("variants")).split("|", QString::SkipEmptyParts);
            // If useVariant == false (default), show combo box
            if (i->Attribute("useVariant"))
            {
                if (std::strcmp(i->Attribute("useVariant"), "true") == 0)
                {
                    // useVariant = true
                }
            }
            // useVariant = false
            else
            {
                // Use fixed combo box
                QComboBox *box = new QComboBox(this);
                box->setSizeAdjustPolicy(QComboBox::AdjustToContents);
                for (int i=0; i < vars.size(); ++i)
                    box->insertItem(0, vars.at(i));

                this->mPropertyGridTree->setItemWidget(item, 1, box);
                continue;
            }
        }

        type = type.toLower();
            if (type == "float" || type == "double" || type == "radius") {
            this->floatType(i, item, value);
        }
        else if (type == "int") {
            this->intType(i, item, value);
        }
        else if (type == "bool") {
            this->boolType(i, item, value);
        }
        else if (type == "string" || type == "name") {
            this->stringType(i, item, value);
        }
        else if (type == "position" || type == "vector") {
            this->vectorType(i, item, value);
        }
        else if (type == "angle") {
            this->angleType(i, item, value);
        }
        else if (type == "vertices") {
            this->verticesType(i, item, value);
        }

        this->mPropertyGridTree->resizeColumnToContents(0);
        this->mPropertyGridTree->updateGeometry();
        this->mProperyGridDock->show();
    }
}
