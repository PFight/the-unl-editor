#include "KnotDialog.hpp"
#include "ui_KnotDialog.h"

using namespace mp;

KnotDialog::KnotDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::KnotDialog)
{
    ui->setupUi(this);
    QObject::connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(btnAccepted()));
    QObject::connect(ui->buttonBox, SIGNAL(rejected()), this, SLOT(btnRejected()));
}

KnotDialog::~KnotDialog()
{
    delete ui;
}

void KnotDialog::setKnotText(the::string txt)
{
    ui->lineEdit->setText(txt.c_str());

}

void KnotDialog::closeEvent(QCloseEvent* ev)
{
}

void KnotDialog::btnAccepted()
{
    setResult(QDialog::Accepted);
    close();
}

void KnotDialog::btnRejected()
{
    setResult(QDialog::Rejected);
    close();
}

the::string KnotDialog::getKnotText()
{
    return the::string(ui->lineEdit->text().toStdString().c_str());
}

void KnotDialog::showEvent(QShowEvent *)
{
     ui->lineEdit->selectAll();
     ui->lineEdit->setFocus();
     this->activateWindow();
}
