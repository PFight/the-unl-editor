#include "DialogAddEdge.h"
#include "ui_DialogAddEdge.h"

DialogAddEdge::DialogAddEdge(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogAddEdge)
{
    ui->setupUi(this);
    QObject::connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(btnAccepted()));
    QObject::connect(ui->buttonBox, SIGNAL(rejected()), this, SLOT(btnRejected()));

}

void DialogAddEdge::btnAccepted()
{
    setResult(QDialog::Accepted);
    SigWindowCloseRequest();
}

void DialogAddEdge::btnRejected()
{
    setResult(QDialog::Rejected);
    SigWindowCloseRequest();
}

DialogAddEdge::~DialogAddEdge()
{
    delete ui;
}

void DialogAddEdge::setText(the::string txt)
{
    ui->lineEdit->setText(txt.c_str());

}


the::string DialogAddEdge::getText()
{
    return the::string(ui->lineEdit->text().toStdString().c_str());
}

void DialogAddEdge::showEvent(QShowEvent *)
{
     ui->lineEdit->selectAll();
     ui->lineEdit->setFocus();
     SigShowed();;
     //this->activateWindow();
}
