#ifndef DIALOGADDEDGE_H
#define DIALOGADDEDGE_H

#include <QDialog>
#include "The2D/Config.hpp"
#include <vector>
#include <The2D/Utils/Signals.hpp>

namespace Ui {
    class DialogAddEdge;
}

class DialogAddEdge : public QDialog
{
        Q_OBJECT

    public:
        the::Signal<void ()> SigShowed;
        the::Signal<void ()> SigWindowCloseRequest;

        explicit DialogAddEdge(QWidget *parent = 0);
        ~DialogAddEdge();

        void setText(the::string txt);
        the::string getText();

        void showEvent(QShowEvent *);

    public slots:

        void btnAccepted();
        void btnRejected();

    private:
        std::vector<the::string> mTypes;
        Ui::DialogAddEdge *ui;
};

#endif // DIALOGADDEDGE_H
