#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

void MainWindow::resizeEvent(QResizeEvent * event)
{
    SigResize(event);
}

void MainWindow::moveEvent(QMoveEvent* ev)
{
    SigMove(ev);
}

MainWindow::~MainWindow()
{
    delete ui;
}
 bool MainWindow::event(QEvent *event)
 {
    //qDebug() << "Event type: %i" << (int)event->type();
    SigEvent(event);
    return QMainWindow::event(event);
 }
void MainWindow::closeEvent(QCloseEvent* ev)
{
    SigCloseEvent(ev);
}
