#-------------------------------------------------
#
# Project created by QtCreator 2012-09-02T13:32:08
#
#-------------------------------------------------

QT += core gui widgets
QT += sql

TARGET = EditorQt
TEMPLATE = app


SOURCES += main.cpp \
    States/mainstate.cpp \
    chooseuserwindow.cpp \
    Managers/ApplicationManager.cpp \
    ChooseMapDialog.cpp \
    States/MapState.cpp \
    QSFMLCanvas.cpp \
    Components/MapKnotComponent.cpp \
    Components/MapEdgeComponent.cpp \
    Components/MapComponent.cpp \
    Components/ArrowSelectionComponent.cpp \
    Managers/SelectionManager.cpp \
    Components/ExitOnWindowCloseComponent.cpp \
    Components/KnotCreationComponent.cpp \
    KnotDialog.cpp \
    Components/KnotEdgeEditComponent.cpp \
    Managers/MainWindowManager.cpp \
    Managers/DataBaseManager.cpp \
    Components/EdgeCreationComponent.cpp \
    Components/MouseSelectionComponent.cpp \
    Components/CameraOnSelectionComponent.cpp \
    Components/MouseButtonsFixComponent.cpp \
    Components/FreeCameraComponent.cpp \
    DialogSaveRequest.cpp \
    DialogAddEdge.cpp \
    mainwindow.cpp

HEADERS  += \
    Data.h \
    States/mainstate.h \
    chooseuserwindow.h \
    Managers/ApplicationManager.hpp \
    Config.hpp \
    ChooseMapDialog.h \
    States/MapState.h \
    QSFMLCanvas.h \
    Components/MapKnotComponent.h \
    Components/MapEdgeComponent.h \
    Components/MapComponent.h \
    Components/ArrowSelectionComponent.h \
    Managers/SelectionManager.hpp \
    Components/ExitOnWindowCloseComponent.h \
    Components/KnotCreationComponent.h \
    KnotDialog.h \
    Components/KnotEdgeEditComponent.h \
    Managers/MainWindowManager.hpp \
    Managers/DataBaseManager.hpp \
    Components/EdgeCreationComponent.h \
    Components/MouseSelectionComponent.h \
    Components/CameraOnSelectionComponent.h \
    Components/MouseButtonsFixComponent.h \
    Components/FreeCameraComponent.h \
    DialogSaveRequest.hpp \
    DialogAddEdge.h \
    mainwindow.h

FORMS    += \
    chooseuser.ui \
    ChooseMapDialog.ui \
    KnotDialog.ui \
    DialogSaveRequest.ui \
    DialogAddEdge.ui \
    mainwindow.ui

INCLUDEPATH += "../../External/SFML/include"
INCLUDEPATH += "../../External/Box2D"
INCLUDEPATH += "../../External/muParser/include"
INCLUDEPATH += "../../The2D/include"
INCLUDEPATH += "../../Components"
INCLUDEPATH += "../../Components/Box2DSupport/include"
INCLUDEPATH += "../../Components/Box2DStc/include"
INCLUDEPATH += "../../The2D/include/The2D/Utils/EASTL"
INCLUDEPATH += "../../The2D/include/Utils/TinyXML"

QMAKE_CXXFLAGS += -std=c++0x

DEPENDPATH += "../../Bin/Lib/Components"
DEPENDPATH += "../../Bin/Lib"
DEPENDPATH += "../../Bin"

RESOURCES += \
    Resources.qrc

OTHER_FILES += \
    tpldb.sqlite \
    TODO.txt

#LIBS += -L"/usr/lib/x86_64-linux-gnu" \
#       -lGL \
#       -lopenal \
#       -lsndfile \
#       -lX11 \
#       -lGLEW \
#       -ljpeg \
LIBS += \
    "/usr/lib/x86_64-linux-gnu/libGLEW.so.1.8" \
    "/usr/lib/x86_64-linux-gnu/libjpeg.so.8" \
    "/usr/lib/nvidia-current/libGL.so.1" \
    "/usr/lib/x86_64-linux-gnu/libX11.so.6" \
    "/usr/lib/x86_64-linux-gnu/libXrandr.so.2"

LIBS += \
    "../../Bin/Lib/libBox2D.a" \
    "../../Bin/Lib/libmuParser.a"
LIBS += \
       "../../Bin/libThe2D.so" \
       ../../Bin/Lib/Components/libBox2DStc.a \
       ../../Bin/Lib/Components/libBox2DSupport.a
#     -L$$PWD/../stc/Bin/Lib \
#       -lsfml-graphics \
#       -lsfml-window \
#       -lsfml-system \
#       -lsfml-audio \
#       -lsfml-network \
#       -lBox2D \
#       -lmuParser \


LFLAGS = -static-libgcc
