#include "States/MainState.hpp"
#include "The2D/GameTree.hpp"
#include "The2D/XmlLoader.hpp"
#include "Box2DSupport/Body.hpp"

#include <QMessageBox>
#include <QCloseEvent>

// #Includes
#include "Logic/CreateGroup.hpp"
#include "Logic/Graph/NodeGroupData.hpp"
#include "Logic/Graph/NodeGroupLayout.hpp"
#include "Logic/Graph/NodeGroupView.hpp"
#include "Logic/Graph/NodeGroup.hpp"
#include "Logic/GraphSelectionVisualisation.hpp"
#include "Logic/CameraManager.hpp"
#include "Logic/UserInput/KeyDragEvents.hpp"
#include "Logic/Graph/Node.hpp"
#include "Logic/Graph/NodeView.hpp"
#include "Logic/Graph/NodeLayout.hpp"
#include <The2D/StateMachine/State.hpp>
#include "Logic/NodeMouseDrag.hpp"
#include "Logic/NodesRepulsion.hpp"
#include "Logic/NodeEdgeDelete.hpp"
#include "Logic/Grid/GridVisualisation.hpp"
#include "Logic/Grid/GridManager.hpp"
#include "Logic/Grid/GridSelectionControll.hpp"
#include "Logic/Grid/GridSelectionVisualisation.hpp"
#include "Logic/Grid/CameraOnGridSelection.hpp"
#include "Logic/Grid/GraphSelectionOverGrid.hpp"
#include "Logic/Grid/GridResize.hpp"
#include "EditorState.hpp"
#include "ETrans.hpp"
#include "StateManager.hpp"
#include "Logic/GraphManager.hpp"
#include "Logic/AdjustEdgesLengthsDuringDrag.hpp"
#include "Logic/SentenceToGraph.hpp"
#include "Logic/SentenceToNodes.hpp"
#include "Logic/SelectSentece.hpp"
#include "Logic/FocusController.hpp"
#include "Logic/NodeKeyboardDrag.hpp"

void TestClass::test(int a)
{
}

namespace mp
{

    MainState::MainState()
    {
        //mMainState = nullptr;
    }

    MainState::~MainState()
    {
        mCore->tree()->deinit();
    }

    void MainState::init(InfrastructureCore *core)
    {
        mCore = core;

        // #TypeRegistering
        mCore->rtti()->registerType<mp::CreateGroup>();
        mCore->rtti()->registerType<mp::NodeGroupData>();
        mCore->rtti()->registerType<mp::NodeGroupLayout>();
        mCore->rtti()->registerType<mp::NodeGroupView>();
        mCore->rtti()->registerType<mp::NodeGroup>();
        mCore->rtti()->registerType<mp::GraphSelectionVisualisation>();
        mCore->rtti()->registerType<mp::KeyDragEvents>();
        mCore->rtti()->registerType<ApplicationManager>();
        mCore->rtti()->registerType<PhysicManager>();
        mCore->rtti()->registerType<CameraManager>();
        mCore->rtti()->registerType<MainWindowManager>();
        mCore->rtti()->registerType<SceneManager>();
        mCore->rtti()->registerType<GraphHelper>();
        mCore->rtti()->registerType<LayoutManager>();
        mCore->rtti()->registerType<Graph>();
        mCore->rtti()->registerType<SelectionManager>();
        //mCore->rtti()->registerType<ArrowSelection>();
        mCore->rtti()->registerType<NodeEdgeEdit>();
        mCore->rtti()->registerType<NodeCreation>();
        mCore->rtti()->registerType<EdgeCreation>();
        mCore->rtti()->registerType<MouseSelection>();
        //mCore->rtti()->registerType<CameraOnSelection>();
        mCore->rtti()->registerType<FreeCamera>();
        mCore->rtti()->registerType<MouseHoverGraph>();
        mCore->rtti()->registerType<InputCommands>();
        mCore->rtti()->registerType<NodeMouseDrag>();
        mCore->rtti()->registerType<NodesRepulsion>();
        mCore->rtti()->registerType<NodeEdgeDelete>();
        mCore->rtti()->registerType<GridManager>();
        mCore->rtti()->registerType<GridVisualisation>();
        mCore->rtti()->registerType<GridSelectionControll>();
        mCore->rtti()->registerType<GridSelectionVisualisation>();
        mCore->rtti()->registerType<CameraOnGridSelection>();
        mCore->rtti()->registerType<GraphSelectionOverGrid>();
        mCore->rtti()->registerType<GridResize>();
        mCore->rtti()->registerType<EditorState>();
        mCore->rtti()->registerType<ETrans>();
        mCore->rtti()->registerType<StateManager>();
        mCore->rtti()->registerType<GraphManager>();
        mCore->rtti()->registerType<AdjustEdgesLengthsDuringDrag>();
        mCore->rtti()->registerType<SentenceToGraph>();
        mCore->rtti()->registerType<SentenceToNodes>();
        mCore->rtti()->registerType<SelectSentece>();
        mCore->rtti()->registerType<FocusController>();
        mCore->rtti()->registerType<NodeKeyboardDrag>();

        //mCore->initService()->enableDebugLog(true);

        mCore->xml()->loadSubTree("Data/MainGuiFsm.xml", true);

     }

    void MainState::update(float dSeconds)
    {
    }

    void MainState::shutdown()
    {
        mCore->tree()->deinit();
    }

    the::string MainState::type() const
    {
        return "main state";
    }


} // namespace mp
