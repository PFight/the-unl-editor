#ifndef MP_MAINSTATE_H
#define MP_MAINSTATE_H


#include "Logic/ApplicationManager.hpp"
#include "The2D/StateMachine.hpp"

#include "Box2DSupport/PhysicManager.hpp"
#include <QMainWindow>
#include "Logic/MainWindowManager.hpp"
#include "Logic/SceneManager.hpp"
#include "Logic/Graph/GraphHelper.hpp"
#include "Logic/LayoutManager.hpp"

#include "Logic/Graph/Graph.hpp"
#include "Logic/SelectionManager.hpp"
//#include "Logic/ArrowSelection.hpp"
#include "Logic/NodeEdgeEdit.hpp"
#include "Logic/NodeCreation.hpp"
#include "Logic/EdgeCreation.hpp"
#include "Logic/MouseSelection.hpp"
//#include "Logic/CameraOnSelection.hpp"
#include "Logic/MainWindowManager.hpp"
#include "Logic/FreeCamera.hpp"
#include "Logic/MouseHoverGraph.hpp"
#include "Logic/UserInput/InputCommands.hpp"

#include <The2D/StateMachine/FiniteStateMachine.hpp>

class TestClass
{
  public:
    void go()
    {
        typedef void (TestClass::*FuncType)(int);
        FuncType signal;
        std::function<void(int)> mySignal;

        for (int i= 0; i<1; i++)
        {
            signal = &TestClass::test;
            mySignal = std::bind(&TestClass::test, this, std::placeholders::_1);
        }

        auto COUNT = 1000000000;

        for (int i =0; i<10000; i++)
        {
            (this->*signal)(42);
            mySignal(42);
        }

        std::cout << "Begun..." << std::endl;

        using namespace std;
        double begin = std::chrono::duration_cast<std::chrono::nanoseconds>(
               std::chrono::high_resolution_clock::now().time_since_epoch()).count();

        for (int i = 0; i < COUNT; i++)
        {
            (this->*signal)(42);
        }

        double end = std::chrono::duration_cast<std::chrono::nanoseconds>(
               std::chrono::high_resolution_clock::now().time_since_epoch()).count();
        double elapsed_ms = double(end - begin) / 1000000.0;

        std::cout << "Pointer: " << elapsed_ms << " ms" << std::endl;

        begin = std::chrono::duration_cast<std::chrono::nanoseconds>(
               std::chrono::high_resolution_clock::now().time_since_epoch()).count();

        for (int i = 0; i < COUNT; i++)
        {
            mySignal(42);
        }

        end = std::chrono::duration_cast<std::chrono::nanoseconds>(
               std::chrono::high_resolution_clock::now().time_since_epoch()).count();
        double elapsed2_ms = double(end - begin) / 1000000.0;

        std::cout << "Bind: " << elapsed2_ms << " ms" << std::endl;

        std::cout << "Bind / Pointer: " << elapsed2_ms / elapsed_ms << std::endl;
        std::cout << " Pointer / Bind: " << elapsed_ms / elapsed2_ms << std::endl;
    }

    void test(int a);
};


namespace mp
{

    class MainState : public the::IState
    {
        public:
            class GuiEventData
            {
                public:
                    virtual ~GuiEventData() {}
            };

            template<typename T>
            class GuiEventDataTpl: public GuiEventData
            {
                public:
                    ~GuiEventDataTpl() {}
                    GuiEventDataTpl(T val){ mValue = val; }
                    T mValue;
            };

            class MouseData: public GuiEventData
            {
                public:
                    ~MouseData() {}
                    QPoint mousePos;
                    Node* pointedNode;
            };

            class KeyData: public GuiEventData
            {
                public:
                    ~KeyData() {}
                    int key;
            };


            enum GuiState
            {
                BrowseMap=1,
                EditMap=2,
                CreatingEdge=4,
                FreeView=8,
                SaveRequest = 16
            };

            enum GuiEvent
            {
                CommandMoveSelectionUp,
                CommandMoveSelectionDown,
                CommandMoveSelectionLeft,
                CommandMoveSelectionRight,
                CommandEditSelected,
                CommandAddNode,
                CommandAddEdge,
                CommandAccept,
                CommandCancel,
                LeftMouseDownOnNode,
                LeftMouseUpOnNode,
                LeftMouseDownOnEmpty,
                LeftMouseUpOnEmpty,
                MouseMove,
                AnyKey,
                CommandSave,
                Exit,
                CommandDeleteSelected
            };

            


            MainState();
            ~MainState();

            void init(InfrastructureCore* core);


            void update(float dSeconds);

            void shutdown();
            the::string type() const;

            void processEvent(GuiEvent event, GuiEventData* data = nullptr);

            void catchQtEvent(QEvent* context);


        private:
            GuiState mCurrentState;
            GuiState mLastState;

            InfrastructureCore *mCore;

    };

} // namespace mp

#endif // MP_MAINSTATE_H
