
#pragma once

#include <QGraphicsView>
#include <QWheelEvent>
#include "The2D/Config.hpp"
#include <vector>
#include <The2D/Utils/Signals.hpp>


class UnlGraphicsView : public QGraphicsView
{
    Q_OBJECT

    public:
        
        explicit UnlGraphicsView(QWidget *parent = 0);
        ~UnlGraphicsView();

        void wheelEvent(QWheelEvent *) override;

        bool event(QEvent* ev) override;

    public slots:


    private:

};
