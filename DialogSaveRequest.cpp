#include "DialogSaveRequest.hpp"
#include "ui_DialogSaveRequest.h"

DialogSaveRequest::DialogSaveRequest(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogSaveRequest)
{
    ui->setupUi(this);
}

DialogSaveRequest::~DialogSaveRequest()
{
    delete ui;
}
