#include "States/MainState.hpp"
#include "The2D/InfrastructureCore.hpp"
#include "The2D/Config.hpp"


int main(int argc, char *argv[])
{
    
    the::InfrastructureCore core;
    std::shared_ptr<mp::MainState> ms = std::shared_ptr<mp::MainState>(new mp::MainState());
    core.state()->push(ms);

    return core.runGame();
}
