#pragma once

#include <QDialog>
#include <The2D/Config.hpp>
#include <vector>

namespace Ui {
class KnotDialog;
}

namespace mp
{
    class KnotDialog : public QDialog
    {
        Q_OBJECT

        public:

            explicit KnotDialog(QWidget *parent = 0);
            ~KnotDialog();

            void setKnotText(the::string txt);
            the::string getKnotText();

            void showEvent(QShowEvent *);
            void closeEvent(QCloseEvent *ev);

            public slots:

            void btnAccepted();
            void btnRejected();


        private:
            Ui::KnotDialog *ui;
            std::vector<the::string> mTypes;
    };
}
