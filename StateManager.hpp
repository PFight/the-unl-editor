////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Manager.hpp>
#include "EditorState.hpp"

////////////////////////////////////////////////////////////
namespace mp {

    class StateManager : public the::Manager
    {
        public:
            THE_ENTITY(StateManager, the::Manager)

            PInt prmMaxStackSize;
            Bind<EditorState> toStartState;
            EditorState mPrevState;

            StateManager();
            virtual ~StateManager();

            void queueToActivate(EditorState* state);
            void queueToPopStatesStack();
            void queueToRepeat(CEvent* ev);

            const eastl::vector<EditorState*>& getActiveStates();
            void onStateActivated(EditorState* state);
            void onStateDeactivated(EditorState* state);
            void deactivateAll();

            void update() override;

        private:
            eastl::vector<EditorState*> mStatesToActivate;
            eastl::vector<EditorState*> mStatesToPop;
            eastl::vector<CEvent*> mEventsToRepeat;
            eastl::vector<EditorState*> mActiveStates;
            eastl::list<EditorState*> mStatesStack;

            void setActive(EditorState* state, bool activate, string* msg,
                string ident, const char* oneIdent, eastl::vector<EditorState*>* coactiveStates = nullptr);
            void setActivateForCoactiveStates(EditorState* state, bool activate, string* msg,
                string ident, const char* oneIdent, eastl::vector<EditorState*>* coactiveStates);

            void onPostInit() override;
            void onDeinit() override;

    };
}; // namespace mp
////////////////////////////////////////////////////////////
