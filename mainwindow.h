#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <The2D/Utils/Signals.hpp>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    the::Signal<void(QResizeEvent* ev)> SigResize;
    the::Signal<void(QMoveEvent* ev)> SigMove;
    the::Signal<void(QEvent* ev)> SigEvent;
    the::Signal<void(QCloseEvent*)> SigCloseEvent;

    virtual void resizeEvent(QResizeEvent * event) override;
    virtual bool event(QEvent *event) override;
    virtual void closeEvent(QCloseEvent* ev) override;
    virtual void moveEvent(QMoveEvent* ev) override;

    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
