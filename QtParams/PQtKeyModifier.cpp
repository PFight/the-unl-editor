#include"PQtKeyModifier.hpp"

using namespace the;
using namespace mp;

PQtKeyModifier::PQtKeyModifier()
{
    setMap({
        std::pair<string, Qt::Key>("Shift", (Qt::Key)0x01000020),                // modifiers
        std::pair<string, Qt::Key>("Control", (Qt::Key)0x01000021),
        std::pair<string, Qt::Key>("Meta", (Qt::Key)0x01000022),
        std::pair<string, Qt::Key>("Alt", (Qt::Key)0x01000023),
        std::pair<string, Qt::Key>("CapsLock", (Qt::Key)0x01000024),
        std::pair<string, Qt::Key>("NumLock", (Qt::Key)0x01000025),
        std::pair<string, Qt::Key>("ScrollLock", (Qt::Key)0x01000026),
    });
}