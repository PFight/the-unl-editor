#pragma once

#include <The2D/Parameters.hpp>
#include <qnamespace.h>

using namespace the;

namespace mp
{
    class PQtKeyModifier : public PMapped<Qt::Key>
    {
        public:
            PQtKeyModifier();
    };
}