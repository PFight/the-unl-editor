#pragma once

#include <The2D/Parameters.hpp>
 #include <QKeySequence>

using namespace the;

namespace mp
{
    class PQtKeySequence : public TValueParameter<QKeySequence>
    {
        public:
           virtual ~PQtKeySequence();

            virtual string serialize() const override;

            void operator=(QKeySequence val);

        protected:
            virtual bool onParse(string& strValue) override;
    };
}