#include"PQtKey.hpp"

using namespace the;
using namespace mp;

PQtKey::PQtKey()
{
    setMap({
        std::pair<string, Qt::Key>("Escape", (Qt::Key)0x01000000),                // misc keys
        std::pair<string, Qt::Key>("Tab", (Qt::Key)0x01000001),
        std::pair<string, Qt::Key>("Backtab", (Qt::Key)0x01000002),
        std::pair<string, Qt::Key>("Backspace", (Qt::Key)0x01000003),
        std::pair<string, Qt::Key>("Return", (Qt::Key)0x01000004),
        std::pair<string, Qt::Key>("Enter", (Qt::Key)0x01000005),
        std::pair<string, Qt::Key>("Insert", (Qt::Key)0x01000006),
        std::pair<string, Qt::Key>("Delete", (Qt::Key)0x01000007),
        std::pair<string, Qt::Key>("Pause", (Qt::Key)0x01000008),
        std::pair<string, Qt::Key>("Print", (Qt::Key)0x01000009),
        std::pair<string, Qt::Key>("SysReq", (Qt::Key)0x0100000a),
        std::pair<string, Qt::Key>("Clear", (Qt::Key)0x0100000b),
        std::pair<string, Qt::Key>("Home", (Qt::Key)0x01000010),                // cursor movement
        std::pair<string, Qt::Key>("End", (Qt::Key)0x01000011),
        std::pair<string, Qt::Key>("Left", (Qt::Key)0x01000012),
        std::pair<string, Qt::Key>("Up", (Qt::Key)0x01000013),
        std::pair<string, Qt::Key>("Right", (Qt::Key)0x01000014),
        std::pair<string, Qt::Key>("Down", (Qt::Key)0x01000015),
        std::pair<string, Qt::Key>("PageUp", (Qt::Key)0x01000016),
        std::pair<string, Qt::Key>("PageDown", (Qt::Key)0x01000017),
        std::pair<string, Qt::Key>("Shift", (Qt::Key)0x01000020),                // modifiers
        std::pair<string, Qt::Key>("Control", (Qt::Key)0x01000021),
        std::pair<string, Qt::Key>("Meta", (Qt::Key)0x01000022),
        std::pair<string, Qt::Key>("Alt", (Qt::Key)0x01000023),
        std::pair<string, Qt::Key>("CapsLock", (Qt::Key)0x01000024),
        std::pair<string, Qt::Key>("NumLock", (Qt::Key)0x01000025),
        std::pair<string, Qt::Key>("ScrollLock", (Qt::Key)0x01000026),
        std::pair<string, Qt::Key>("F1", (Qt::Key)0x01000030),                // function keys
        std::pair<string, Qt::Key>("F2", (Qt::Key)0x01000031),
        std::pair<string, Qt::Key>("F3", (Qt::Key)0x01000032),
        std::pair<string, Qt::Key>("F4", (Qt::Key)0x01000033),
        std::pair<string, Qt::Key>("F5", (Qt::Key)0x01000034),
        std::pair<string, Qt::Key>("F6", (Qt::Key)0x01000035),
        std::pair<string, Qt::Key>("F7", (Qt::Key)0x01000036),
        std::pair<string, Qt::Key>("F8", (Qt::Key)0x01000037),
        std::pair<string, Qt::Key>("F9", (Qt::Key)0x01000038),
        std::pair<string, Qt::Key>("F10", (Qt::Key)0x01000039),
        std::pair<string, Qt::Key>("F11", (Qt::Key)0x0100003a),
        std::pair<string, Qt::Key>("F12", (Qt::Key)0x0100003b),
        std::pair<string, Qt::Key>("F13", (Qt::Key)0x0100003c),
        std::pair<string, Qt::Key>("F14", (Qt::Key)0x0100003d),
        std::pair<string, Qt::Key>("F15", (Qt::Key)0x0100003e),
        std::pair<string, Qt::Key>("F16", (Qt::Key)0x0100003f),
        std::pair<string, Qt::Key>("F17", (Qt::Key)0x01000040),
        std::pair<string, Qt::Key>("F18", (Qt::Key)0x01000041),
        std::pair<string, Qt::Key>("F19", (Qt::Key)0x01000042),
        std::pair<string, Qt::Key>("F20", (Qt::Key)0x01000043),
        std::pair<string, Qt::Key>("F21", (Qt::Key)0x01000044),
        std::pair<string, Qt::Key>("F22", (Qt::Key)0x01000045),
        std::pair<string, Qt::Key>("F23", (Qt::Key)0x01000046),
        std::pair<string, Qt::Key>("F24", (Qt::Key)0x01000047),
        std::pair<string, Qt::Key>("F25", (Qt::Key)0x01000048),                // F25 .. F35 only on X11
        std::pair<string, Qt::Key>("F26", (Qt::Key)0x01000049),
        std::pair<string, Qt::Key>("F27", (Qt::Key)0x0100004a),
        std::pair<string, Qt::Key>("F28", (Qt::Key)0x0100004b),
        std::pair<string, Qt::Key>("F29", (Qt::Key)0x0100004c),
        std::pair<string, Qt::Key>("F30", (Qt::Key)0x0100004d),
        std::pair<string, Qt::Key>("F31", (Qt::Key)0x0100004e),
        std::pair<string, Qt::Key>("F32", (Qt::Key)0x0100004f),
        std::pair<string, Qt::Key>("F33", (Qt::Key)0x01000050),
        std::pair<string, Qt::Key>("F34", (Qt::Key)0x01000051),
        std::pair<string, Qt::Key>("F35", (Qt::Key)0x01000052),
        std::pair<string, Qt::Key>("Super_L", (Qt::Key)0x01000053),                 // extra keys
        std::pair<string, Qt::Key>("Super_R", (Qt::Key)0x01000054),
        std::pair<string, Qt::Key>("Menu", (Qt::Key)0x01000055),
        std::pair<string, Qt::Key>("Hyper_L", (Qt::Key)0x01000056),
        std::pair<string, Qt::Key>("Hyper_R", (Qt::Key)0x01000057),
        std::pair<string, Qt::Key>("Help", (Qt::Key)0x01000058),
        std::pair<string, Qt::Key>("Direction_L", (Qt::Key)0x01000059),
        std::pair<string, Qt::Key>("Direction_R", (Qt::Key)0x01000060),
        std::pair<string, Qt::Key>("Space", (Qt::Key)0x20),                // 7 bit printable ASCII
        std::pair<string, Qt::Key>("Exclam", (Qt::Key)0x21),
        std::pair<string, Qt::Key>("QuoteDbl", (Qt::Key)0x22),
        std::pair<string, Qt::Key>("NumberSign", (Qt::Key)0x23),
        std::pair<string, Qt::Key>("Dollar", (Qt::Key)0x24),
        std::pair<string, Qt::Key>("Percent", (Qt::Key)0x25),
        std::pair<string, Qt::Key>("Ampersand", (Qt::Key)0x26),
        std::pair<string, Qt::Key>("Apostrophe", (Qt::Key)0x27),
        std::pair<string, Qt::Key>("ParenLeft", (Qt::Key)0x28),
        std::pair<string, Qt::Key>("ParenRight", (Qt::Key)0x29),
        std::pair<string, Qt::Key>("Asterisk", (Qt::Key)0x2a),
        std::pair<string, Qt::Key>("Plus", (Qt::Key)0x2b),
        std::pair<string, Qt::Key>("Comma", (Qt::Key)0x2c),
        std::pair<string, Qt::Key>("Minus", (Qt::Key)0x2d),
        std::pair<string, Qt::Key>("Period", (Qt::Key)0x2e),
        std::pair<string, Qt::Key>("Slash", (Qt::Key)0x2f),
        std::pair<string, Qt::Key>("0", (Qt::Key)0x30),
        std::pair<string, Qt::Key>("1", (Qt::Key)0x31),
        std::pair<string, Qt::Key>("2", (Qt::Key)0x32),
        std::pair<string, Qt::Key>("3", (Qt::Key)0x33),
        std::pair<string, Qt::Key>("4", (Qt::Key)0x34),
        std::pair<string, Qt::Key>("5", (Qt::Key)0x35),
        std::pair<string, Qt::Key>("6", (Qt::Key)0x36),
        std::pair<string, Qt::Key>("7", (Qt::Key)0x37),
        std::pair<string, Qt::Key>("8", (Qt::Key)0x38),
        std::pair<string, Qt::Key>("9", (Qt::Key)0x39),
        std::pair<string, Qt::Key>("Colon", (Qt::Key)0x3a),
        std::pair<string, Qt::Key>("Semicolon", (Qt::Key)0x3b),
        std::pair<string, Qt::Key>("Less", (Qt::Key)0x3c),
        std::pair<string, Qt::Key>("Equal", (Qt::Key)0x3d),
        std::pair<string, Qt::Key>("Greater", (Qt::Key)0x3e),
        std::pair<string, Qt::Key>("Question", (Qt::Key)0x3f),
        std::pair<string, Qt::Key>("At", (Qt::Key)0x40),
        std::pair<string, Qt::Key>("A", (Qt::Key)0x41),
        std::pair<string, Qt::Key>("B", (Qt::Key)0x42),
        std::pair<string, Qt::Key>("C", (Qt::Key)0x43),
        std::pair<string, Qt::Key>("D", (Qt::Key)0x44),
        std::pair<string, Qt::Key>("E", (Qt::Key)0x45),
        std::pair<string, Qt::Key>("F", (Qt::Key)0x46),
        std::pair<string, Qt::Key>("G", (Qt::Key)0x47),
        std::pair<string, Qt::Key>("H", (Qt::Key)0x48),
        std::pair<string, Qt::Key>("I", (Qt::Key)0x49),
        std::pair<string, Qt::Key>("J", (Qt::Key)0x4a),
        std::pair<string, Qt::Key>("K", (Qt::Key)0x4b),
        std::pair<string, Qt::Key>("L", (Qt::Key)0x4c),
        std::pair<string, Qt::Key>("M", (Qt::Key)0x4d),
        std::pair<string, Qt::Key>("N", (Qt::Key)0x4e),
        std::pair<string, Qt::Key>("O", (Qt::Key)0x4f),
        std::pair<string, Qt::Key>("P", (Qt::Key)0x50),
        std::pair<string, Qt::Key>("Q", (Qt::Key)0x51),
        std::pair<string, Qt::Key>("R", (Qt::Key)0x52),
        std::pair<string, Qt::Key>("S", (Qt::Key)0x53),
        std::pair<string, Qt::Key>("T", (Qt::Key)0x54),
        std::pair<string, Qt::Key>("U", (Qt::Key)0x55),
        std::pair<string, Qt::Key>("V", (Qt::Key)0x56),
        std::pair<string, Qt::Key>("W", (Qt::Key)0x57),
        std::pair<string, Qt::Key>("X", (Qt::Key)0x58),
        std::pair<string, Qt::Key>("Y", (Qt::Key)0x59),
        std::pair<string, Qt::Key>("Z", (Qt::Key)0x5a),
        std::pair<string, Qt::Key>("BracketLeft", (Qt::Key)0x5b),
        std::pair<string, Qt::Key>("Backslash", (Qt::Key)0x5c),
        std::pair<string, Qt::Key>("BracketRight", (Qt::Key)0x5d),
        std::pair<string, Qt::Key>("AsciiCircum", (Qt::Key)0x5e),
        std::pair<string, Qt::Key>("Underscore", (Qt::Key)0x5f),
        std::pair<string, Qt::Key>("QuoteLeft", (Qt::Key)0x60),
        std::pair<string, Qt::Key>("BraceLeft", (Qt::Key)0x7b),
        std::pair<string, Qt::Key>("Bar", (Qt::Key)0x7c),
        std::pair<string, Qt::Key>("BraceRight", (Qt::Key)0x7d),
        std::pair<string, Qt::Key>("AsciiTilde", (Qt::Key)0x7e),

        std::pair<string, Qt::Key>("nobreakspace", (Qt::Key)0x0a0),
        std::pair<string, Qt::Key>("exclamdown", (Qt::Key)0x0a1),
        std::pair<string, Qt::Key>("cent", (Qt::Key)0x0a2),
        std::pair<string, Qt::Key>("sterling", (Qt::Key)0x0a3),
        std::pair<string, Qt::Key>("currency", (Qt::Key)0x0a4),
        std::pair<string, Qt::Key>("yen", (Qt::Key)0x0a5),
        std::pair<string, Qt::Key>("brokenbar", (Qt::Key)0x0a6),
        std::pair<string, Qt::Key>("section", (Qt::Key)0x0a7),
        std::pair<string, Qt::Key>("diaeresis", (Qt::Key)0x0a8),
        std::pair<string, Qt::Key>("copyright", (Qt::Key)0x0a9),
        std::pair<string, Qt::Key>("ordfeminine", (Qt::Key)0x0aa),
        std::pair<string, Qt::Key>("guillemotleft", (Qt::Key)0x0ab),        // left angle quotation mark
        std::pair<string, Qt::Key>("notsign", (Qt::Key)0x0ac),
        std::pair<string, Qt::Key>("hyphen", (Qt::Key)0x0ad),
        std::pair<string, Qt::Key>("registered", (Qt::Key)0x0ae),
        std::pair<string, Qt::Key>("macron", (Qt::Key)0x0af),
        std::pair<string, Qt::Key>("degree", (Qt::Key)0x0b0),
        std::pair<string, Qt::Key>("plusminus", (Qt::Key)0x0b1),
        std::pair<string, Qt::Key>("twosuperior", (Qt::Key)0x0b2),
        std::pair<string, Qt::Key>("threesuperior", (Qt::Key)0x0b3),
        std::pair<string, Qt::Key>("acute", (Qt::Key)0x0b4),
        std::pair<string, Qt::Key>("mu", (Qt::Key)0x0b5),
        std::pair<string, Qt::Key>("paragraph", (Qt::Key)0x0b6),
        std::pair<string, Qt::Key>("periodcentered", (Qt::Key)0x0b7),
        std::pair<string, Qt::Key>("cedilla", (Qt::Key)0x0b8),
        std::pair<string, Qt::Key>("onesuperior", (Qt::Key)0x0b9),
        std::pair<string, Qt::Key>("masculine", (Qt::Key)0x0ba),
        std::pair<string, Qt::Key>("guillemotright", (Qt::Key)0x0bb),        // right angle quotation mark
        std::pair<string, Qt::Key>("onequarter", (Qt::Key)0x0bc),
        std::pair<string, Qt::Key>("onehalf", (Qt::Key)0x0bd),
        std::pair<string, Qt::Key>("threequarters", (Qt::Key)0x0be),
        std::pair<string, Qt::Key>("questiondown", (Qt::Key)0x0bf),
        std::pair<string, Qt::Key>("Agrave", (Qt::Key)0x0c0),
        std::pair<string, Qt::Key>("Aacute", (Qt::Key)0x0c1),
        std::pair<string, Qt::Key>("Acircumflex", (Qt::Key)0x0c2),
        std::pair<string, Qt::Key>("Atilde", (Qt::Key)0x0c3),
        std::pair<string, Qt::Key>("Adiaeresis", (Qt::Key)0x0c4),
        std::pair<string, Qt::Key>("Aring", (Qt::Key)0x0c5),
        std::pair<string, Qt::Key>("AE", (Qt::Key)0x0c6),
        std::pair<string, Qt::Key>("Ccedilla", (Qt::Key)0x0c7),
        std::pair<string, Qt::Key>("Egrave", (Qt::Key)0x0c8),
        std::pair<string, Qt::Key>("Eacute", (Qt::Key)0x0c9),
        std::pair<string, Qt::Key>("Ecircumflex", (Qt::Key)0x0ca),
        std::pair<string, Qt::Key>("Ediaeresis", (Qt::Key)0x0cb),
        std::pair<string, Qt::Key>("Igrave", (Qt::Key)0x0cc),
        std::pair<string, Qt::Key>("Iacute", (Qt::Key)0x0cd),
        std::pair<string, Qt::Key>("Icircumflex", (Qt::Key)0x0ce),
        std::pair<string, Qt::Key>("Idiaeresis", (Qt::Key)0x0cf),
        std::pair<string, Qt::Key>("ETH", (Qt::Key)0x0d0),
        std::pair<string, Qt::Key>("Ntilde", (Qt::Key)0x0d1),
        std::pair<string, Qt::Key>("Ograve", (Qt::Key)0x0d2),
        std::pair<string, Qt::Key>("Oacute", (Qt::Key)0x0d3),
        std::pair<string, Qt::Key>("Ocircumflex", (Qt::Key)0x0d4),
        std::pair<string, Qt::Key>("Otilde", (Qt::Key)0x0d5),
        std::pair<string, Qt::Key>("Odiaeresis", (Qt::Key)0x0d6),
        std::pair<string, Qt::Key>("multiply", (Qt::Key)0x0d7),
        std::pair<string, Qt::Key>("Ooblique", (Qt::Key)0x0d8),
        std::pair<string, Qt::Key>("Ugrave", (Qt::Key)0x0d9),
        std::pair<string, Qt::Key>("Uacute", (Qt::Key)0x0da),
        std::pair<string, Qt::Key>("Ucircumflex", (Qt::Key)0x0db),
        std::pair<string, Qt::Key>("Udiaeresis", (Qt::Key)0x0dc),
        std::pair<string, Qt::Key>("Yacute", (Qt::Key)0x0dd),
        std::pair<string, Qt::Key>("THORN", (Qt::Key)0x0de),
        std::pair<string, Qt::Key>("ssharp", (Qt::Key)0x0df),
        std::pair<string, Qt::Key>("division", (Qt::Key)0x0f7),
        std::pair<string, Qt::Key>("ydiaeresis", (Qt::Key)0x0ff),

        // International input method support (X keycode - 0xEE00), the
        // definition follows Qt/Embedded 2.3.7) Only interesting if
        // you are writing your own input method

        // International & multi-key character composition
        std::pair<string, Qt::Key>("AltGr", (Qt::Key)0x01001103),
        std::pair<string, Qt::Key>("Multi_key", (Qt::Key)0x01001120),  // Multi-key character compose
        std::pair<string, Qt::Key>("Codeinput", (Qt::Key)0x01001137),
        std::pair<string, Qt::Key>("SingleCandidate", (Qt::Key)0x0100113c),
        std::pair<string, Qt::Key>("MultipleCandidate", (Qt::Key)0x0100113d),
        std::pair<string, Qt::Key>("PreviousCandidate", (Qt::Key)0x0100113e),

        // Misc Functions
        std::pair<string, Qt::Key>("Mode_switch", (Qt::Key)0x0100117e),  // Character set switch
        //std::pair<string, Qt::Key>("script_switch", (Qt::Key)0x0100117e),  // Alias for mode_switch

        // Japanese keyboard support
        std::pair<string, Qt::Key>("Kanji", (Qt::Key)0x01001121),  // Kanji, Kanji convert
        std::pair<string, Qt::Key>("Muhenkan", (Qt::Key)0x01001122),  // Cancel Conversion
        //std::pair<string, Qt::Key>("Henkan_Mode", (Qt::Key)0x01001123),  // Start/Stop Conversion
        std::pair<string, Qt::Key>("Henkan", (Qt::Key)0x01001123),  // Alias for Henkan_Mode
        std::pair<string, Qt::Key>("Romaji", (Qt::Key)0x01001124),  // to Romaji
        std::pair<string, Qt::Key>("Hiragana", (Qt::Key)0x01001125),  // to Hiragana
        std::pair<string, Qt::Key>("Katakana", (Qt::Key)0x01001126),  // to Katakana
        std::pair<string, Qt::Key>("Hiragana_Katakana", (Qt::Key)0x01001127),  // Hiragana/Katakana toggle
        std::pair<string, Qt::Key>("Zenkaku", (Qt::Key)0x01001128),  // to Zenkaku
        std::pair<string, Qt::Key>("Hankaku", (Qt::Key)0x01001129),  // to Hankaku
        std::pair<string, Qt::Key>("Zenkaku_Hankaku", (Qt::Key)0x0100112a),  // Zenkaku/Hankaku toggle
        std::pair<string, Qt::Key>("Touroku", (Qt::Key)0x0100112b),  // Add to Dictionary
        std::pair<string, Qt::Key>("Massyo", (Qt::Key)0x0100112c),  // Delete from Dictionary
        std::pair<string, Qt::Key>("Kana_Lock", (Qt::Key)0x0100112d),  // Kana Lock
        std::pair<string, Qt::Key>("Kana_Shift", (Qt::Key)0x0100112e),  // Kana Shift
        std::pair<string, Qt::Key>("Eisu_Shift", (Qt::Key)0x0100112f),  // Alphanumeric Shift
        std::pair<string, Qt::Key>("Eisu_toggle", (Qt::Key)0x01001130),  // Alphanumeric toggle
        //std::pair<string, Qt::Key>("Kanji_Bangou", (Qt::Key)0x01001137),  // Codeinput
        //std::pair<string, Qt::Key>("Zen_Koho", (Qt::Key)0x0100113d),  // Multiple/All Candidate(s)
        //std::pair<string, Qt::Key>("Mae_Koho", (Qt::Key)0x0100113e),  // Previous Candidate

        // Korean keyboard support
        //
        // In fact, many Korean users need only 2 keys, std::pair<string, Qt::Key>("Hangul and
        // std::pair<string, Qt::Key>("Hangul_Hanja. But rest of the keys are good for future.

        std::pair<string, Qt::Key>("Hangul", (Qt::Key)0x01001131),  // Hangul start/stop(toggle)
        std::pair<string, Qt::Key>("Hangul_Start", (Qt::Key)0x01001132),  // Hangul start
        std::pair<string, Qt::Key>("Hangul_End", (Qt::Key)0x01001133),  // Hangul end), English start
        std::pair<string, Qt::Key>("Hangul_Hanja", (Qt::Key)0x01001134),  // Start Hangul->Hanja Conversion
        std::pair<string, Qt::Key>("Hangul_Jamo", (Qt::Key)0x01001135),  // Hangul Jamo mode
        std::pair<string, Qt::Key>("Hangul_Romaja", (Qt::Key)0x01001136),  // Hangul Romaja mode
        //std::pair<string, Qt::Key>("Hangul_Codeinput", (Qt::Key)0x01001137),  // Hangul code input mode
        std::pair<string, Qt::Key>("Hangul_Jeonja", (Qt::Key)0x01001138),  // Jeonja mode
        std::pair<string, Qt::Key>("Hangul_Banja", (Qt::Key)0x01001139),  // Banja mode
        std::pair<string, Qt::Key>("Hangul_PreHanja", (Qt::Key)0x0100113a),  // Pre Hanja conversion
        std::pair<string, Qt::Key>("Hangul_PostHanja", (Qt::Key)0x0100113b),  // Post Hanja conversion
        //std::pair<string, Qt::Key>("Hangul_SingleCandidate", (Qt::Key)0x0100113c),  // Single candidate
        //std::pair<string, Qt::Key>("Hangul_MultipleCandidate", (Qt::Key)0x0100113d),  // Multiple candidate
        //std::pair<string, Qt::Key>("Hangul_PreviousCandidate", (Qt::Key)0x0100113e),  // Previous candidate
        std::pair<string, Qt::Key>("Hangul_Special", (Qt::Key)0x0100113f),  // Special symbols
        //std::pair<string, Qt::Key>("Hangul_switch", (Qt::Key)0x0100117e),  // Alias for mode_switch

        // dead keys (X keycode - 0xED00 to avoid the conflict)
        std::pair<string, Qt::Key>("Dead_Grave", (Qt::Key)0x01001250),
        std::pair<string, Qt::Key>("Dead_Acute", (Qt::Key)0x01001251),
        std::pair<string, Qt::Key>("Dead_Circumflex", (Qt::Key)0x01001252),
        std::pair<string, Qt::Key>("Dead_Tilde", (Qt::Key)0x01001253),
        std::pair<string, Qt::Key>("Dead_Macron", (Qt::Key)0x01001254),
        std::pair<string, Qt::Key>("Dead_Breve", (Qt::Key)0x01001255),
        std::pair<string, Qt::Key>("Dead_Abovedot", (Qt::Key)0x01001256),
        std::pair<string, Qt::Key>("Dead_Diaeresis", (Qt::Key)0x01001257),
        std::pair<string, Qt::Key>("Dead_Abovering", (Qt::Key)0x01001258),
        std::pair<string, Qt::Key>("Dead_Doubleacute", (Qt::Key)0x01001259),
        std::pair<string, Qt::Key>("Dead_Caron", (Qt::Key)0x0100125a),
        std::pair<string, Qt::Key>("Dead_Cedilla", (Qt::Key)0x0100125b),
        std::pair<string, Qt::Key>("Dead_Ogonek", (Qt::Key)0x0100125c),
        std::pair<string, Qt::Key>("Dead_Iota", (Qt::Key)0x0100125d),
        std::pair<string, Qt::Key>("Dead_Voiced_Sound", (Qt::Key)0x0100125e),
        std::pair<string, Qt::Key>("Dead_Semivoiced_Sound", (Qt::Key)0x0100125f),
        std::pair<string, Qt::Key>("Dead_Belowdot", (Qt::Key)0x01001260),
        std::pair<string, Qt::Key>("Dead_Hook", (Qt::Key)0x01001261),
        std::pair<string, Qt::Key>("Dead_Horn", (Qt::Key)0x01001262),

        // multimedia/internet keys - ignored by default - see QKeyEvent c'tor
        std::pair<string, Qt::Key>("Back", (Qt::Key)0x01000061),
        std::pair<string, Qt::Key>("Forward", (Qt::Key)0x01000062),
        std::pair<string, Qt::Key>("Stop", (Qt::Key)0x01000063),
        std::pair<string, Qt::Key>("Refresh", (Qt::Key)0x01000064),
        std::pair<string, Qt::Key>("VolumeDown", (Qt::Key)0x01000070),
        std::pair<string, Qt::Key>("VolumeMute", (Qt::Key)0x01000071),
        std::pair<string, Qt::Key>("VolumeUp", (Qt::Key)0x01000072),
        std::pair<string, Qt::Key>("BassBoost", (Qt::Key)0x01000073),
        std::pair<string, Qt::Key>("BassUp", (Qt::Key)0x01000074),
        std::pair<string, Qt::Key>("BassDown", (Qt::Key)0x01000075),
        std::pair<string, Qt::Key>("TrebleUp", (Qt::Key)0x01000076),
        std::pair<string, Qt::Key>("TrebleDown", (Qt::Key)0x01000077),
        std::pair<string, Qt::Key>("MediaPlay", (Qt::Key)0x01000080),
        std::pair<string, Qt::Key>("MediaStop", (Qt::Key)0x01000081),
        std::pair<string, Qt::Key>("MediaPrevious", (Qt::Key)0x01000082),
        std::pair<string, Qt::Key>("MediaNext", (Qt::Key)0x01000083),
        std::pair<string, Qt::Key>("MediaRecord", (Qt::Key)0x01000084),
        std::pair<string, Qt::Key>("MediaPause", (Qt::Key)0x1000085),
        std::pair<string, Qt::Key>("MediaTogglePlayPause", (Qt::Key)0x1000086),
        std::pair<string, Qt::Key>("HomePage", (Qt::Key)0x01000090),
        std::pair<string, Qt::Key>("Favorites", (Qt::Key)0x01000091),
        std::pair<string, Qt::Key>("Search", (Qt::Key)0x01000092),
        std::pair<string, Qt::Key>("Standby", (Qt::Key)0x01000093),
        std::pair<string, Qt::Key>("OpenUrl", (Qt::Key)0x01000094),
        std::pair<string, Qt::Key>("LaunchMail", (Qt::Key)0x010000a0),
        std::pair<string, Qt::Key>("LaunchMedia", (Qt::Key)0x010000a1),
        std::pair<string, Qt::Key>("Launch0", (Qt::Key)0x010000a2),
        std::pair<string, Qt::Key>("Launch1", (Qt::Key)0x010000a3),
        std::pair<string, Qt::Key>("Launch2", (Qt::Key)0x010000a4),
        std::pair<string, Qt::Key>("Launch3", (Qt::Key)0x010000a5),
        std::pair<string, Qt::Key>("Launch4", (Qt::Key)0x010000a6),
        std::pair<string, Qt::Key>("Launch5", (Qt::Key)0x010000a7),
        std::pair<string, Qt::Key>("Launch6", (Qt::Key)0x010000a8),
        std::pair<string, Qt::Key>("Launch7", (Qt::Key)0x010000a9),
        std::pair<string, Qt::Key>("Launch8", (Qt::Key)0x010000aa),
        std::pair<string, Qt::Key>("Launch9", (Qt::Key)0x010000ab),
        std::pair<string, Qt::Key>("LaunchA", (Qt::Key)0x010000ac),
        std::pair<string, Qt::Key>("LaunchB", (Qt::Key)0x010000ad),
        std::pair<string, Qt::Key>("LaunchC", (Qt::Key)0x010000ae),
        std::pair<string, Qt::Key>("LaunchD", (Qt::Key)0x010000af),
        std::pair<string, Qt::Key>("LaunchE", (Qt::Key)0x010000b0),
        std::pair<string, Qt::Key>("LaunchF", (Qt::Key)0x010000b1),
        std::pair<string, Qt::Key>("MonBrightnessUp", (Qt::Key)0x010000b2),
        std::pair<string, Qt::Key>("MonBrightnessDown", (Qt::Key)0x010000b3),
        std::pair<string, Qt::Key>("KeyboardLightOnOff", (Qt::Key)0x010000b4),
        std::pair<string, Qt::Key>("KeyboardBrightnessUp", (Qt::Key)0x010000b5),
        std::pair<string, Qt::Key>("KeyboardBrightnessDown", (Qt::Key)0x010000b6),
        std::pair<string, Qt::Key>("PowerOff", (Qt::Key)0x010000b7),
        std::pair<string, Qt::Key>("WakeUp", (Qt::Key)0x010000b8),
        std::pair<string, Qt::Key>("Eject", (Qt::Key)0x010000b9),
        std::pair<string, Qt::Key>("ScreenSaver", (Qt::Key)0x010000ba),
        std::pair<string, Qt::Key>("WWW", (Qt::Key)0x010000bb),
        std::pair<string, Qt::Key>("Memo", (Qt::Key)0x010000bc),
        std::pair<string, Qt::Key>("LightBulb", (Qt::Key)0x010000bd),
        std::pair<string, Qt::Key>("Shop", (Qt::Key)0x010000be),
        std::pair<string, Qt::Key>("History", (Qt::Key)0x010000bf),
        std::pair<string, Qt::Key>("AddFavorite", (Qt::Key)0x010000c0),
        std::pair<string, Qt::Key>("HotLinks", (Qt::Key)0x010000c1),
        std::pair<string, Qt::Key>("BrightnessAdjust", (Qt::Key)0x010000c2),
        std::pair<string, Qt::Key>("Finance", (Qt::Key)0x010000c3),
        std::pair<string, Qt::Key>("Community", (Qt::Key)0x010000c4),
        std::pair<string, Qt::Key>("AudioRewind", (Qt::Key)0x010000c5),
        std::pair<string, Qt::Key>("BackForward", (Qt::Key)0x010000c6),
        std::pair<string, Qt::Key>("ApplicationLeft", (Qt::Key)0x010000c7),
        std::pair<string, Qt::Key>("ApplicationRight", (Qt::Key)0x010000c8),
        std::pair<string, Qt::Key>("Book", (Qt::Key)0x010000c9),
        std::pair<string, Qt::Key>("CD", (Qt::Key)0x010000ca),
        std::pair<string, Qt::Key>("Calculator", (Qt::Key)0x010000cb),
        std::pair<string, Qt::Key>("ToDoList", (Qt::Key)0x010000cc),
        std::pair<string, Qt::Key>("ClearGrab", (Qt::Key)0x010000cd),
        std::pair<string, Qt::Key>("Close", (Qt::Key)0x010000ce),
        std::pair<string, Qt::Key>("Copy", (Qt::Key)0x010000cf),
        std::pair<string, Qt::Key>("Cut", (Qt::Key)0x010000d0),
        std::pair<string, Qt::Key>("Display", (Qt::Key)0x010000d1),
        std::pair<string, Qt::Key>("DOS", (Qt::Key)0x010000d2),
        std::pair<string, Qt::Key>("Documents", (Qt::Key)0x010000d3),
        std::pair<string, Qt::Key>("Excel", (Qt::Key)0x010000d4),
        std::pair<string, Qt::Key>("Explorer", (Qt::Key)0x010000d5),
        std::pair<string, Qt::Key>("Game", (Qt::Key)0x010000d6),
        std::pair<string, Qt::Key>("Go", (Qt::Key)0x010000d7),
        std::pair<string, Qt::Key>("iTouch", (Qt::Key)0x010000d8),
        std::pair<string, Qt::Key>("LogOff", (Qt::Key)0x010000d9),
        std::pair<string, Qt::Key>("Market", (Qt::Key)0x010000da),
        std::pair<string, Qt::Key>("Meeting", (Qt::Key)0x010000db),
        std::pair<string, Qt::Key>("MenuKB", (Qt::Key)0x010000dc),
        std::pair<string, Qt::Key>("MenuPB", (Qt::Key)0x010000dd),
        std::pair<string, Qt::Key>("MySites", (Qt::Key)0x010000de),
        std::pair<string, Qt::Key>("News", (Qt::Key)0x010000df),
        std::pair<string, Qt::Key>("OfficeHome", (Qt::Key)0x010000e0),
        std::pair<string, Qt::Key>("Option", (Qt::Key)0x010000e1),
        std::pair<string, Qt::Key>("Paste", (Qt::Key)0x010000e2),
        std::pair<string, Qt::Key>("Phone", (Qt::Key)0x010000e3),
        std::pair<string, Qt::Key>("Calendar", (Qt::Key)0x010000e4),
        std::pair<string, Qt::Key>("Reply", (Qt::Key)0x010000e5),
        std::pair<string, Qt::Key>("Reload", (Qt::Key)0x010000e6),
        std::pair<string, Qt::Key>("RotateWindows", (Qt::Key)0x010000e7),
        std::pair<string, Qt::Key>("RotationPB", (Qt::Key)0x010000e8),
        std::pair<string, Qt::Key>("RotationKB", (Qt::Key)0x010000e9),
        std::pair<string, Qt::Key>("Save", (Qt::Key)0x010000ea),
        std::pair<string, Qt::Key>("Send", (Qt::Key)0x010000eb),
        std::pair<string, Qt::Key>("Spell", (Qt::Key)0x010000ec),
        std::pair<string, Qt::Key>("SplitScreen", (Qt::Key)0x010000ed),
        std::pair<string, Qt::Key>("Support", (Qt::Key)0x010000ee),
        std::pair<string, Qt::Key>("TaskPane", (Qt::Key)0x010000ef),
        std::pair<string, Qt::Key>("Terminal", (Qt::Key)0x010000f0),
        std::pair<string, Qt::Key>("Tools", (Qt::Key)0x010000f1),
        std::pair<string, Qt::Key>("Travel", (Qt::Key)0x010000f2),
        std::pair<string, Qt::Key>("Video", (Qt::Key)0x010000f3),
        std::pair<string, Qt::Key>("Word", (Qt::Key)0x010000f4),
        std::pair<string, Qt::Key>("Xfer", (Qt::Key)0x010000f5),
        std::pair<string, Qt::Key>("ZoomIn", (Qt::Key)0x010000f6),
        std::pair<string, Qt::Key>("ZoomOut", (Qt::Key)0x010000f7),
        std::pair<string, Qt::Key>("Away", (Qt::Key)0x010000f8),
        std::pair<string, Qt::Key>("Messenger", (Qt::Key)0x010000f9),
        std::pair<string, Qt::Key>("WebCam", (Qt::Key)0x010000fa),
        std::pair<string, Qt::Key>("MailForward", (Qt::Key)0x010000fb),
        std::pair<string, Qt::Key>("Pictures", (Qt::Key)0x010000fc),
        std::pair<string, Qt::Key>("Music", (Qt::Key)0x010000fd),
        std::pair<string, Qt::Key>("Battery", (Qt::Key)0x010000fe),
        std::pair<string, Qt::Key>("Bluetooth", (Qt::Key)0x010000ff),
        std::pair<string, Qt::Key>("WLAN", (Qt::Key)0x01000100),
        std::pair<string, Qt::Key>("UWB", (Qt::Key)0x01000101),
        std::pair<string, Qt::Key>("AudioForward", (Qt::Key)0x01000102),
        std::pair<string, Qt::Key>("AudioRepeat", (Qt::Key)0x01000103),
        std::pair<string, Qt::Key>("AudioRandomPlay", (Qt::Key)0x01000104),
        std::pair<string, Qt::Key>("Subtitle", (Qt::Key)0x01000105),
        std::pair<string, Qt::Key>("AudioCycleTrack", (Qt::Key)0x01000106),
        std::pair<string, Qt::Key>("Time", (Qt::Key)0x01000107),
        std::pair<string, Qt::Key>("Hibernate", (Qt::Key)0x01000108),
        std::pair<string, Qt::Key>("View", (Qt::Key)0x01000109),
        std::pair<string, Qt::Key>("TopMenu", (Qt::Key)0x0100010a),
        std::pair<string, Qt::Key>("PowerDown", (Qt::Key)0x0100010b),
        std::pair<string, Qt::Key>("Suspend", (Qt::Key)0x0100010c),
        std::pair<string, Qt::Key>("ContrastAdjust", (Qt::Key)0x0100010d),

        std::pair<string, Qt::Key>("LaunchG", (Qt::Key)0x0100010e),
        std::pair<string, Qt::Key>("LaunchH", (Qt::Key)0x0100010f),

        std::pair<string, Qt::Key>("TouchpadToggle", (Qt::Key)0x01000110),
        std::pair<string, Qt::Key>("TouchpadOn", (Qt::Key)0x01000111),
        std::pair<string, Qt::Key>("TouchpadOff", (Qt::Key)0x01000112),

        std::pair<string, Qt::Key>("MicMute", (Qt::Key)0x01000113),

        std::pair<string, Qt::Key>("MediaLast", (Qt::Key)0x0100ffff),

        // Keypad navigation keys
        std::pair<string, Qt::Key>("Select", (Qt::Key)0x01010000),
        std::pair<string, Qt::Key>("Yes", (Qt::Key)0x01010001),
        std::pair<string, Qt::Key>("No", (Qt::Key)0x01010002),

        // Newer misc keys
        std::pair<string, Qt::Key>("Cancel", (Qt::Key)0x01020001),
        std::pair<string, Qt::Key>("Printer", (Qt::Key)0x01020002),
        std::pair<string, Qt::Key>("Execute", (Qt::Key)0x01020003),
        std::pair<string, Qt::Key>("Sleep", (Qt::Key)0x01020004),
        std::pair<string, Qt::Key>("Play", (Qt::Key)0x01020005), // Not the same as std::pair<string, Qt::Key>("MediaPlay
        std::pair<string, Qt::Key>("Zoom", (Qt::Key)0x01020006),
        //std::pair<string, Qt::Key>("Jisho", (Qt::Key)0x01020007), // IME: Dictionary key
        //std::pair<string, Qt::Key>("Oyayubi_Left", (Qt::Key)0x01020008), // IME: Left Oyayubi key
        //std::pair<string, Qt::Key>("Oyayubi_Right", (Qt::Key)0x01020009), // IME: Right Oyayubi key

        // Device keys
        std::pair<string, Qt::Key>("Context1", (Qt::Key)0x01100000),
        std::pair<string, Qt::Key>("Context2", (Qt::Key)0x01100001),
        std::pair<string, Qt::Key>("Context3", (Qt::Key)0x01100002),
        std::pair<string, Qt::Key>("Context4", (Qt::Key)0x01100003),
        std::pair<string, Qt::Key>("Call", (Qt::Key)0x01100004),      // set absolute state to in a call (do not toggle state)
        std::pair<string, Qt::Key>("Hangup", (Qt::Key)0x01100005),    // set absolute state to hang up (do not toggle state)
        std::pair<string, Qt::Key>("Flip", (Qt::Key)0x01100006),
        std::pair<string, Qt::Key>("ToggleCallHangup", (Qt::Key)0x01100007), // a toggle key for answering, or hanging up, based on current call state
        std::pair<string, Qt::Key>("VoiceDial", (Qt::Key)0x01100008),
        std::pair<string, Qt::Key>("LastNumberRedial", (Qt::Key)0x01100009),

        std::pair<string, Qt::Key>("Camera", (Qt::Key)0x01100020),
        std::pair<string, Qt::Key>("CameraFocus", (Qt::Key)0x01100021),

        std::pair<string, Qt::Key>("unknown", (Qt::Key)0x01ffffff)
    });
}