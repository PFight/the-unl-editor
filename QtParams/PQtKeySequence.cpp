#include "QtParams/PQtKeySequence.hpp"

using namespace the;
using namespace mp;

PQtKeySequence::~PQtKeySequence()
{
}

string PQtKeySequence::serialize() const
{
    return getValue().toString().toStdString();
}

void PQtKeySequence::operator=(QKeySequence val)
{
    setValue(val);
}

bool PQtKeySequence::onParse(string& strValue)
{
    setValue(QKeySequence(strValue.c_str()));
    return !mValue.isEmpty() || strValue == "";
}